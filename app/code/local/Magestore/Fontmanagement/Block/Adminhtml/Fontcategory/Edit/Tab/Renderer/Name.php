<?php
class Magestore_Fontmanagement_Block_Adminhtml_Fontcategory_Edit_Tab_Renderer_Name extends Mage_Adminhtml_Block_Widget implements Varien_Data_Form_Element_Renderer_Interface {
	public function render(Varien_Data_Form_Element_Abstract $element) {

		if ($this->canDisplayUseDefault($element) 
				&& $this->nameInStore($element)==false):
			$element->setDisabled(true);
		endif;
		
		$disabled = true;
		$title = 'use_default';
		$idName = "use_default[]";
		$html = '<td class="label">';
		$html.= ''.$element->getLabelHtml().'';
		$html.= '</td>';
		$html.= '<td class="value">';
		$html.= ''.$element->getElementHtml().'';		
		$html.= '</td>';
		//echo "ajay".$this->canDisplayUseDefault($element);
		$html.= '<td class="scope-label"><span class="nobr">[STORE VIEW]</span></td>';
		if ($this->canDisplayUseDefault($element)):			
			$html.= '<td class="value use-default"><input class="checkbox config-inherit" id="name_default" onclick="toggleValueElements(this, this.parentNode.parentNode)" type="checkbox" name="'.$idName.'" value="name" ' . ($this->nameInStore($element) ? '' : 'checked="checked"') . ' />
			<label for="name_default" class="normal">'.$this->__('Use Default Value').'</label></td>';
		endif;
		return $html;
	}
	
	public function nameInStore($element)
    {
		$formData = $element->getForm()->getDataObject();
		$fontcategorytitle = Mage::getModel('fontmanagement/fontcategorytitle')->getCollection()
												->addFieldToFilter('font_cat_id',$formData->getId())
												->addFieldToFilter('store_id',$formData->getStoreId());
		
		if($fontcategorytitle->getSize() > 0 
			&& $formData->getId()
			&& $formData->getStoreId())
        {
			return true;
        }   			
        return false;
    }
	
	public function canDisplayUseDefault($element)
    {		
		$formData = $element->getForm()->getDataObject();
            if ($this->getData()
				&& $formData->getId()
                && $formData->getStoreId()) {
                return true;
            }        
        return false;
    }
}
?>