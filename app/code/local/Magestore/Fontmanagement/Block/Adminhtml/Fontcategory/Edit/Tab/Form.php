<?php

class Magestore_Fontmanagement_Block_Adminhtml_Fontcategory_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
	  
	$storeObj = new Varien_Object();		
	$storeObj->setId($this->getRequest()->getParam("id"));
	$storeObj->setStoreId($this->getRequest()->getParam("store"));
	$form->setDataObject($storeObj);
	  
      $fieldset = $form->addFieldset('fontmanagement_form', array('legend'=>Mage::helper('fontmanagement')->__('Font information')));
     
      $name = $fieldset->addField('category_name', 'text', array(
          'label'     => Mage::helper('fontmanagement')->__('Font Category Name'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'category_name',
      ));
	  if($name)
		{
			$form->getElement('category_name')->setRenderer(Mage::app()->getLayout()
				->createBlock('fontmanagement/adminhtml_fontcategory_edit_tab_renderer_name'));				
		}
	  $fieldset->addField('position', 'text', array(
          'label'     => Mage::helper('fontmanagement')->__('Position'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'position',
      )); 
     
      if ( Mage::getSingleton('adminhtml/session')->getFontManagementData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getFontManagementData());
          Mage::getSingleton('adminhtml/session')->setFontManagementData(null);
      } elseif ( Mage::registry('fontmanagement_data') ) {
		if(Mage::app()->getRequest()->getParam('store')!=''){
			$storeId = Mage::app()->getRequest()->getParam('store');
		}else{
			$storeId = 0;
		}
		
		$fontcategorytitle = Mage::getModel('fontmanagement/fontcategorytitle')->getCollection()
										->addFieldToFilter('font_cat_id',$this->getRequest()->getParam('id'))
										->addFieldToFilter('store_id',$storeId);		
		
		if($fontcategorytitle->getSize() > 0)
		{
			$data = Mage::registry('fontmanagement_data')->getData();
			$data['category_name'] = $fontcategorytitle->getFirstItem()->getCategoryName();			
		}else{		
			$fontcategorytitle = Mage::getModel('fontmanagement/fontcategorytitle')->getCollection()
										->addFieldToFilter('font_cat_id',$this->getRequest()->getParam('id'))
										->addFieldToFilter('store_id',0);
			$data = Mage::registry('fontmanagement_data')->getData();	
			$data['category_name'] = $fontcategorytitle->getFirstItem()->getCategoryName();					
		}	
		
          $form->setValues($data);
          // $form->setValues(Mage::registry('fontmanagement_data')->getData());
      }
      return parent::_prepareForm();
  }
} ?>