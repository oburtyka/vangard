<?php
$this->startSetup();
$this->run("CREATE TABLE IF NOT EXISTS {$this->getTable('font_category_title')} (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`font_cat_id` int(11) NULL,
`store_id` int(11) DEFAULT 0,
`category_name` varchar(255) NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ; 
");
$this->endSetup();
?>
	