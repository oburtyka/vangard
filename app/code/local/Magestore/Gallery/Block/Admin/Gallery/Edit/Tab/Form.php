<?php

class Magestore_Gallery_Block_Admin_Gallery_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
	$form = new Varien_Data_Form();
	$this->setForm($form);

	$storeObj = new Varien_Object();		
	$storeObj->setId($this->getRequest()->getParam("id"));
	$storeObj->setStoreId($this->getRequest()->getParam("store"));
	$form->setDataObject($storeObj);
	  
      $fieldset = $form->addFieldset('gallery_form', array('legend'=>Mage::helper('gallery')->__('Template information')));
     
	  $albums = array(array('value' => '', 'label' => 'Select a Category'));
	  $collection = Mage::getModel('gallery/album')->getCollection();
	  foreach ($collection as $album) {
		 $albums[] = array('value' => $album->getId(), 'label' => $album->getTitle());
	  }

      $fieldset->addField('album_id', 'select', array(
          'label'     => Mage::helper('gallery')->__('Category'),
          'name'      => 'album_id',
          'required'  => true,
          'values'    => $albums,
      ));

      $name = $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('gallery')->__('Title'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));
		if ($name)
		{
			$form->getElement('title')->setRenderer(Mage::app()->getLayout()
				->createBlock('gallery/admin_gallery_edit_tab_renderer_name'));				
		}
     $fieldset->addField('filename', 'image', array(
          'label'     => Mage::helper('gallery')->__('File'),
          'required'  => false,
          'name'      => 'filename',
	  ));


      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('gallery')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('gallery')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('gallery')->__('Disabled'),
              ),
          ),
      ));
	  
      
     /* $fieldset->addField('order', 'text', array(
          'label'     => Mage::helper('gallery')->__('Order'),
          'class'     => 'validate-zero-or-greater input-text validation-failed',
          'required'  => false,
          'name'      => 'order',
      ));
      $fieldset->addField('content', 'editor', array(
          'name'      => 'content',
          'label'     => Mage::helper('gallery')->__('Content'),
          'title'     => Mage::helper('gallery')->__('Content'),
          'style'     => 'width:700px; height:500px;',
          'wysiwyg'   => false
      ));*/
      if ( Mage::getSingleton('adminhtml/session')->getGalleryData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getGalleryData());
          Mage::getSingleton('adminhtml/session')->setGalleryData(null);
      } elseif ( Mage::registry('gallery_data') ) {
		if(Mage::app()->getRequest()->getParam('store')!=''){
			$storeId = Mage::app()->getRequest()->getParam('store');
		}else{
			$storeId = 0;
		}
		
		$gallerytitle = Mage::getModel('gallery/gallerytitle')->getCollection()
										->addFieldToFilter('gallery_id',$this->getRequest()->getParam('id'))
										->addFieldToFilter('store_id',$storeId);		
		
		if($gallerytitle->getSize() > 0)
		{
			$data = Mage::registry('gallery_data')->getData();
			$data['title'] = $gallerytitle->getFirstItem()->getTitle();			
		}else{		
			$gallerytitle = Mage::getModel('gallery/gallerytitle')->getCollection()
										->addFieldToFilter('gallery_id',$this->getRequest()->getParam('id'))
										->addFieldToFilter('store_id',0);
			$data = Mage::registry('gallery_data')->getData();	
			$data['title'] = $gallerytitle->getFirstItem()->getTitle();					
		}	
		
        $form->setValues($data);
          // $form->setValues(Mage::registry('gallery_data')->getData());
      }
      return parent::_prepareForm();
  }
}