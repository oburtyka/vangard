<?php

class Magestore_Gallery_Block_Admin_Album_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
	  
		$storeObj = new Varien_Object();		
		$storeObj->setId($this->getRequest()->getParam("id"));
		$storeObj->setStoreId($this->getRequest()->getParam("store"));
		$form->setDataObject($storeObj);
	  
      $fieldset = $form->addFieldset('album_form', array('legend'=>Mage::helper('gallery')->__('Category information')));
     
     $name = $fieldset->addField('title', 'text', array(
          'label'     => Mage::helper('gallery')->__('Category Name'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'title',
      ));
		if ($name)
		{
			$form->getElement('title')->setRenderer(Mage::app()->getLayout()
				->createBlock('gallery/admin_album_edit_tab_renderer_name'));				
		}
      $fieldset->addField('filename', 'image', array(
          'label'     => Mage::helper('gallery')->__('Photo'),
          'required'  => false,
          'name'      => 'filename',
	  ));
     /*$fieldset->addField('url_key', 'text', array(
          'label'     => Mage::helper('gallery')->__('Url key'),
          'required'  => false,
          'name'      => 'url_key',
      ));*/
      $fieldset->addField('status', 'select', array(
          'label'     => Mage::helper('gallery')->__('Status'),
          'name'      => 'status',
          'values'    => array(
              array(
                  'value'     => 1,
                  'label'     => Mage::helper('gallery')->__('Enabled'),
              ),

              array(
                  'value'     => 2,
                  'label'     => Mage::helper('gallery')->__('Disabled'),
              ),
          ),
      ));
      
     /* $fieldset->addField('content', 'editor', array(
          'name'      => 'content',
          'label'     => Mage::helper('gallery')->__('Description'),
          'title'     => Mage::helper('gallery')->__('Description'),
          'style'     => 'width:700px; height:500px;',
          'wysiwyg'   => false,
      ));*/
      $fieldset->addField('url_rewrite_id', 'hidden', array(
          'name'      => 'url_rewrite_id',
      ));
      if ( Mage::getSingleton('adminhtml/session')->getGalleryData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getGalleryData());
          Mage::getSingleton('adminhtml/session')->setGalleryData(null);
      } elseif ( Mage::registry('album_data') ) {
		if(Mage::app()->getRequest()->getParam('store')!=''){
			$storeId = Mage::app()->getRequest()->getParam('store');
		}else{
			$storeId = 0;
		}
		
		$albumtitle = Mage::getModel('gallery/albumtitle')->getCollection()
										->addFieldToFilter('album_id',$this->getRequest()->getParam('id'))
										->addFieldToFilter('store_id',$storeId);		
		
		if($albumtitle->getSize() > 0)
		{
			$data = Mage::registry('album_data')->getData();
			$data['title'] = $albumtitle->getFirstItem()->getTitle();			
		}else{		
			$albumtitle = Mage::getModel('gallery/albumtitle')->getCollection()
										->addFieldToFilter('album_id',$this->getRequest()->getParam('id'))
										->addFieldToFilter('store_id',0);
			$data = Mage::registry('album_data')->getData();	
			$data['title'] = $albumtitle->getFirstItem()->getTitle();					
		}	
		
        $form->setValues($data);
          // $form->setValues(Mage::registry('album_data')->getData());
      }
      return parent::_prepareForm();
  }
}