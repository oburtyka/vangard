<?php 

class Magestore_Catalog_Model_Price_Observer
{
    public function __construct(){
   }
	public function apply_dealer_price($observer)
	{

	    $event 			= $observer->getEvent();
		$product 		= $event->getProduct();   
		$finalPrice 	= $product->getFinalPrice();
		
	if($product->getCustomOption('info_buyRequest'))
	{
		$pdfdata = $product->getCustomOption('info_buyRequest')->getItem()->getPdfdata();
		$xml = simplexml_load_string($pdfdata);	
		
		foreach($xml->savecode->accPrizing as $data)
		{
			if(!empty($data->pName) && $name != 2)
				$name =(float) Mage::getStoreConfig('sales/backend/name_price');							
			if(!empty($data->pNumber) && $number != 2)
				$number = (float)Mage::getStoreConfig('sales/backend/number_price');	
				
			$i++;							
		}	
					
		$price = $finalPrice  + $name + $number;
		$product->setFinalPrice($price);
	}
 		return $this;
	}   
} 