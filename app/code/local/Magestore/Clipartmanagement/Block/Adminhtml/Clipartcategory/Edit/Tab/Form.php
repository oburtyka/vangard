<?php

class Magestore_Clipartmanagement_Block_Adminhtml_Clipartcategory_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
	  
	$storeObj = new Varien_Object();		
	$storeObj->setId($this->getRequest()->getParam("id"));
	$storeObj->setStoreId($this->getRequest()->getParam("store"));
	$form->setDataObject($storeObj);
	  
      $fieldset = $form->addFieldset('clipartmanagement_form', array('legend'=>Mage::helper('clipartmanagement')->__('Clipart information')));
     
	  $parent_cat = Mage::getModel('clipartmanagement/clipartcategory')->getCollection()->addFieldToFilter('parent_cat_id', 0)->toOptionArray();
	  $pricedata = Mage::getModel('clipartmanagement/clipartcategory')->getCollection()->addFieldToFilter('clipart_cat_id',Mage::registry('clipartmanagement_data')->getParent_cat_id());
	  $pricedataarray = $pricedata->getdata() ;
	  $priceval = $pricedataarray[0]['price'];
	  $sel_cat = array(
                  'value'     => 0,
                  'label'     => Mage::helper('clipartmanagement')->__('Select Parent Category (Root Category)'),
              );
			  
	  array_unshift($parent_cat, $sel_cat);	  	  
      
	  
	  $name = $fieldset->addField('category_name', 'text', array(
          'label'     => Mage::helper('clipartmanagement')->__('Clipart Category Name'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'category_name',
      ));
	  if ($name)
		{
			$form->getElement('category_name')->setRenderer(Mage::app()->getLayout()
				->createBlock('clipartmanagement/adminhtml_clipartcategory_edit_tab_renderer_name'));				
		}
	   $fieldset->addField('position', 'text', array(
          'label'     => Mage::helper('clipartmanagement')->__('Category Position'),
          'required'  => false,
          'name'      => 'position',
      ));
	 
      if ( Mage::getSingleton('adminhtml/session')->getClipartManagementData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getClipartManagementData());
          Mage::getSingleton('adminhtml/session')->setClipartManagementData(null);
      } elseif ( Mage::registry('clipartmanagement_data') ) {
		if(Mage::app()->getRequest()->getParam('store')!=''){
			$storeId = Mage::app()->getRequest()->getParam('store');
		}else{
			$storeId = 0;
		}
		
		$clipartcategorytitle = Mage::getModel('clipartmanagement/clipartcategorytitle')->getCollection()
										->addFieldToFilter('clipart_category_id',$this->getRequest()->getParam('id'))
										->addFieldToFilter('store_id',$storeId);		
		
		if($clipartcategorytitle->getSize() > 0)
		{
			$data = Mage::registry('clipartmanagement_data')->getData();
			$data['category_name'] = $clipartcategorytitle->getFirstItem()->getClipartCategoryName();			
		}else{		
			$clipartcategorytitle = Mage::getModel('clipartmanagement/clipartcategorytitle')->getCollection()
										->addFieldToFilter('clipart_category_id',$this->getRequest()->getParam('id'))
										->addFieldToFilter('store_id',0);
			$data = Mage::registry('clipartmanagement_data')->getData();	
			$data['category_name'] = $clipartcategorytitle->getFirstItem()->getClipartCategoryName();					
		}	
		
          $form->setValues($data);
          // $form->setValues(Mage::registry('clipartmanagement_data')->getData());
      }
      return parent::_prepareForm();
  }
} ?>
