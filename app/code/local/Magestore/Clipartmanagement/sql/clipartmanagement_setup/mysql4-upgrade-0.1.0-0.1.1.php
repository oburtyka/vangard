<?php
$this->startSetup();
$this->run("CREATE TABLE IF NOT EXISTS {$this->getTable('clipart_category_title')} (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`clipart_category_id` int(11) NULL,
`store_id` int(11) DEFAULT 0,
`clipart_category_name` varchar(255) NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ; 
");
$this->run("CREATE TABLE IF NOT EXISTS {$this->getTable('clipart_management_title')} (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`clipart_id` int(11) NULL,
`store_id` int(11) DEFAULT 0,
`clipart_name` varchar(255) NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ; 
");
$this->endSetup();
?>