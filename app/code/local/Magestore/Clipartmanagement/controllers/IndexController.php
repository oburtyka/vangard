<?php
class Magestore_Clipartmanagement_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {	
		$this->loadLayout();     
		$this->renderLayout();
    }
	
	public function clipartCategoryAction()
	{
		$collection = Mage::getModel('clipartmanagement/clipartcategory')
				->getCollection()
				->AddFieldToFilter('parent_cat_id',0)
				->AddFieldToFilter('status', 1);
		$collection->setOrder("position", "ASC");
		$xmlString = '<?xml version="1.0" encoding="iso-8859-1"?><artCategory>';

		foreach($collection as $res)		
		{
			$clipartcollection = Mage::getModel('clipartmanagement/clipart')
								->getCollection()
								->AddFieldToFilter('c_category_id',$res->getClipart_cat_id())
								->AddFieldToFilter('status', 1);
			if($clipartcollection->count()>0)
			{
				$xmlString .= '	<category>';
				$xmlString .= '	<catName>'.$res->getCategory_name().'</catName>';
				$xmlString .= '	<catID>'.$res->getClipart_cat_id().'</catID>';
				$xmlString .= '	<position>'.$res->getPosition().'</position>';	
				$xmlString .= '	<type>'.'category'.'</type>';
				$xmlString .= '	</category>';			
			}
		}
		$xmlString .= '</artCategory>';
		header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: text/xml");
		header ("Content-Description: PHP/INTERBASE Generated Data" );
		echo $xmlString;
	}
	public function clipartAction()
    {
		$cid = $this->getRequest()->getParam('cid', false);
		$path = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
		
		$collection = Mage::getModel('clipartmanagement/clipart')
						->getCollection()
						->AddFieldToFilter('c_category_id',$cid)
						->AddFieldToFilter('status', 1);
		$collection->setOrder("position", "ASC");
		 
		$xmlString = '<?xml version="1.0" encoding="iso-8859-1"?><allArt>';
		foreach($collection as $res)		
		{
			$xmlString .= '	<art>';
			$xmlString .= '	<artName>'.$res->getClipart_name().'</artName>';
			$xmlString .= '	<artID>'.$res->getClipart_id().'</artID>';
			$xmlString .= '	<parentID>'.$res->getC_category_id().'</parentID>';
			$xmlString .= '	<thumbPath>'.$path.'/media/clipart/images/thumb/'.$res->getClipart_image().'</thumbPath>';		
			$xmlString .= '	<imagePath>'.$path.'/media/clipart/images/'.$res->getClipart_image().'</imagePath>';
			$xmlString .= '	<status>'.$res->getStatus().'</status>';
			$xmlString .= '	<position>'.$res->getPosition().'</position>';
			$xmlString .= '	<type>'.'art'.'</type>';
			$xmlString .= '	</art>';	
		}

		$xmlString .= '</allArt>';
		header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: text/xml");
		header ("Content-Description: PHP/INTERBASE Generated Data" );
		echo $xmlString;
	}
}