<?php
$installer = $this;
$installer->startSetup();

$installer->run("CREATE TABLE IF NOT EXISTS `{$this->getTable('save_design')}` (
  `design_id` int(11) NOT NULL auto_increment,
  `customer_id` int(11) NOT NULL,
  `products_id` int(11) NOT NULL,
  `design_name` varchar(255) NOT NULL,
  `front_image` varchar(255) NOT NULL,
  `back_image` varchar(255) NOT NULL,
  `left_image` varchar(255) NOT NULL,
  `right_image` varchar(255) NOT NULL,
  `save_string` longtext NOT NULL,
  `action` varchar(255) NOT NULL,
  PRIMARY KEY  (`design_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1");

$installer->run("CREATE TABLE IF NOT EXISTS `{$this->getTable('user_cliparts')}` (
  `id` int(11) NOT NULL auto_increment,
  `customer_id` int(11) NOT NULL,
  `imgname` varchar(200) NOT NULL,
  `vectorname` varchar(250) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1
");

if (!$installer->getConnection()->tableColumnExists($installer->getTable('sales_flat_quote_item'), 'front_image')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('sales_flat_quote_item'),
        'front_image',
        'varchar (255) NULL'
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('sales_flat_quote_item'), 'back_image')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('sales_flat_quote_item'),
        'back_image',
        'varchar (255) NULL'
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('sales_flat_quote_item'), 'left_image')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('sales_flat_quote_item'),
        'left_image',
        'varchar (255) NULL'
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('sales_flat_quote_item'), 'right_image')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('sales_flat_quote_item'),
        'right_image',
        'varchar (255) NULL'
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('sales_flat_quote_item'), 'namevalue')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('sales_flat_quote_item'),
        'namevalue',
        'varchar (255) NULL'
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('sales_flat_quote_item'), 'numbervalue')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('sales_flat_quote_item'),
        'numbervalue',
        'varchar (255) NULL'
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('sales_flat_quote_item'), 'savestr')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('sales_flat_quote_item'),
        'savestr',
        'LONGTEXT NULL'
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('sales_flat_quote_item'), 'sourcefiledata')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('sales_flat_quote_item'),
        'sourcefiledata',
        'LONGTEXT NULL'
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('sales_flat_quote_item'), 'pdfdata')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('sales_flat_quote_item'),
        'pdfdata',
        'LONGTEXT NULL'
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('sales_flat_quote_item'), 'xmlfile')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('sales_flat_quote_item'),
        'xmlfile',
        'LONGTEXT NULL'
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('sales_flat_quote_item'), 'comment')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('sales_flat_quote_item'),
        'comment',
        'TEXT'
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('sales_flat_quote_item'), 'totalcolor')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('sales_flat_quote_item'),
        'totalcolor',
        'decimal (10,2)'
    );
}

if (!$installer->getConnection()->tableColumnExists($installer->getTable('sales_flat_quote_item'), 'compare_id')) {
    $installer->getConnection()->addColumn(
        $installer->getTable('sales_flat_quote_item'),
        'compare_id',
        'varchar (255) NULL'
    );
}

$installer->run("CREATE TABLE IF NOT EXISTS `{$this->getTable('designtool_configarea')}` (
  `id` int(11) NOT NULL auto_increment,
  `product_id` int(11) NOT NULL,
  `fa_height` varchar(200) NULL,
  `fa_width` varchar(200) NULL,
  `fa_x` varchar(200) NULL,
  `fa_y` varchar(200) NULL,
  `ba_height` varchar(200) NULL,  
  `ba_width` varchar(200) NULL,
  `ba_x` varchar(200) NULL,
  `ba_y` varchar(200) NULL,
  `le_height` varchar(200) NULL,
  `le_width` varchar(200) NULL,
  `le_x` varchar(200) NULL,
  `le_y` varchar(200) NULL,
  `ri_height` varchar(200) NULL,
  `ri_width` varchar(200) NULL,
  `ri_x` varchar(200) NULL,
  `ri_y` varchar(200) NULL,
  `fa_output_width` varchar(200) NULL,
  `fa_output_height` varchar(200) NULL,
  `ba_output_width` varchar(200) NULL,
  `ba_output_height` varchar(200) NULL,
  `le_output_width` varchar(200) NULL,
  `le_output_height` varchar(200) NULL,
  `ri_output_width` varchar(200) NULL,
  `ri_output_height` varchar(200) NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1
");

$installer->endSetup();
?>