<?php

$installer = $this;

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');
$installer->startSetup();


//adding Designtool attribute set
$sNewSetName = 'Designtool';
$iCatalogProductEntityTypeId = (int) $setup->getEntityTypeId('catalog_product');

$oAttributeset = Mage::getModel('eav/entity_attribute_set')
    ->setEntityTypeId($iCatalogProductEntityTypeId)
    ->setAttributeSetName($sNewSetName);
//adding designtool attribute set based on default attribute set
if ($oAttributeset->validate()) {
    $oAttributeset
        ->save()
        ->initFromSkeleton($iCatalogProductEntityTypeId)
        ->save();
}
else {
    die('Attribute set with name ' . $sNewSetName . ' already exists.');
}
 	
// adding Designtool attribute group
// the attribute added will be displayed under the group/tab Designtool in product edit page
$setup->addAttributeGroup('catalog_product', 'Designtool', 'Design Tool Configuration', 1);


//add color attribute
$setup->addAttribute('catalog_product', 'color', array(
	'group'     	=> 'Designtool',
	'input'         => 'select',
    'type'          => 'text',
    'label'         => 'Color',
	'backend'       => '',
	'visible'       => 1,
	'required'		=> 0,
	'user_defined' => 1,
	'is_configurable' => 1, 
	'searchable' => 1,
	'filterable' => 0,
	'comparable'	=> 1,
	'visible_on_front' => 1,
	'visible_in_advanced_search'  => 0,
	'is_html_allowed_on_front' => 0,
	'apply_to' => array('simple'),
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,	
)); 


//add size attribute
$setup->addAttribute('catalog_product', 'size', array(
	'group'     	=> 'Designtool',
	'input'         => 'select',
    'type'          => 'text',
    'label'         => 'Size',
	'backend'       => '',
	'visible'       => 1,
	'required'		=> 0,
	'user_defined' => 1,
	'is_configurable' => 1, 
	'searchable' => 1,
	'filterable' => 0,
	'comparable'	=> 1,
	'visible_on_front' => 1,
	'visible_in_advanced_search'  => 0,
	'is_html_allowed_on_front' => 0,
	'apply_to' => array('simple'),
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,	
)); 

//add is_customizable attribute
$setup->addAttribute('catalog_product', 'is_customizable', array(
	'group'     	=> 'Designtool',
	'input'         => 'boolean',
    'type'          => 'text',
    'label'         => 'Customizable Product',
	'backend'       => '',
	'visible'       => 1,
	'required'		=> 0,	
	'user_defined' => 1,
	'is_configurable' => 0, 
	'searchable' => 1,
	'filterable' => 0,
	'comparable'	=> 1,
	'visible_on_front' => 1,
	'visible_in_advanced_search'  => 0,
	'is_html_allowed_on_front' => 0,	
	'used_in_product_listing'   => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));

$setup->updateAttribute('catalog_product', 'is_customizable', 'apply_to', 'configurable');
$setup->updateAttribute('catalog_product', 'is_customizable', 'used_in_product_listing', 1);
$setup->addAttributeToSet('catalog_product', 'Designtool', 'Designtool', 'is_customizable', 10);

//add multicolor attribute
$setup->addAttribute('catalog_product', 'multicolor', array(
	'group'     	=> 'Designtool',
	'input'         => 'boolean',
    'type'          => 'text',
    'label'         => 'Add Multicolor functionality',
	'backend'       => '',
	'visible'       => 1,
	'required'		=> 0,	
	'apply_to'      => 'configurable',
	'user_defined' => 1,
	'is_configurable' => 0, 
	'searchable' => 1,
	'filterable' => 0,
	'comparable'	=> 1,
	'visible_on_front' => 1,
	'visible_in_advanced_search'  => 0,
	'is_html_allowed_on_front' => 0,	
	'used_in_product_listing'   => 0,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));

$setup->updateAttribute('catalog_product', 'multicolor', 'apply_to', 'configurable');
$setup->addAttributeToSet('catalog_product', 'Designtool', 'Designtool', 'multicolor', 20);

//add no_of_sides attribute
$setup->addAttribute('catalog_product', 'no_of_sides', array(
	'group'     	=> 'Designtool',
	'input'         => 'select',
    'type'          => 'text',
    'label'         => 'No Of Sides',
	'backend'       => '',
	'visible'       => 1,
	'required'		=> 0,	
	'apply_to'      => 'configurable',
	'user_defined' => 1,
	'is_configurable' => 0, 
	'searchable' => 1,
	'filterable' => 0,
	'option'            => array (
                                            'value' => array('optionone' => array('1'),
                                                             'optiontwo' => array('2'),
                                                             'optionthree' => array('3'),
															 'optionfour' => array('4'),
                                                        )
                                        ),
	'comparable'	=> 1,
	'visible_on_front' => 1,
	'visible_in_advanced_search'  => 0,
	'is_html_allowed_on_front' => 0,	
	'used_in_product_listing'   => 0,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));
$setup->updateAttribute('catalog_product', 'no_of_sides', 'is_configurable', 0);
$setup->updateAttribute('catalog_product', 'no_of_sides', 'apply_to', 'configurable');
$setup->addAttributeToSet('catalog_product', 'Designtool', 'Designtool', 'no_of_sides', 30);

//add color_image attribute
$setup->addAttribute('catalog_product', 'color_image', array(	
	'input'         => 'media_image',
    'type'          => 'varchar',
    'label'         => 'Color Image',
	'backend'       => '',
	'visible'       => 1,
	'required'		=> 0,		
	'user_defined' => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,	
));
$setup->addAttributeToSet('catalog_product', 'Designtool', 'Images', 'color_image', 6);
$setup->updateAttribute('catalog_product', 'color_image', 'apply_to', 'simple');

//add front_image attribute
$setup->addAttribute('catalog_product', 'front_image', array(	
	'input'         => 'media_image',
    'type'          => 'varchar',
    'label'         => 'Front Image',
	'backend'       => '',
	'visible'       => 1,
	'required'		=> 0,		
	'user_defined' => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,	
));
$setup->addAttributeToSet('catalog_product', 'Designtool', 'Images', 'front_image', 7);

//add back_image attribute
$setup->addAttribute('catalog_product', 'back_image', array(	
	'input'         => 'media_image',
    'type'          => 'varchar',
    'label'         => 'Back Image',
	'backend'       => '',
	'visible'       => 1,
	'required'		=> 0,		
	'user_defined' => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,	
));
$setup->addAttributeToSet('catalog_product', 'Designtool', 'Images', 'back_image', 8);

//add left_image attribute
$setup->addAttribute('catalog_product', 'left_image', array(	
	'input'         => 'media_image',
    'type'          => 'varchar',
    'label'         => 'Left Image',
	'backend'       => '',
	'visible'       => 1,
	'required'		=> 0,		
	'user_defined' => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,	
));
$setup->addAttributeToSet('catalog_product', 'Designtool', 'Images', 'left_image', 9);

//add right_image attribute
$setup->addAttribute('catalog_product', 'right_image', array(	
	'input'         => 'media_image',
    'type'          => 'varchar',
    'label'         => 'Right Image',
	'backend'       => '',
	'visible'       => 1,
	'required'		=> 0,		
	'user_defined' => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,	
));
$setup->addAttributeToSet('catalog_product', 'Designtool', 'Images', 'right_image', 10);

//add is_name_number attribute
$setup->addAttribute('catalog_product', 'is_name_number', array(
	'group'     	=> 'Designtool',
	'input'         => 'boolean',
    'type'          => 'text',
    'label'         => 'Enable Name/Number',
	'backend'       => '',
	'visible'       => 1,
	'required'		=> 0,	
	'user_defined' => 1,
	'is_configurable' => 0, 
	'searchable' => 1,
	'filterable' => 0,
	'comparable'	=> 1,
	'visible_on_front' => 1,
	'visible_in_advanced_search'  => 0,
	'is_html_allowed_on_front' => 0,	
	'used_in_product_listing'   => 1,
    'global'        => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
));

$setup->updateAttribute('catalog_product', 'is_name_number', 'apply_to', 'configurable');
$setup->updateAttribute('catalog_product', 'is_name_number', 'used_in_product_listing', 1);
$setup->addAttributeToSet('catalog_product', 'Designtool', 'Designtool', 'is_name_number', 15);

$installer->endSetup();