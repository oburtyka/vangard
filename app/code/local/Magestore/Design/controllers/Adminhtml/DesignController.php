<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Mage
 * @package    Mage_Sendfriend
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Magestore_Design_Adminhtml_DesignController extends Mage_Adminhtml_Controller_Action
{
    var $xmlString;
	/**
     * Initialize product instance
     *
     */  
    protected function _getSession()
    {
        return Mage::getSingleton('customer/session');
    }
	public function indexAction()
    {   		
		echo "ajay";
		$this->xmlString = '';
		$this->loadLayout();     
		$this->renderLayout();

    }	
	
	public function productAction()
	{
		$categoryId = $this->getRequest()->getParam('cid', false);
		$productId = $this->getRequest()->getParam('pid', false);
		$user = $this->getRequest()->getParam('user', false);
		
		if($categoryId!='')
		{
			header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
			header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
			header ("Cache-Control: no-cache, must-revalidate");
			header ("Pragma: no-cache");
			header ("Content-type: text/xml");
			header ("Content-Description: PHP/INTERBASE Generated Data" );
			echo Mage::getModel('design/design')->getProductsFromCategory($categoryId);
		}
		else
		{
			header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
			header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
			header ("Cache-Control: no-cache, must-revalidate");
			header ("Pragma: no-cache");
			header ("Content-type: text/xml");
			header ("Content-Description: PHP/INTERBASE Generated Data" );
			// echo Mage::getModel('design/design')->getProductFromId($productId,$user);
			$product = Mage::getModel('catalog/product')->load($productId);
			$productType = $product->getTypeID();
			//Simple Product    
			if($productType == 'simple')
			{   
			  echo Mage::getModel('design/design')->getSimpleProductFromId($productId,$user);
			}                           
			//Configurable Product
			if($productType == 'configurable')
			{   
			  echo Mage::getModel('design/design')->getConfigurableProductFromId($productId,$user);
			}
		}
	}
	
	public function categoryAction()
	{
		header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: text/xml");
		header ("Content-Description: PHP/INTERBASE Generated Data" );
		$this->renderChildrenCategory(Mage::app()->getStore()->getRootCategoryId());
		echo '<?xml version="1.0" encoding="iso-8859-1"?>';
		echo '<allCategory>';		
		echo $this->xmlString;		
		echo "</allCategory>";

	}
	
	public function renderChildrenCategory($catId)
	{		
		$children = Mage::getModel('catalog/category')->load($catId)->getChildren();
		
		if(!empty($children)) {
			$categories = explode(',', $children);
		
			foreach($categories as $categoryId) {
				$category = Mage::getModel('catalog/category')->load($categoryId);
				//echo '<category label="'.$category->getName().'" catName="'.$category->getName().'" orderNo="'.$category->getPosition().'" type="subcategory" catID="'.$category->getId().'" >';
				$this->xmlString .= '<category>';
				$this->xmlString .= '<catName>'.$category->getName().'</catName>';
				$this->xmlString .=  '<catID>'.$category->getId().'</catID>';
				$this->xmlString .=  '<orderNo>'.$category->getPosition().'</orderNo>';
				$this->xmlString .=  '<catDesc>'.$category->getDescription().'</catDesc>';		
				$this->xmlString .= Mage::getModel("catalog/category")->load($category->getId())->getImage();
				if( $categoryImage!='')
					$this->xmlString .=  '<catThumb>'.$path.'/media/catalog/category/'. $categoryImage.'</catThumb>';
					
				$this->xmlString .=  '<type>'.'subcategory'.'</type>';
				$this->renderChildrenCategory($categoryId);
				$this->xmlString .= "</category>";
			}
		
		}				
		//echo $this->xmlString;
		
	}
	public function getConfigAction()
	{
		$facebook = Mage::getStoreConfig('designtool_options/setup_app_config/facebook');
		$flickr = Mage::getStoreConfig('designtool_options/setup_app_config/flickr');
		$namePrice =(float) Mage::getStoreConfig('designtool_options/name_number_price/name_price');	
		$numberPrice = (float)Mage::getStoreConfig('designtool_options/name_number_price/number_price');		
		$symbol =  Mage::app()->getLocale()->currency(Mage::app()->getStore()->getCurrentCurrencyCode())->getSymbol(); 	
		$xmlString = '<?xml version="1.0" encoding="iso-8859-1"?><configuration>';
		$xmlString .= '	<facebook>'.$facebook.'</facebook>';
		$xmlString .= '	<flickr>'.$flickr.'</flickr>';
		$xmlString .= '	<nameprice>'.Mage::helper('core')->currency($namePrice,true,false).'</nameprice>';
		$xmlString .= '	<numberprice>'.Mage::helper('core')->currency($numberPrice,true,false).'</numberprice>';		
		$xmlString .= '</configuration>';
		header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
		header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
		header ("Cache-Control: no-cache, must-revalidate");
		header ("Pragma: no-cache");
		header ("Content-type: text/xml");
		header ("Content-Description: PHP/INTERBASE Generated Data" );
		echo $xmlString;
	}
	public function getPriceAction()
	{		
		$flashvars = $this->getRequest()->getParam('flashvars', false);
		if(isset($flashvars)){			
			$xml = simplexml_load_string($flashvars);				
			$totalQty = $xml->total_qty;	
			$colorId = $xml->colorid;
			$usedColorCounter = 0;
			$usedColorCounter = $xml->noofcolor->front + $xml->noofcolor->back + $xml->noofcolor->left + $xml->noofcolor->right;
			$productModel = Mage::getModel('catalog/product');
			$colorAttribute = $productModel->getResource()->getAttribute("color");
			if ($colorAttribute->usesSource()) {
					$colorLabel = $colorAttribute->getSource()->getOptionText($colorId);
					$colorName = explode('(', $colorLabel);			
					$colorName = $colorName[0];
			}
			
			$product = Mage::getModel('catalog/product')->load($xml->prodid);
			$configurableProduct = Mage::getModel('catalog/product_type_configurable')->setProduct($product);
			
			$size = $product->getResource()->getAttribute('size');
			$size_id = $size->getAttributeId();
			$finalPrice = 0;
			$total_cost = 0.0;
			$qcPrice = 0;
			$totalQcPrice = 0;
			$namePrice = 0;
			$numberPrice = 0;
			$i = 0;	
			
			foreach($xml->sizes->size as $size)
			{			
				$qty = $size->quantity;
				$productId = $size->productid;		
				
				$sizeId = $size->optionid;
				$sizeAttribute = $productModel->getResource()->getAttribute("size");
				
				if ($sizeAttribute->usesSource()) {
					$sizeName = $sizeAttribute->getSource()->getOptionText($sizeId);
				}
				$child = Mage::getModel('catalog/product')->load($productId);
				
				$availableQty = $child->getStockItem()->getQty();
				//echo "available qty".$availableQty."<br />";
				$minSaleQty = $child->getStockItem()->getMinSaleQty();
				//echo "min qty".$minSaleQty."<br />";
				$maxSaleQty = $child->getStockItem()->getMaxSaleQty();
				$isConfigSetting = $child->getStockItem()->getUseConfigMaxSaleQty();
				//echo $isConfigSetting."<br />";		
				if($isConfigSetting==0)
				{
					if($qty > $maxSaleQty)
					{
						$message = 'The maximum quantity allowed for Color '.$colorName.' and Size '.$sizeName.' for purchase is '.$maxSaleQty;				
					}
				}
				if($qty > $availableQty)
				{
					$message = 'Quantity '.$qty.' is not available for Color '.$colorName.' and Size '.$sizeName;			
				}
				
				if($qty < $minSaleQty)
				{
					$message = "Minimum sale quantity for Color ".$colorName." and Size ".$sizeName." is ".$minSaleQty;
				}	
				
				$finalPrice = Mage::getModel('design/design')->getFinalPrice($qty,$product,$colorId,$sizeId);

				$finalPrice = $finalPrice * $qty;		
				$total_cost = $total_cost + $finalPrice;	
				
				$qcPrice = Mage::getModel('qcprice/qcprice')->getQcPrice($usedColorCounter,$qty);				
				$totalQcPrice = $totalQcPrice + ($qcPrice*$qty);
					
			}	
			/*For Name Number Price*/
			$nameFixPrice =(float) Mage::getStoreConfig('designtool_options/name_number_price/name_price');	
			$numberFixPrice = (float)Mage::getStoreConfig('designtool_options/name_number_price/number_price');
			$namesCount = $xml->names;	
			$numbersCount = $xml->numbers;	
			$total_cost = $total_cost + $totalQcPrice + $namesCount*$nameFixPrice + $numbersCount*$numberFixPrice;
			echo "ttotal=".Mage::helper('core')->currency($total_cost,true,false);
			echo "&printingprice=".Mage::helper('core')->currency($totalQcPrice,true,false);
			echo "&totalquantity=".$xml->total_qty;
			echo "&totalusedcolors=".$usedColorCounter;
			echo "&namePrice=".$namePrice;
			echo "&numberPrice=".$numberPrice;
			if($xml->total_qty == 0)
				echo "&eachshirt=0";
			else	
				echo "&eachshirt=".Mage::helper('core')->currency($total_cost/$xml->total_qty,true,false);
			echo "&msg=".$message;			
		}
	}
	
	public function getUserdesignAction()
	{
		$path = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
		Mage::getSingleton("core/session", array("name" => "frontend"));
		$session = Mage::getSingleton("customer/session");
		$customer_email = $session->getCustomer()->getEmail();
		$customer_id = $session->getCustomerId();

		$collection  = Mage::getModel('design/savedesign')
						->getCollection()
						->AddFieldToFilter('customer_id',$customer_id);
						//->AddFieldToFilter('design_name',array('nlike'=>'share%'));
		 
		 if($customer_email != "")
		{
			$xmlString = '<?xml version="1.0" encoding="iso-8859-1"?><AllUserDesign>';
			foreach($collection as $res)		
			{
				$xmlString .= '	<Userdesign>';	
				$xmlString .= '	<UserDesignName>'.$res->getDesignName().'</UserDesignName>';
				$xmlString .= '	<UserDesignID>'.$res->getDesignId().'</UserDesignID>';
				
				$xmlString .= '	<UserFrontImagePath>'.$path.'designtool/saveimg/'.$res->getFrontImage().'</UserFrontImagePath>';
				
				if($res->getBackImage()!= "")
					$xmlString .= '	<UserBackImagePath>'.$path.'designtool/saveimg/'.$res->getBackImage().'</UserBackImagePath>';
				
				if($res->getLeftImage()!= "")
					$xmlString .= '	<UserLeftImagePath>'.$path.'designtool/saveimg/'.$res->getLeftImage().'</UserLeftImagePath>';
				
				if($res->getLeftImage()!= "")	
					$xmlString .= '	<UserRightImagePath>'.$path.'designtool/saveimg/'.$res->getRightImage().'</UserRightImagePath>';
						
				$xmlString .= '	<UserXmlData>'.$res->getSaveString().'</UserXmlData>';
				$xmlString .= '	</Userdesign>';	
			}
			$xmlString .= '</AllUserDesign>';
			
			header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
			header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
			header ("Cache-Control: no-cache, must-revalidate");
			header ("Pragma: no-cache");
			header ("Content-type: text/xml");
			header ("Content-Description: PHP/INTERBASE Generated Data" );
			echo $xmlString;
		}else{
			echo "Please check the login detail";
		}
	}
	
} ?>