<?php 
/**
 * Designnbuy_Texturemanagement extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Texturemanagement
 * @copyright  	Copyright (c) 2013
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * module base admin controller
 *
 * @category	Designnbuy
 * @package		Designnbuy_Texturemanagement
 * @author Ultimate Module Creator
 */
class Designnbuy_Texturemanagement_Controller_Adminhtml_Texturemanagement extends Mage_Adminhtml_Controller_Action{
	/**
	 * upload file and get the uploaded name
	 * @access public
	 * @param string $input
	 * @param string $destinationFolder
	 * @param array $data
	 * @return string
	 * @author Ultimate Module Creator
	 */
	protected function _uploadAndGetName($input, $destinationFolder, $data){
		try{
			if (isset($data[$input]['delete'])){
				return '';
			}
			else{
				$uploader = new Varien_File_Uploader($input);
				$uploader->setAllowRenameFiles(true);
				$uploader->setFilesDispersion(false);
				$uploader->setAllowCreateFolders(false);
				$result = $uploader->save($destinationFolder);
				$imageUrl = $destinationFolder.$result['file'];				
				$imageResized = Mage::getBaseDir('media').DS.'texture'.DS.'resize'. DS . $result['file'];				
				
				if (!file_exists($imageResized)&&file_exists($imageUrl)) :
					$imageObj = new Varien_Image($imageUrl);
					$imageObj->constrainOnly(TRUE);
					$imageObj->keepAspectRatio(TRUE);
					$imageObj->keepFrame(FALSE);
					$imageObj->resize(26, 26);
					$imageObj->save($imageResized);
				endif;	
				return $result['file'];
			}
		}
		catch (Exception $e){
			if ($e->getCode() != Varien_File_Uploader::TMP_NAME_EMPTY){
				throw $e;
			}
			else{
				if (isset($data[$input]['value'])){
					return $data[$input]['value'];
				}
			}
		}
		return '';
	}
}