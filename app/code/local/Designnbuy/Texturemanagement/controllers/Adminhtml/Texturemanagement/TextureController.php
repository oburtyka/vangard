<?php
/**
 * Designnbuy_Texturemanagement extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Texturemanagement
 * @copyright  	Copyright (c) 2013
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Texture admin controller
 *
 * @category	Designnbuy
 * @package		Designnbuy_Texturemanagement
 * @author Ultimate Module Creator
 */
class Designnbuy_Texturemanagement_Adminhtml_Texturemanagement_TextureController extends Designnbuy_Texturemanagement_Controller_Adminhtml_Texturemanagement{
	/**
	 * init the texture
	 * @access protected
	 * @return Designnbuy_Texturemanagement_Model_Texture
	 */
	protected function _initTexture(){
		$textureId  = (int) $this->getRequest()->getParam('id');
		$texture	= Mage::getModel('texturemanagement/texture');
		if ($textureId) {
			$texture->load($textureId);
		}
		Mage::register('current_texture', $texture);
		return $texture;
	}
 	/**
	 * default action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function indexAction() {
		$this->loadLayout();
		$this->_title(Mage::helper('texturemanagement')->__('Texturemanagement'))
			 ->_title(Mage::helper('texturemanagement')->__('Textures'));
		$this->renderLayout();
	}
	/**
	 * grid action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function gridAction() {
		$this->loadLayout()->renderLayout();
	}
	/**
	 * edit texture - action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function editAction() {
		$textureId	= $this->getRequest()->getParam('id');
		$texture  	= $this->_initTexture();
		if ($textureId && !$texture->getId()) {
			$this->_getSession()->addError(Mage::helper('texturemanagement')->__('This texture no longer exists.'));
			$this->_redirect('*/*/');
			return;
		}
		$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
		if (!empty($data)) {
			$texture->setData($data);
		}
		Mage::register('texture_data', $texture);
		$this->loadLayout();
		$this->_title(Mage::helper('texturemanagement')->__('Texturemanagement'))
			 ->_title(Mage::helper('texturemanagement')->__('Textures'));
		if ($texture->getId()){
			$this->_title($texture->getTextureName());
		}
		else{
			$this->_title(Mage::helper('texturemanagement')->__('Add texture'));
		}
		if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) { 
			$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true); 
		}
		$this->renderLayout();
	}
	/**
	 * new texture action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function newAction() {
		$this->_forward('edit');
	}
	/**
	 * save texture - action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function saveAction() {
		if ($data = $this->getRequest()->getPost('texture')) {
			try {
				$texture = $this->_initTexture();
				$texture->addData($data);
				$texture_imageName = $this->_uploadAndGetName('texture_image', Mage::helper('texturemanagement/texture_image')->getImageBaseDir(), $data);
				$texture->setData('texture_image', $texture_imageName);
				$texture->save();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('texturemanagement')->__('Texture was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);
				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $texture->getId()));
					return;
				}
				$this->_redirect('*/*/');
				return;
			} 
			catch (Mage_Core_Exception $e){
				if (isset($data['texture_image']['value'])){
					$data['texture_image'] = $data['texture_image']['value'];
				}
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				Mage::getSingleton('adminhtml/session')->setFormData($data);
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				return;
			}
			catch (Exception $e) {
				Mage::logException($e);
				if (isset($data['texture_image']['value'])){
					$data['texture_image'] = $data['texture_image']['value'];
				}
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('texturemanagement')->__('There was a problem saving the texture.'));
				Mage::getSingleton('adminhtml/session')->setFormData($data);
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				return;
			}
		}
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('texturemanagement')->__('Unable to find texture to save.'));
		$this->_redirect('*/*/');
	}
	/**
	 * delete texture - action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0) {
			try {
				$texture = Mage::getModel('texturemanagement/texture');
				$texture->setId($this->getRequest()->getParam('id'))->delete();
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('texturemanagement')->__('Texture was successfully deleted.'));
				$this->_redirect('*/*/');
				return; 
			}
			catch (Mage_Core_Exception $e){
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
			catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('texturemanagement')->__('There was an error deleteing texture.'));
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				Mage::logException($e);
				return;
			}
		}
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('texturemanagement')->__('Could not find texture to delete.'));
		$this->_redirect('*/*/');
	}
	/**
	 * mass delete texture - action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function massDeleteAction() {
		$textureIds = $this->getRequest()->getParam('texture');
		if(!is_array($textureIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('texturemanagement')->__('Please select textures to delete.'));
		}
		else {
			try {
				foreach ($textureIds as $textureId) {
					$texture = Mage::getModel('texturemanagement/texture');
					$texture->setId($textureId)->delete();
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('texturemanagement')->__('Total of %d textures were successfully deleted.', count($textureIds)));
			}
			catch (Mage_Core_Exception $e){
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
			catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('texturemanagement')->__('There was an error deleteing textures.'));
				Mage::logException($e);
			}
		}
		$this->_redirect('*/*/index');
	}
	/**
	 * mass status change - action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function massStatusAction(){
		$textureIds = $this->getRequest()->getParam('texture');
		if(!is_array($textureIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('texturemanagement')->__('Please select textures.'));
		} 
		else {
			try {
				foreach ($textureIds as $textureId) {
				$texture = Mage::getSingleton('texturemanagement/texture')->load($textureId)
							->setStatus($this->getRequest()->getParam('status'))
							->setIsMassupdate(true)
							->save();
				}
				$this->_getSession()->addSuccess($this->__('Total of %d textures were successfully updated.', count($textureIds)));
			}
			catch (Mage_Core_Exception $e){
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
			catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('texturemanagement')->__('There was an error updating textures.'));
				Mage::logException($e);
			}
		}
		$this->_redirect('*/*/index');
	}
	/**
	 * export as csv - action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function exportCsvAction(){
		$fileName   = 'texture.csv';
		$content	= $this->getLayout()->createBlock('texturemanagement/adminhtml_texture_grid')->getCsv();
		$this->_prepareDownloadResponse($fileName, $content);
	}
	/**
	 * export as MsExcel - action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function exportExcelAction(){
		$fileName   = 'texture.xls';
		$content	= $this->getLayout()->createBlock('texturemanagement/adminhtml_texture_grid')->getExcelFile();
		$this->_prepareDownloadResponse($fileName, $content);
	}
	/**
	 * export as xml - action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function exportXmlAction(){
		$fileName   = 'texture.xml';
		$content	= $this->getLayout()->createBlock('texturemanagement/adminhtml_texture_grid')->getXml();
		$this->_prepareDownloadResponse($fileName, $content);
	}
}