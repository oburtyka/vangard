<?php 
/**
 * Designnbuy_Texturemanagement extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Texturemanagement
 * @copyright  	Copyright (c) 2013
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Texturemanagement module install script
 *
 * @category	Designnbuy
 * @package		Designnbuy_Texturemanagement
 * @author Ultimate Module Creator
 */
$this->startSetup();
$table = $this->getConnection()
	->newTable($this->getTable('texturemanagement/texture'))
	->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'identity'  => true,
		'nullable'  => false,
		'primary'   => true,
		), 'Texture ID')
	->addColumn('texture_name', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
		'nullable'  => false,
		), 'Texture Name')

	->addColumn('texture_image', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
		), 'Texture Image')

	->addColumn('status', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		), 'Status')

	->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
		), 'Texture Creation Time')
	->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
		), 'Texture Modification Time')
	->setComment('Texture Table');
$this->getConnection()->createTable($table);
$this->endSetup();