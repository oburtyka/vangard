<?php
/**
 * Designnbuy_Texturemanagement extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Texturemanagement
 * @copyright  	Copyright (c) 2013
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Texture admin grid block
 *
 * @category	Designnbuy
 * @package		Designnbuy_Texturemanagement
 * @author Ultimate Module Creator
 */
class Designnbuy_Texturemanagement_Block_Adminhtml_Texture_Grid extends Mage_Adminhtml_Block_Widget_Grid{
	/**
	 * constructor
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function __construct(){
		parent::__construct();
		$this->setId('textureGrid');
		$this->setDefaultSort('entity_id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
		$this->setUseAjax(true);
	}
	/**
	 * prepare collection
	 * @access protected
	 * @return Designnbuy_Texturemanagement_Block_Adminhtml_Texture_Grid
	 * @author Ultimate Module Creator
	 */
	protected function _prepareCollection(){
		$collection = Mage::getModel('texturemanagement/texture')->getCollection();
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}
	/**
	 * prepare grid collection
	 * @access protected
	 * @return Designnbuy_Texturemanagement_Block_Adminhtml_Texture_Grid
	 * @author Ultimate Module Creator
	 */
	protected function _prepareColumns(){
		$this->addColumn('entity_id', array(
			'header'	=> Mage::helper('texturemanagement')->__('Id'),
			'index'		=> 'entity_id',
			'type'		=> 'number'
		));
		$this->addColumn('texture_name', array(
			'header'=> Mage::helper('texturemanagement')->__('Texture Name'),
			'index' => 'texture_name',
			'type'	 	=> 'text',

		));
		$this->addColumn('status', array(
			'header'	=> Mage::helper('texturemanagement')->__('Status'),
			'index'		=> 'status',
			'type'		=> 'options',
			'options'	=> array(
				'1' => Mage::helper('texturemanagement')->__('Enabled'),
				'0' => Mage::helper('texturemanagement')->__('Disabled'),
			)
		));		
		$this->addColumn('action',
			array(
				'header'=>  Mage::helper('texturemanagement')->__('Action'),
				'width' => '100',
				'type'  => 'action',
				'getter'=> 'getId',
				'actions'   => array(
					array(
						'caption'   => Mage::helper('texturemanagement')->__('Edit'),
						'url'   => array('base'=> '*/*/edit'),
						'field' => 'id'
					)
				),
				'filter'=> false,
				'is_system'	=> true,
				'sortable'  => false,
		));
		$this->addExportType('*/*/exportCsv', Mage::helper('texturemanagement')->__('CSV'));
		$this->addExportType('*/*/exportExcel', Mage::helper('texturemanagement')->__('Excel'));
		$this->addExportType('*/*/exportXml', Mage::helper('texturemanagement')->__('XML'));
		return parent::_prepareColumns();
	}
	/**
	 * prepare mass action
	 * @access protected
	 * @return Designnbuy_Texturemanagement_Block_Adminhtml_Texture_Grid
	 * @author Ultimate Module Creator
	 */
	protected function _prepareMassaction(){
		$this->setMassactionIdField('entity_id');
		$this->getMassactionBlock()->setFormFieldName('texture');
		$this->getMassactionBlock()->addItem('delete', array(
			'label'=> Mage::helper('texturemanagement')->__('Delete'),
			'url'  => $this->getUrl('*/*/massDelete'),
			'confirm'  => Mage::helper('texturemanagement')->__('Are you sure?')
		));
		$this->getMassactionBlock()->addItem('status', array(
			'label'=> Mage::helper('texturemanagement')->__('Change status'),
			'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
			'additional' => array(
				'status' => array(
						'name' => 'status',
						'type' => 'select',
						'class' => 'required-entry',
						'label' => Mage::helper('texturemanagement')->__('Status'),
						'values' => array(
								'1' => Mage::helper('texturemanagement')->__('Enabled'),
								'0' => Mage::helper('texturemanagement')->__('Disabled'),
						)
				)
			)
		));
		return $this;
	}
	/**
	 * get the row url
	 * @access public
	 * @param Designnbuy_Texturemanagement_Model_Texture
	 * @return string
	 * @author Ultimate Module Creator
	 */
	public function getRowUrl($row){
		return $this->getUrl('*/*/edit', array('id' => $row->getId()));
	}
	/**
	 * get the grid url
	 * @access public
	 * @return string
	 * @author Ultimate Module Creator
	 */
	public function getGridUrl(){
		return $this->getUrl('*/*/grid', array('_current'=>true));
	}
	/**
	 * after collection load
	 * @access protected
	 * @return Designnbuy_Texturemanagement_Block_Adminhtml_Texture_Grid
	 * @author Ultimate Module Creator
	 */
	 /*
	protected function _afterLoadCollection(){
		$this->getCollection()->walk('afterLoad');
		parent::_afterLoadCollection();
	}
	*/
	/**
	 * filter store column
	 * @access protected
	 * @param Designnbuy_Texturemanagement_Model_Resource_Texture_Collection $collection
	 * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
	 * @return Designnbuy_Texturemanagement_Block_Adminhtml_Texture_Grid
	 * @author Ultimate Module Creator
	 */
	 /*
	protected function _filterStoreCondition($collection, $column){
		if (!$value = $column->getFilter()->getValue()) {
        	return;
		}
		$collection->addStoreFilter($value);
		return $this;
    }*/
}