<?php
/**
 * Designnbuy_Texturemanagement extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Texturemanagement
 * @copyright  	Copyright (c) 2013
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Texture admin edit block
 *
 * @category	Designnbuy
 * @package		Designnbuy_Texturemanagement
 * @author Ultimate Module Creator
 */
class Designnbuy_Texturemanagement_Block_Adminhtml_Texture_Edit extends Mage_Adminhtml_Block_Widget_Form_Container{
	/**
	 * constuctor
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function __construct(){
		parent::__construct();
		$this->_blockGroup = 'texturemanagement';
		$this->_controller = 'adminhtml_texture';
		$this->_updateButton('save', 'label', Mage::helper('texturemanagement')->__('Save Texture'));
		$this->_updateButton('delete', 'label', Mage::helper('texturemanagement')->__('Delete Texture'));
		$this->_addButton('saveandcontinue', array(
			'label'		=> Mage::helper('texturemanagement')->__('Save And Continue Edit'),
			'onclick'	=> 'saveAndContinueEdit()',
			'class'		=> 'save',
		), -100);
		$this->_formScripts[] = "
			function saveAndContinueEdit(){
				editForm.submit($('edit_form').action+'back/edit/');
			}
		";
	}
	/**
	 * get the edit form header
	 * @access public
	 * @return string
	 * @author Ultimate Module Creator
	 */
	public function getHeaderText(){
		if( Mage::registry('texture_data') && Mage::registry('texture_data')->getId() ) {
			return Mage::helper('texturemanagement')->__("Edit Texture '%s'", $this->htmlEscape(Mage::registry('texture_data')->getTextureName()));
		} 
		else {
			return Mage::helper('texturemanagement')->__('Add Texture');
		}
	}
}