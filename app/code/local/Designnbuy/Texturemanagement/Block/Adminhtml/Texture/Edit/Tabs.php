<?php
/**
 * Designnbuy_Texturemanagement extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Texturemanagement
 * @copyright  	Copyright (c) 2013
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Texture admin edit tabs
 *
 * @category	Designnbuy
 * @package		Designnbuy_Texturemanagement
 * @author Ultimate Module Creator
 */
class Designnbuy_Texturemanagement_Block_Adminhtml_Texture_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs{
	/**
	 * constructor
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function __construct(){
		parent::__construct();
		$this->setId('texture_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('texturemanagement')->__('Texture'));
	}
	/**
	 * before render html
	 * @access protected
	 * @return Designnbuy_Texturemanagement_Block_Adminhtml_Texture_Edit_Tabs
	 * @author Ultimate Module Creator
	 */
	protected function _beforeToHtml(){
		$this->addTab('form_section', array(
			'label'		=> Mage::helper('texturemanagement')->__('Texture'),
			'title'		=> Mage::helper('texturemanagement')->__('Texture'),
			'content' 	=> $this->getLayout()->createBlock('texturemanagement/adminhtml_texture_edit_tab_form')->toHtml(),
		));		
		return parent::_beforeToHtml();
	}
}