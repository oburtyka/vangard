<?php
/**
 * Designnbuy_Texturemanagement extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Texturemanagement
 * @copyright  	Copyright (c) 2013
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Texture edit form tab
 *
 * @category	Designnbuy
 * @package		Designnbuy_Texturemanagement
 * @author Ultimate Module Creator
 */
class Designnbuy_Texturemanagement_Block_Adminhtml_Texture_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form{	
	/**
	 * prepare the form
	 * @access protected
	 * @return Texturemanagement_Texture_Block_Adminhtml_Texture_Edit_Tab_Form
	 * @author Ultimate Module Creator
	 */
	protected function _prepareForm(){
		$form = new Varien_Data_Form();
		$form->setHtmlIdPrefix('texture_');
		$form->setFieldNameSuffix('texture');
		$this->setForm($form);
		$fieldset = $form->addFieldset('texture_form', array('legend'=>Mage::helper('texturemanagement')->__('Texture')));
		$fieldset->addType('image', Mage::getConfig()->getBlockClassName('texturemanagement/adminhtml_texture_helper_image'));
		
		$fieldset->addField('texture_name', 'text', array(
			'label' => Mage::helper('texturemanagement')->__('Texture Name'),
			'name'  => 'texture_name',
			'required'  => true,
			'class' => 'required-entry',

		));
		
		$fieldset->addField('texture_image', 'image', array(
			'label' => Mage::helper('texturemanagement')->__('Texture Image'),
			'name'  => 'texture_image'		
		));
		
		$fieldset->addField('status', 'select', array(
			'label' => Mage::helper('texturemanagement')->__('Status'),
			'name'  => 'status',
			'values'=> array(
				array(
					'value' => 1,
					'label' => Mage::helper('texturemanagement')->__('Enabled'),
				),
				array(
					'value' => 0,
					'label' => Mage::helper('texturemanagement')->__('Disabled'),
				),
			),
		));
		if (Mage::getSingleton('adminhtml/session')->getTextureData()){
			$form->setValues(Mage::getSingleton('adminhtml/session')->getTextureData());
			Mage::getSingleton('adminhtml/session')->setTextureData(null);
		}
		elseif (Mage::registry('current_texture')){
			$form->setValues(Mage::registry('current_texture')->getData());
		}
		return parent::_prepareForm();
	}
}