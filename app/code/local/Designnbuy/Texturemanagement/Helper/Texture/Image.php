<?php 
/**
 * Designnbuy_Texturemanagement extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Texturemanagement
 * @copyright  	Copyright (c) 2013
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Texture image helper
 *
 * @category	Designnbuy
 * @package		Designnbuy_Texturemanagement
 * @author Ultimate Module Creator
 */
class Designnbuy_Texturemanagement_Helper_Texture_Image extends Designnbuy_Texturemanagement_Helper_Image_Abstract{
	/**
	 * image placeholder
	 * @var string
	 */
	protected $_placeholder = 'images/placeholder/texture.jpg';
	/**
	 * image subdir
	 * @var string
	 */
	protected $_subdir      = 'texture';
}