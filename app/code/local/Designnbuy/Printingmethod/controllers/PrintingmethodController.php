<?php 
/**
 * Designnbuy_Printingmethod extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @copyright  	Copyright (c) 2014
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Printing Method front contrller
 *
 * @category	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @author Ultimate Module Creator
 */
class Designnbuy_Printingmethod_PrintingmethodController extends Mage_Core_Controller_Front_Action{
	/**
 	 * default action
 	 * @access public
 	 * @return void
 	 * @author Ultimate Module Creator
 	 */
 	public function indexAction(){
		$this->loadLayout();
 		if (Mage::helper('printingmethod/printingmethod')->getUseBreadcrumbs()){
			if ($breadcrumbBlock = $this->getLayout()->getBlock('breadcrumbs')){
				$breadcrumbBlock->addCrumb('home', array(
							'label'	=> Mage::helper('printingmethod')->__('Home'), 
							'link' 	=> Mage::getUrl(),
						)
				);
				$breadcrumbBlock->addCrumb('printingmethods', array(
							'label'	=> Mage::helper('printingmethod')->__('Printing Methods'), 
							'link'	=> '',
					)
				);
			}
		}
		$this->renderLayout();
	}
	/**
 	 * view printing method action
 	 * @access public
 	 * @return void
 	 * @author Ultimate Module Creator
 	 */
	public function viewAction(){
		$printingmethodId 	= $this->getRequest()->getParam('id', 0);
		$printingmethod 	= Mage::getModel('printingmethod/printingmethod')
						->setStoreId(Mage::app()->getStore()->getId())
						->load($printingmethodId);
		if (!$printingmethod->getId()){
			$this->_forward('no-route');
		}
		elseif (!$printingmethod->getStatus()){
			$this->_forward('no-route');
		}
		else{
			Mage::register('current_printingmethod_printingmethod', $printingmethod);
			$this->loadLayout();
			if ($root = $this->getLayout()->getBlock('root')) {
				$root->addBodyClass('printingmethod-printingmethod printingmethod-printingmethod' . $printingmethod->getId());
			}
			if (Mage::helper('printingmethod/printingmethod')->getUseBreadcrumbs()){
				if ($breadcrumbBlock = $this->getLayout()->getBlock('breadcrumbs')){
					$breadcrumbBlock->addCrumb('home', array(
								'label'	=> Mage::helper('printingmethod')->__('Home'), 
								'link' 	=> Mage::getUrl(),
							)
					);
					$breadcrumbBlock->addCrumb('printingmethods', array(
								'label'	=> Mage::helper('printingmethod')->__('Printing Methods'), 
								'link'	=> Mage::helper('printingmethod')->getPrintingmethodsUrl(),
						)
					);
					$breadcrumbBlock->addCrumb('printingmethod', array(
								'label'	=> $printingmethod->getPrintingMethod(), 
								'link'	=> '',
						)
					);
				}
			}
			$this->renderLayout();
		}
	}
}