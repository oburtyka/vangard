<?php
/**
 * Designnbuy_Printingmethod extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @copyright  	Copyright (c) 2014
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Adminhtml observer
 *
 * @category	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @author Ultimate Module Creator
 */
class Designnbuy_Printingmethod_Model_Adminhtml_Observer{
	/**
	 * check if tab can be added
	 * @access protected
	 * @param Mage_Catalog_Model_Product $product
	 * @return bool
	 * @author Ultimate Module Creator
	 */
	protected function _canAddTab($product){
		if ($product->getId()){
			return true;
		}
		if (!$product->getAttributeSetId()){
			return false;
		}
		$request = Mage::app()->getRequest();
		if ($request->getParam('type') == 'configurable'){
			if ($request->getParam('attribtues')){
				return true;
			}
		}
		return false;
	}
	/**
	 * add the printingmethod tab to products
	 * @access public
	 * @param Varien_Event_Observer $observer
	 * @return Designnbuy_Printingmethod_Model_Adminhtml_Observer
	 * @author Ultimate Module Creator
	 */
	public function addPrintingmethodBlock($observer){
		$block = $observer->getEvent()->getBlock();
		$product = Mage::registry('product');
		if($product){
		$productType = $product->getTypeId();
		$Designtool = "Designtool";		
		$DesigntoolAttributeSetId = Mage::getModel('eav/entity_attribute_set')
                            ->load($Designtool, 'attribute_set_name')
                            ->getAttributeSetId();
		
		$Designer3D = "3DDesigner";		
		$Designer3DAttributeSetId = Mage::getModel('eav/entity_attribute_set')
                            ->load($Designer3D, 'attribute_set_name')
                            ->getAttributeSetId();
							
		$attributeSetModel = Mage::getModel("eav/entity_attribute_set");
		$attributeSetModel->load($product->getAttributeSetId());
		$attributeSetId  = $attributeSetModel->getAttributeSetId();
		}
		if ($block instanceof Mage_Adminhtml_Block_Catalog_Product_Edit_Tabs && $this->_canAddTab($product) && ($productType == 'configurable' || $productType == 'simple') && ($attributeSetId == $DesigntoolAttributeSetId || $attributeSetId == $Designer3DAttributeSetId)){
			$block->addTab('printingmethods', array(
				'label' => Mage::helper('printingmethod')->__('Printing Methods'),
				'url'   => Mage::helper('adminhtml')->getUrl('adminhtml/printingmethod_printingmethod_catalog_product/printingmethods', array('_current' => true)),
				'class' => 'ajax',
			));
		}
		return $this;
	}
	/**
	 * save printingmethod - product relation
	 * @access public
	 * @param Varien_Event_Observer $observer
	 * @return Designnbuy_Printingmethod_Model_Adminhtml_Observer
	 * @author Ultimate Module Creator
	 */
	public function savePrintingmethodData($observer){
		$post = Mage::app()->getRequest()->getPost('printingmethods', -1);
		if ($post != '-1') {
			$post = Mage::helper('adminhtml/js')->decodeGridSerializedInput($post);
			$product = Mage::registry('product');
			$printingmethodProduct = Mage::getResourceSingleton('printingmethod/printingmethod_product')->saveProductRelation($product, $post);
		}
		return $this;
	}}