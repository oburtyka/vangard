<?php
/**
 * Designnbuy_Printingmethod extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @copyright  	Copyright (c) 2014
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Printing Method model
 *
 * @category	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @author Ultimate Module Creator
 */
class Designnbuy_Printingmethod_Model_Printingmethod extends Mage_Core_Model_Abstract{
	/**
	 * Entity code.
	 * Can be used as part of method name for entity processing
	 */
	const ENTITY= 'printingmethod_printingmethod';
	const CACHE_TAG = 'printingmethod_printingmethod';
	/**
	 * Prefix of model events names
	 * @var string
	 */
	protected $_eventPrefix = 'printingmethod_printingmethod';
	
	/**
	 * Parameter name in event
	 * @var string
	 */
	protected $_eventObject = 'printingmethod';
	protected $_productInstance = null;
	/**
	 * constructor
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function _construct(){
		parent::_construct();
		$this->_init('printingmethod/printingmethod');
	}
	/**
	 * before save printing method
	 * @access protected
	 * @return Designnbuy_Printingmethod_Model_Printingmethod
	 * @author Ultimate Module Creator
	 */
	protected function _beforeSave(){
		parent::_beforeSave();
		$now = Mage::getSingleton('core/date')->gmtDate();
		if ($this->isObjectNew()){
			$this->setCreatedAt($now);
		}
		$this->setUpdatedAt($now);
		return $this;
	}
	/**
	 * get the url to the printing method details page
	 * @access public
	 * @return string
	 * @author Ultimate Module Creator
	 */
	public function getPrintingmethodUrl(){
		if ($this->getUrlKey()){
			return Mage::getUrl('', array('_direct'=>$this->getUrlKey()));
		}
		return Mage::getUrl('printingmethod/printingmethod/view', array('id'=>$this->getId()));
	}
	/**
	 * check URL key
	 * @access public
	 * @param string $urlKey
	 * @param bool $active
	 * @return mixed
	 * @author Ultimate Module Creator
	 */
	public function checkUrlKey($urlKey, $active = true){
		return $this->_getResource()->checkUrlKey($urlKey, $active);
	}
	/**
	 * save printingmethod relation
	 * @access public
	 * @return Designnbuy_Printingmethod_Model_Printingmethod
	 * @author Ultimate Module Creator
	 */
	protected function _afterSave() {
		$this->getProductInstance()->savePrintingmethodRelation($this);
		return parent::_afterSave();
	}
	/**
	 * get product relation model
	 * @access public
	 * @return Designnbuy_Printingmethod_Model_Printingmethod_Product
	 * @author Ultimate Module Creator
	 */
	public function getProductInstance(){
		if (!$this->_productInstance) {
			$this->_productInstance = Mage::getSingleton('printingmethod/printingmethod_product');
		}
		return $this->_productInstance;
	}
	/**
	 * get selected products array
	 * @access public
	 * @return array
	 * @author Ultimate Module Creator
	 */
	public function getSelectedProducts(){
		if (!$this->hasSelectedProducts()) {
			$products = array();
			foreach ($this->getSelectedProductsCollection() as $product) {
				$products[] = $product;
			}
			$this->setSelectedProducts($products);
		}
		return $this->getData('selected_products');
	}
	/**
	 * Retrieve collection selected products
	 * @access public
	 * @return Designnbuy_Printingmethod_Resource_Printingmethod_Product_Collection
	 * @author Ultimate Module Creator
	 */
	public function getSelectedProductsCollection(){
		$collection = $this->getProductInstance()->getProductCollection($this);
		return $collection;
	}
	
	public function getPrintingMethodFixedPrice($printingMethodId, $qty, $side){
		$price = 0;
		$resource = Mage::getSingleton('core/resource');
		$adapter = $resource->getConnection('core_read');					
		$fixedPricePrintingMethodTable = $resource->getTableName('printingmethod/fixedprice');		
		$qrangeTable = $resource->getTableName('qrange/qrange');
				
		$fixedPriceselect = $adapter->select()
			->from($fixedPricePrintingMethodTable, array('*'))
			->where('printing_method_id=?',$printingMethodId)
			->where('qr.quantity_range_from<=?',$qty)
			->where('qr.quantity_range_to>=?',$qty)			
			->join(array('qr'=>$qrangeTable), 'qty_range_id = qr.qrange_id', array('qr.*'));
		$fixedPriceData =$adapter->fetchRow($fixedPriceselect);
		if($fixedPriceData){
			if($side=='front'){
				$price = $fixedPriceData['first_side_price'];
			}
			if($side=='back'){
				$price = $fixedPriceData['second_side_price'];
			}
			if($side=='left'){
				$price = $fixedPriceData['third_side_price'];
			}
			if($side=='right'){
				$price = $fixedPriceData['fourth_side_price'];
			}
		}
		return $price;
	}
	
	public function getPrintingMethodQCPrice($printingMethodId, $noOfColors, $qty, $side){
		$price = 0;
		$resource = Mage::getSingleton('core/resource');
		$adapter = $resource->getConnection('core_read');					
		$qcPricePrintingMethodTable = $resource->getTableName('printingmethod/qcprice');
		$colorsTable = $resource->getTableName('colors/colors');
		$qrangeTable = $resource->getTableName('qrange/qrange');
				
		$qcPriceselect = $adapter->select()
			->from($qcPricePrintingMethodTable, array('*'))
			->where('printing_method_id=?',$printingMethodId)
			->where('qr.quantity_range_from<=?',$qty)
			->where('qr.quantity_range_to>=?',$qty)
			->where('clrs.colors_counter=?',$noOfColors)
			->join(array('qr'=>$qrangeTable), 'qty_range_id = qr.qrange_id', array('qr.*'))
			->join(array('clrs'=>$colorsTable), 'noofcolor = clrs.colors_id', array('clrs.*'));
		$qcPriceData =$adapter->fetchRow($qcPriceselect);
		
		if($qcPriceData){
			if($side=='front'){
				$price = $qcPriceData['first_side_price'];
			}
			if($side=='back'){
				$price = $qcPriceData['second_side_price'];
			}
			if($side=='left'){
				$price = $qcPriceData['third_side_price'];
			}
			if($side=='right'){
				$price = $qcPriceData['fourth_side_price'];
			}
		}
		return $price;
	}
	
	public function getPrintingMethodQAPrice($printingMethodId, $area, $qty, $side){
		
		$price = 0;
		$resource = Mage::getSingleton('core/resource');
		$adapter = $resource->getConnection('core_read');
		$writeConnection = $resource->getConnection('core_write');					
		$qaPricePrintingMethodTable = $resource->getTableName('printingmethod/qaprice');
		$dgareaTable = $resource->getTableName('dgarea/dgarea');
		$qrangeTable = $resource->getTableName('qrange/qrange');
				
		$qaPriceselect = $adapter->select()
			->from($qaPricePrintingMethodTable, array('*'))
			->where('printing_method_id=?',$printingMethodId)
			->where('qr.quantity_range_from<=?',$qty)
			->where('qr.quantity_range_to>=?',$qty)
			->where('ar.square_area>=?',$area)
			->join(array('qr'=>$qrangeTable), 'qty_range_id = qr.qrange_id', array('qr.*'))
			->join(array('ar'=>$dgareaTable), 'area = ar.dgarea_id', array('ar.*'))
			->order('ar.square_area ASC')
			->limit(1);
		$qaPriceData = $adapter->fetchRow($qaPriceselect);
		
		//echo "totalRows".$totalRows = $writeConnection->exec($qaPriceselect);
		
		$adapter->closeConnection();
		$writeConnection->closeConnection();
		
		if(empty($qaPriceData)){
		
		$adapter->getConnection();
		$qaPriceselect = $adapter->select()
			->from($qaPricePrintingMethodTable, array('*'))
			->where('printing_method_id=?',$printingMethodId)
			->where('qr.quantity_range_from<=?',$qty)
			->where('qr.quantity_range_to>=?',$qty)
			->where('ar.square_area<=?',$area)			
			->join(array('qr'=>$qrangeTable), 'qty_range_id = qr.qrange_id', array('qr.*'))
			->join(array('ar'=>$dgareaTable), 'area = ar.dgarea_id', array('ar.*'))
			->order('ar.square_area DESC')
			->limit(1);
			$qaPriceData = $adapter->fetchRow($qaPriceselect);			
		}
		
		if($qaPriceData){
			if($side=='front'){
				$price = $qaPriceData['first_side_price'];
			}
			if($side=='back'){
				$price = $qaPriceData['second_side_price'];
			}
			if($side=='left'){
				$price = $qaPriceData['third_side_price'];
			}
			if($side=='right'){
				$price = $qaPriceData['fourth_side_price'];
			}
		}	
		
		return $price;
	}
	
	public function getPrintingMethodArtWorkSetupFee($printingMethodId){
		$printingMethod = Mage::getModel('printingmethod/printingmethod')->load($printingMethodId);
		$artWorkSetUpType = $printingMethod->getArtworkSetupPriceType();		
	}
	
}