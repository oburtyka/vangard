<?php
class Designnbuy_Printingmethod_Model_Setupfee extends Varien_Object{
	const FEE_AMOUNT = 10;

	public static function getFee(){
		return self::FEE_AMOUNT;
	}
	public static function canApply($address){
		return true;
	}
	public static function getSetupfee($address){
		$quote = $address->getQuote();
		
						 
		 $quoteItemCollection = Mage::getModel("sales/quote_item")
				 ->getCollection()
				 ->setQuote($quote)
				 ->addFieldToFilter("quote_id", $quote->getId())
				 // ->addFieldToFilter("product_type","configurable")
				 ->addFieldToFilter("parent_item_id", array('null' => true)); 	
		
		$setupPrice = 0;
		$compareId = array();
		foreach($quoteItemCollection as $quoteItem){
			if($quoteItem->getPdfdata()){
				if (!in_array($quoteItem->getCompareId(), $compareId)) {					
					$xmlData = new Varien_Simplexml_Config($quoteItem->getPdfdata());
					$designData = new Varien_Object(current($xmlData)->asArray());		
					$customizedSides = $designData->getData('iscustomized');					
					$printMethodId = $designData->getData('printMethodId');
					$printingMethodData = Mage::getModel('printingmethod/printingmethod')->load($printMethodId);
					$customizedCount = 0;
					if(count($printingMethodData)>0){
						$artWorkSetUpType = $printingMethodData->getArtworkSetupPriceType();
						$artWorkSetUpPrice = $printingMethodData->getArtworkSetupPrice();
						$artWorkTotalPrice = 0;
						
						if($artWorkSetUpType == 1){						
							if($customizedSides['front'] == 'true')
								$customizedCount++;
							if($customizedSides['back'] == 'true')
								$customizedCount++;
							if($customizedSides['left'] == 'true')
								$customizedCount++;
							if($customizedSides['right'] == 'true')
								$customizedCount++;
						
						$setupPrice = $setupPrice + $printingMethodData->getArtworkSetupPrice()*$customizedCount;			
						}else{			
							$noofcolor = $designData->getData('noofcolor');		
							$totalNumberOfColors = intval($noofcolor['front'])+ intval($noofcolor['back']) + intval($noofcolor['left']) + intval($noofcolor['right']);						
							$setupPrice = $setupPrice + ($totalNumberOfColors * $printingMethodData->getArtworkSetupPrice());
						}
					}
					$compareId[] = $quoteItem->getCompareId();
				}
			}			
		}
		//echo "setupPrice".$setupPrice;
		//put here your business logic to check if fee should be applied or not
		//if($address->getAddressType() == 'billing'){
		return $setupPrice;
		//}
	}
}