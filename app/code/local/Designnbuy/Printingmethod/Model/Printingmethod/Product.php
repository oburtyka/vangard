<?php 
/**
 * Designnbuy_Printingmethod extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @copyright  	Copyright (c) 2014
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Printing Method product model
 *
 * @category	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @author Ultimate Module Creator
 */
class Designnbuy_Printingmethod_Model_Printingmethod_Product extends Mage_Core_Model_Abstract{
	/**
	 * Initialize resource
	 * @access protected
	 * @return void
	 * @author Ultimate Module Creator
	 */
	protected function _construct(){
		$this->_init('printingmethod/printingmethod_product');
	}
	/**
	 * Save data for printingmethod-product relation
	 * @access public
	 * @param  Designnbuy_Printingmethod_Model_Printingmethod $printingmethod
	 * @return Designnbuy_Printingmethod_Model_Printingmethod_Product
	 * @author Ultimate Module Creator
	 */
	public function savePrintingmethodRelation($printingmethod){
		$data = $printingmethod->getProductsData();
		if (!is_null($data)) {
			$this->_getResource()->savePrintingmethodRelation($printingmethod, $data);
		}
		return $this;
	}
	/**
	 * get products for printingmethod
	 * @access public
	 * @param Designnbuy_Printingmethod_Model_Printingmethod $printingmethod
	 * @return Designnbuy_Printingmethod_Model_Resource_Printingmethod_Product_Collection
	 * @author Ultimate Module Creator
	 */
	public function getProductCollection($printingmethod){
		$collection = Mage::getResourceModel('printingmethod/printingmethod_product_collection')
			->addPrintingmethodFilter($printingmethod);
		return $collection;
	}
}