<?php 
/**
 * Designnbuy_Printingmethod extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @copyright  	Copyright (c) 2014
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Printing Method list on product page block
 *
 * @category	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @author Ultimate Module Creator
 */
class Designnbuy_Printingmethod_Block_Catalog_Product_List_Printingmethod extends Mage_Catalog_Block_Product_Abstract{
	/**
	 * get the list of printingmethods
	 * @access protected
	 * @return Designnbuy_Printingmethod_Model_Resource_Printingmethod_Collection 
	 * @author Ultimate Module Creator
	 */
	public function getPrintingmethodCollection(){
		if (!$this->hasData('printingmethod_collection')){
			$product = Mage::registry('product');
			$collection = Mage::getResourceSingleton('printingmethod/printingmethod_collection')
				->addStoreFilter(Mage::app()->getStore())

				->addFilter('status', 1)
				->addProductFilter($product);
			$collection->getSelect()->order('related_product.position', 'ASC');
			$this->setData('printingmethod_collection', $collection);
		}
		return $this->getData('printingmethod_collection');
	}
}