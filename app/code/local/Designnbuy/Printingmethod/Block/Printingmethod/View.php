<?php 
/**
 * Designnbuy_Printingmethod extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @copyright  	Copyright (c) 2014
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Printing Method view block
 *
 * @category	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @author Ultimate Module Creator
 */
class Designnbuy_Printingmethod_Block_Printingmethod_View extends Mage_Core_Block_Template{
	/**
	 * get the current printing method
	 * @access public
	 * @return mixed (Designnbuy_Printingmethod_Model_Printingmethod|null)
	 * @author Ultimate Module Creator
	 */
	public function getCurrentPrintingmethod(){
		return Mage::registry('current_printingmethod_printingmethod');
	}
} 