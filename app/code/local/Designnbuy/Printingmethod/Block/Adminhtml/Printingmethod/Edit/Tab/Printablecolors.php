<?php 
/**
 * Designnbuy_Printingmethod extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @copyright  	Copyright (c) 2014
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * store selection tab
 *
 * @category	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @author Ultimate Module Creator
 */
class Designnbuy_Printingmethod_Block_Adminhtml_Printingmethod_Edit_Tab_Printablecolors extends Mage_Adminhtml_Block_Widget_Form{
	/**
	 * prepare the form
	 * @access protected
	 * @return Designnbuy_Printingmethod_Block_Adminhtml_Printingmethod_Edit_Tab_Stores
	 * @author Ultimate Module Creator
	 */
	public function __construct()
    {
        parent::__construct();
       
        $this->setTemplate('printingmethod/printablecolors.phtml');
    }   
   protected function _getProduct()
    {     
		//return Mage::registry('producttemplatecreator_data')->getProduct();		
    }
    
  
    
    public function getProductId()
    {
       
		
    }
    
    public function getPritingMethodId()
    {
        return $this->getRequest()->getParam('id');
    }
    
    public function getTabTitle()
    {
        return $this->__('Custom Options Images');
    }
   
    
    public function isHidden()
    {
        return ($this->getRequest()->getParam('store') != 0) ? true : false;
    }    
    
	
	/** options for every row, they will be rendered as dynamic row with inputs */
    public function getPrintableColorsValues()
    {       
		$values = array();
		$value = array();

		$printablecolorsCollection = Mage::getModel('printingmethod/printablecolors')
					->getCollection()
					->addFieldToFilter('printing_method_id',$this->getPritingMethodId())
					->setOrder('sort_order', 'DESC');
		foreach ($printablecolorsCollection as $printablecolors) {
		
		$value['id'] = $printablecolors->getId();
		$value['color_code'] = $printablecolors->getColorCode();
		$value['sort_order'] = $printablecolors->getSortOrder();			
			foreach ($this->getStores() as $store) {				
				$storeValues = $this->getStoreOptionValues($printablecolors->getId(),$store->getId());			
				$value['store' . $store->getId()] = isset($storeValues[$store->getId()]) ? 
											$storeValues[$store->getId()] : '';
			}
			$values[] = new Varien_Object($value);
		}
		
		
        return $values;
    }
	public function getStoreOptionValues($colorId, $storeId)
    {
		$printableColorData = Mage::getResourceModel('printingmethod/printablecolorstitle_collection')
												->addFieldToFilter('color_id',$colorId)
												->addFieldToFilter('store_id',$storeId);
		// echo "dssss<pre>";
			// print_r($printableColorData->getData());
		foreach ($printableColorData as $item) {
                $values[$item->getStoreId()] = $item->getTitle();
            }
		return $values;
		
	}
    /*protected function _getSelectOptionTypes()
    {
        return Mage::helper('swatches_customoptions')->getSelectOptionTypes();
    } */    
    /**
     * Determines whether to display the tab
     * Add logic here to decide whether you want the tab to display
     *
     * @return bool
     */
	//Code added to show only  configurable product configure area starts
    public function canShowTab()
    {    
		return true;		
    }
	
	public function getStores()
    {       
		$stores = Mage::getModel('core/store')
			->getResourceCollection()
			->setLoadDefault(true)
			->load();        
        return $stores;
    }
}