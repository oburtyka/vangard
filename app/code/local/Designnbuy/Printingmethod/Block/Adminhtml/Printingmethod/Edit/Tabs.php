<?php
/**
 * Designnbuy_Printingmethod extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @copyright  	Copyright (c) 2014
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Printing Method admin edit tabs
 *
 * @category	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @author Ultimate Module Creator
 */
class Designnbuy_Printingmethod_Block_Adminhtml_Printingmethod_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs{
	/**
	 * constructor
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function __construct(){
		parent::__construct();
		$this->setId('printingmethod_tabs');
		$this->setDestElementId('edit_form');
		$this->setTitle(Mage::helper('printingmethod')->__('Printing Method'));
	}
	/**
	 * before render html
	 * @access protected
	 * @return Designnbuy_Printingmethod_Block_Adminhtml_Printingmethod_Edit_Tabs
	 * @author Ultimate Module Creator
	 */
	protected function _beforeToHtml(){
		$this->addTab('form_section', array(
			'label'		=> Mage::helper('printingmethod')->__('Printing Method'),
			'title'		=> Mage::helper('printingmethod')->__('Printing Method'),
			'content' 	=> $this->getLayout()->createBlock('printingmethod/adminhtml_printingmethod_edit_tab_form')->toHtml(),
		));
		/*
		if (!Mage::app()->isSingleStoreMode()){
			$this->addTab('form_store_section', array(
				'label'		=> Mage::helper('printingmethod')->__('Store views'),
				'title'		=> Mage::helper('printingmethod')->__('Store views'),
				'content' 	=> $this->getLayout()->createBlock('printingmethod/adminhtml_printingmethod_edit_tab_stores')->toHtml(),
			));
		}
		*/
		
		if($this->getRequest()->getParam('id')){
			$printingmethod = Mage::getModel('printingmethod/printingmethod')->load($this->getRequest()->getParam('id'));
						
			if($printingmethod->getPrintableColors()==2){
				$this->addTab('printable_colors', array(
					'label' => Mage::helper('printingmethod')->__('Printable Colors'),					
					'content'   =>$this->getLayout()->createBlock('printingmethod/adminhtml_printingmethod_edit_tab_printablecolors')->toHtml(),
				));				
			}
			
			if($printingmethod->getPricingLogic()==1){
				$this->addTab('fixed_price', array(
						'label' => Mage::helper('printingmethod')->__('Fixed Price'),					
						'content'   =>$this->getLayout()->createBlock('printingmethod/adminhtml_printingmethod_edit_tab_fixedprice')->toHtml(),
				));
			}
			
			if($printingmethod->getPricingLogic()==2){
				$this->addTab('qc_price', array(
					'label' => Mage::helper('printingmethod')->__('Quantity Colors Price'),
					'content'   =>$this->getLayout()->createBlock('printingmethod/adminhtml_printingmethod_edit_tab_qcprice')->toHtml(),
				));
			}
						
			if($printingmethod->getPricingLogic()==3){			
				$this->addTab('qa_price', array(
						'label' => Mage::helper('printingmethod')->__('Quantity Area Price'),
						'content'   =>$this->getLayout()->createBlock('printingmethod/adminhtml_printingmethod_edit_tab_qaprice')->toHtml(),
				));
			}
			
		}
		
		$this->addTab('products', array(
			'label' => Mage::helper('printingmethod')->__('Associated Products'),
			'url'   => $this->getUrl('*/*/products', array('_current' => true)),
   			'class'	=> 'ajax'
		));
		return parent::_beforeToHtml();
	}
}