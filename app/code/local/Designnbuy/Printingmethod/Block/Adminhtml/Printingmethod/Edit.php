<?php
/**
 * Designnbuy_Printingmethod extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @copyright  	Copyright (c) 2014
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Printing Method admin edit block
 *
 * @category	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @author Ultimate Module Creator
 */
class Designnbuy_Printingmethod_Block_Adminhtml_Printingmethod_Edit extends Mage_Adminhtml_Block_Widget_Form_Container{
	/**
	 * constuctor
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function __construct(){
		parent::__construct();
		$this->_blockGroup = 'printingmethod';
		$this->_controller = 'adminhtml_printingmethod';
		$this->_updateButton('save', 'label', Mage::helper('printingmethod')->__('Save Printing Method'));
		$this->_updateButton('delete', 'label', Mage::helper('printingmethod')->__('Delete Printing Method'));
		$this->_addButton('saveandcontinue', array(
			'label'		=> Mage::helper('printingmethod')->__('Save And Continue Edit'),
			'onclick'	=> 'saveAndContinueEdit()',
			'class'		=> 'save',
		), -100);
		// $this->_formScripts[] = "
			// function saveAndContinueEdit(){
				// editForm.submit($('edit_form').action+'back/edit/');
			// }
		// ";
		$this->_formScripts[] = '
			var templateSyntax = /(^|.|\r|\n)({{(\w+)}})/;
			var attributesForm = new varienForm("edit_form", \''.$this->getUrl('*/*/validate', array('_current'=>true)).'\');
			attributesForm._processValidationResult = function(transport) {
			var response = transport.responseText.evalJSON();		
			
			if (response.error){
				// if (response.attribute && $(response.attribute)) {
				if (response.attribute) {					
					$("message_"+response.attribute).setHasError(true, attributesForm);					
					Validation.ajaxError($("message_"+response.attribute), response.message);
					if (!Prototype.Browser.IE){
						$("message_"+response.attribute).focus();
					}
				} 
			} else {	
				attributesForm._submit();				
			}
		}; 
		function saveAndContinueEdit(urlTemplate) {			
			// var template = new Template(urlTemplate, templateSyntax);
			// var url = template.evaluate({tab_id:printingmethod_tabs_printable_colors.activeTab.id});
		   attributesForm.submit($("edit_form").action+"back/edit/");
		}
		';
	}
	/**
	 * get the edit form header
	 * @access public
	 * @return string
	 * @author Ultimate Module Creator
	 */
	public function getHeaderText(){
		if( Mage::registry('printingmethod_data') && Mage::registry('printingmethod_data')->getId() ) {
			return Mage::helper('printingmethod')->__("Edit Printing Method '%s'", $this->htmlEscape(Mage::registry('printingmethod_data')->getPrintingMethod()));
		} 
		else {
			return Mage::helper('printingmethod')->__('Add Printing Method');
		}
	}
}