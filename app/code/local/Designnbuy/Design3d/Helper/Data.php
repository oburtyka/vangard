<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Designnbuy_Design3d_Helper_Data extends Mage_Core_Helper_Abstract
{
	public function get3dAttributeSet()
    {      	
		return "3dDesigner";
    }
	
	public function getdesigntoolAttributeSet()
    {      	
		return "Designtool";
    }
	
	public function checkAttributeSet($attrSetName)
    {      	
		$attributeSetId = Mage::getModel('eav/entity_attribute_set')
			->load($attrSetName, 'attribute_set_name')
			->getAttributeSetId();
		
		return $attributeSetId;
    }
	
	public function is3dProduct($productId)
    {      	
		$_product = Mage::getModel('catalog/product')->load($productId);
		$attributeSetModel = Mage::getModel("eav/entity_attribute_set");
		$attributeSetModel->load($_product->getAttributeSetId());
		$attributeSetName  = $attributeSetModel->getAttributeSetName();
		if($attributeSetName == $this->get3dAttributeSet())
		{
			return true;
		}
		else
		{
			return false;
		}
    }
	
	public function getDesignPageUrl($product)
    {      	
		return $this->_getUrl('design3d/index/index', array(
            'id' => $product->getId(),
            'cat_id' => $categoryId
        ));
    }
	public function get3dDesignerPageUrl($product)
    {      	
		return $this->_getUrl('design3d/index/design3d', array(
            'id' => $product->getId()
        ));
    }
	public function getdesignUrl()
	{
        return 	$this->_getUrl('design3d/index/savedesign');
	}
	
	public function getNamePrice()
	{
        return 	(float) Mage::getStoreConfig('designtool_options/name_number_price/name_price');
	}
	
	public function getNumberPrice()
	{
        return 	(float)Mage::getStoreConfig('designtool_options/name_number_price/number_price');
	}	

}
?>