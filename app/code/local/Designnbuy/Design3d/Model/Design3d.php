<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category   Mage
 * @package    Mage_Sendfriend
 * @copyright  Copyright (c) 2008 Irubin Consulting Inc. DBA Varien (http://www.varien.com)
 * @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

class Designnbuy_Design3d_Model_Design3d extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init('design3d/design3d');
    }   
	public  function  getFinalPrice($qty=null,$product,$colorid,$sizeid)
        { 
			if  (is_null($qty)  &&  !is_null($product->getCalculatedFinalPrice()))  {
					return  $product->getCalculatedFinalPrice();
			}
			//$finalPrice  =  $product->getFinalPrice($qty,  $product);
			$finalPrice  = Mage::getModel('catalog/product_type_price')->getFinalPrice($qty,$product);
			 
			/*get discounted price if catalog product rule applied*/
			$store_id = Mage::app()->getStore()->getStoreId();			
			$discounted_price = Mage::getResourceModel('catalogrule/rule')->getRulePrice( 
									Mage::app()->getLocale()->storeTimeStamp($store_id), 
									Mage::app()->getStore($store_id)->getWebsiteId(), 
									Mage::getSingleton('customer/session')->getCustomerGroupId(), 
									$product->getId());
			 	 
			if ($discounted_price!='') {
				$finalPrice=$discounted_price;
			}
			
			$baseprice = $finalPrice; 
			$product->getTypeInstance()->setStoreFilter($product->getStore());
			$productType=$product->getTypeID();
			if($productType == 'configurable')
			{ 
				$_attributes = $product->getTypeInstance(true)->getConfigurableAttributes($product);
				 
				foreach($_attributes as $_attribute)
				{
					foreach($_attribute->getprices() as $attributeoption)
					{
						if($attributeoption['value_index'] == $sizeid || $attributeoption['value_index'] == $colorid )
						{
							$finalPrice += $this->getPriceToAdd($attributeoption,$baseprice);
						}
					}
				}	
			}
			
				return  $finalPrice ;
        }
	public function getCustomTierPrice($qutoeqty,$product)
		{
			$cnt = count($product->getTierPrice());
			if($cnt > 0)
			{
				$tierprices = $product->getTierPrice();
				
				foreach($tierprices as $tierprice)				
				{
					$price_qty = $tierprice['price_qty'];
					if( $qutoeqty >= ceil($price_qty)  )
					{
						$newtierprice = $tierprice['price'];
					}
				}
			}
			if(!isset($newtierprice))
			{
				$finalprice =$product->getPrice();
			}
			else
			{
				$finalprice =$newtierprice;
			}
		return $finalprice;
			
		}	
/* 	public function  getValueByIndex($values, $index) {
        foreach ($values as $value) {
            if($value['value_index'] == $index) {
                return $value;
            }
        }
        return false;
    } */
	public function getPriceToAdd($priceInfo, $productPrice)
    {
        if($priceInfo['is_percent']) {
            $ratio = $priceInfo['pricing_value']/100;
            $price = $productPrice * $ratio;
        } else {
            $price = $priceInfo['pricing_value'];
        }
        return $price;
    }	
	
	public function get3dProductsFromCategory($categoryId)
	{
		$designtoolAttrSetName = "3dDesigner";
		//Get Attribute set Id from Attribute set name
		$designtoolAttributeSetId = Mage::getModel('eav/entity_attribute_set')
                            ->load($designtoolAttrSetName, 'attribute_set_name')
                            ->getAttributeSetId();
		// Load Category by category id.
		$catagory = Mage::getModel('catalog/category')->load($categoryId);

		// Get product collection by category id.
		$productCollection = Mage::getResourceModel('catalog/product_collection')
							->addCategoryFilter($catagory) //category filter
							->AddFieldToFilter('is_customizable', 1)
							->AddFieldToFilter('status', 1)
							->addAttributeToFilter('attribute_set_id',$designtoolAttributeSetId)
							->addAttributeToSelect('*');
		Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($productCollection);
		
		$xmlString = '<?xml version="1.0" encoding="iso-8859-1"?><allProducts>';
		foreach($productCollection as $product)
		{
			
			if($product->getis_salable() && $product->getStockItem()->getis_in_stock())
			{
				$configProductId = $product->getEntityId();				
				$configProduct = Mage::getModel('catalog/product')->load($configProductId);
				$includeinlist = true;
				if($configProduct->getTypeID() == 'configurable'){
					$childProducts = Mage::getModel('catalog/product_type_configurable')->getUsedProducts(null,$configProduct);
					// for associative simple product
					$includeinlist = false;
					foreach($childProducts as $child){
						if($child->getStatus() ==1){
							if( $child->getStockItem()->getManageStock() == 1 ){
								if($child->getStockItem()->getQty() >0 and $child->getStockItem()->getIsInStock() > 0){
									$includeinlist =true;
									break;
								}	
							} else {
								$includeinlist =true;
								break;
							}
						}	
					}
				}
				if($includeinlist){
					// $isMulticolor = $configProduct->getMulticolor();
					$xmlString .= '<product>';
					$xmlString .= '<name>'.$configProduct->getName().'</name>';
					$xmlString .= '<productID>'.$configProduct->getEntityId().'</productID>';
					$xmlString .= '<code>'.$configProduct->getSku().'</code>';
					$xmlString .= '<shortDesc>'.htmlspecialchars($configProduct->getShortDescription()).'</shortDesc>';
					$xmlString .= '<longDesc>'.htmlspecialchars($configProduct->getDescription()).'</longDesc>';	

					if($configProduct->getThumbnail() == 'no_selection' or $configProduct->getThumbnail()== '')
						$thumbnailImage = '';
					else
						$thumbnailImage = Mage::getModel('catalog/product_media_config')->getMediaUrl($configProduct->getThumbnail());
					$xmlString .= '<thumbImage>'.$thumbnailImage.'</thumbImage>';		
					$xmlString .= '</product>';				
				}			
			}
			
		}
		$xmlString .= '</allProducts>';
		return $xmlString;
	}
	public function getSimple3dProductFromId($productId,$user)
	{
		$storeId = Mage::app()->getStore()->getStoreId();
		
		/*If product id not exist in case of design idea then get the first product id from collection*/
		$designtoolAttrSetName = "3DDesigner";
		//Get Attribute set Id from Attribute set name
		$designtoolAttributeSetId = Mage::getModel('eav/entity_attribute_set')
                            ->load($designtoolAttrSetName, 'attribute_set_name')
                            ->getAttributeSetId();						
		
		$productCollection = Mage::getModel('catalog/product')
					->getCollection()
					->addAttributeToFilter('entity_id', $productId)
					->AddFieldToFilter('is_customizable', 1)					
					->addAttributeToFilter('type_id', 'simple')
					->addAttributeToFilter('attribute_set_id',$designtoolAttributeSetId)					
					->addAttributeToSelect('*');
		$productCount = count($productCollection->getData());
		
		if($productCount==0 && $user!='')		
		{
			$productCollection = Mage::getModel('catalog/product')
							->getCollection()	
							->AddFieldToFilter('is_customizable', 1)
							->AddFieldToFilter('status', 1)
							->addAttributeToFilter('type_id', 'simple')
							->addAttributeToFilter('attribute_set_id',$designtoolAttributeSetId)					
							->addAttributeToSelect('*');
						
			Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($productCollection);		
			$firstProduct = $productCollection->getFirstItem();	
			$productId = $firstProduct->getEntityId();
		}
		
		$simpleProductId = $productId;
		$simpleProduct = Mage::getModel('catalog/product')->load($simpleProductId);		
		$xmlString = '<?xml version="1.0" encoding="iso-8859-1"?><allProducts>';
		$xmlString .= '	<product>';
		
		$xmlString .= '	<name>'.$simpleProduct->getName().'</name>';
		$xmlString .= '	<productID>'.$simpleProduct->getEntityId().'</productID>';
		$xmlString .= '	<code>'.$simpleProduct->getSku().'</code>';		
		$xmlString .= '	<isNameNumber>'.$simpleProduct->getIsNameNumber().'</isNameNumber>';
		$xmlString .= '	<shortDesc>'.htmlspecialchars($simpleProduct->getShortDescription()).'</shortDesc>';
		$xmlString .= '	<longDesc>'.htmlspecialchars($simpleProduct->getDescription()).'</longDesc>';	

		if($simpleProduct->getThumbnail() == 'no_selection' or $simpleProduct->getThumbnail()== '')
			$thumbnailImage = '';
		else
			$thumbnailImage = Mage::getModel('catalog/product_media_config')->getMediaUrl($simpleProduct->getThumbnail());

		if($simpleProduct->getStatus() == '1')
			$status = 'yes';
		else
			$status = 'no';		

		$xmlString .= '	<defaultThumb>'.$thumbnailImage.'</defaultThumb>';		
		$xmlString .= '	<allImages><image>';
		$xmlString .= '	<thumbImage>'.$thumbnailImage.'</thumbImage>';
		$xmlString .= '	</image></allImages>';
		$noOfSides = $simpleProduct->getAttributeText('customizable_sides');
		 
		// if ($simpleProduct->getAttributeText('is_allow_front') == "Yes")
		// {
			// $isAllowFront = 1;
		// }
		// else
		// {
			// $isAllowFront = 0;
		// }
		
		switch($noOfSides)
		{
			case 'Front':
				$xmlString .= '	<noofSides>1</noofSides>';				
			break;
			case 'Back':
				$xmlString .= '	<noofSides>1</noofSides>';				
			break;
			case 'Front & Back':			
				$xmlString .= '	<noofSides>2</noofSides>';
			break;			
		}	
		$isAllowFront = $simpleProduct->getIsAllowFront();
		$xmlString .= '	<isAllowFront>'.$isAllowFront.'</isAllowFront>';
		$materialType = $simpleProduct->getAttributeText('material_type');
		$xmlString .= '	<materialType>'.$materialType.'</materialType>';
		
		$colorImage = Mage::helper('fileattributes')->getFileUrl($simpleProduct->getFront_3d());
		$simpleFrontImage = $simpleProduct->getFront_3d();
		if($simpleFrontImage== '')
			$simpleFront3dImage = '';
		else
			$simpleFront3dImage = Mage::helper('fileattributes')->getFileUrl($simpleProduct->getFront_3d());

		$simpleBackImage = $simpleProduct->getBack_3d();		
		if($simpleBackImage== '')
			$simpleBack3dImage = '';
		else
			$simpleBack3dImage = Mage::helper('fileattributes')->getFileUrl($simpleProduct->getBack_3d());			
			
		$simpleMainImage = $simpleProduct->getMain_3d();		
		if($simpleMainImage== '')
			$simpleMainMobileImage = '';
		else
			$simpleMainMobileImage = Mage::helper('fileattributes')->getFileUrl($simpleProduct->getMain_3d());
		
		$simpleAWDImage = $simpleProduct->getAwd_3d();		
		if($simpleAWDImage== '')
			$simpleAWDMobileImage = '';
		else
			$simpleAWDMobileImage = Mage::helper('fileattributes')->getFileUrl($simpleProduct->getAwd_3d());
			
		//$xmlString .= '	<noofSides>'.$noOfSides.'</noofSides>';
		$xmlString .= '	<mainImage>'.$simpleMainMobileImage.'</mainImage>';	
		$xmlString .= '	<awdImage>'.$simpleAWDMobileImage.'</awdImage>';	
		$xmlString .= '	<productImages><image>';
		switch($noOfSides)
		{
			case 'Front':
				$xmlString .= '	<frontImage>'.$simpleFront3dImage.'</frontImage>';				
			break;
			case 'Back':
				$xmlString .= '	<backImage>'.$simpleBack3dImage.'</backImage>';				
			break;
			case 'Front & Back':			
				$xmlString .= '	<frontImage>'.$simpleFront3dImage.'</frontImage>';
				$xmlString .= '	<backImage>'.$simpleBack3dImage.'</backImage>';
			break;			
		}	
		$xmlString .= '	</image></productImages>';
		$availableQty = $simpleProduct->getStockItem()->getQty();	
		$minSaleQty = $simpleProduct->getStockItem()->getMinSaleQty();				
		$isConfigSetting = $simpleProduct->getStockItem()->getUseConfigMaxSaleQty();
		$maxSaleQty = $simpleProduct->getStockItem()->getMaxSaleQty();
		$minQty = min($availableQty,$maxSaleQty);
		$xmlString .= '	<minQty>'.$minSaleQty.'</minQty>';
		$xmlString .= '	<maxQty>'.$minQty.'</maxQty>';
		$collection = Mage::getModel('texturemanagement/texture')
			->getCollection()
			->AddFieldToFilter('status', 1)
			->AddFieldToFilter('texture_image', array('neq' => 'NULL' ));
        
		$xmlString .= '<textures>';
		foreach ($collection as $texture)
		{
			if($texture->getTextureImage()!= NULL && file_exists(Mage::helper('texturemanagement/texture_image')->getImageBaseDir().$texture->getTextureImage()))
			{	
				$textureImage = Mage::helper('texturemanagement/texture_image')->getImageBaseUrl().$texture->getTextureImage();
				$textureResizedimage = Mage::getBaseDir('media').DS.'texture'. DS .'resize'. DS . $texture->getTextureImage();
				$textureResizeimage = '';
				if(file_exists($textureResizedimage) && $texture->getTextureImage()!=NULL) 
				{
					$textureResizeimage = Mage::helper('texturemanagement/texture_image')->getImageBaseUrl().'resize/'.$texture->getTextureImage();
				}
				
				$textureImage = Mage::helper('texturemanagement/texture_image')->getImageBaseUrl().$texture->getTextureImage();
				$xmlString .= '	<texture>';
				$xmlString .= '	<textureID>'.$texture->getEntityId().'</textureID>';	
				$xmlString .= '	<textureName>'.$texture->getTextureName().'</textureName>';
				$xmlString .= '	<textureIcon>'.$textureResizeimage.'</textureIcon>';
				$xmlString .= '	<textureImage>'.$textureImage.'</textureImage>';
				$xmlString .= '	</texture>';
			}
		}
		$xmlString .= '</textures>';
		/*get configure area from designtool_configarea table*/
		$configAreaData = Mage::getModel('design/configarea')->load($simpleProductId,'product_id');
		$xmlString .= '	<Area>';	
		switch($noOfSides)
		{
			case 4:
				$xmlString .= '	<frontArea>'.$configAreaData->getFaX().','.$configAreaData->getFaY().','.$configAreaData->getFaWidth().','.$configAreaData->getFaHeight().','.$configAreaData->getFaOutputWidth().','.$configAreaData->getFaOutputHeight().'</frontArea>';		
				$xmlString .= '	<backArea>'.$configAreaData->getBaX().','.$configAreaData->getBaY().','.$configAreaData->getBaWidth().','.$configAreaData->getBaHeight().','.$configAreaData->getBaOutputWidth().','.$configAreaData->getBaOutputHeight().'</backArea>';
				$xmlString .= '	<leftArea>'.$configAreaData->getLeX().','.$configAreaData->getLeY().','.$configAreaData->getLeWidth().','.$configAreaData->getLeHeight().','.$configAreaData->getLeOutputWidth().','.$configAreaData->getLeOutputHeight().'</leftArea>';		
				$xmlString .= '	<rightArea>'.$configAreaData->getRiX().','.$configAreaData->getRiY().','.$configAreaData->getRiWidth().','.$configAreaData->getRiHeight().','.$configAreaData->getRiOutputWidth().','.$configAreaData->getRiOutputHeight().'</rightArea>';			
			break;
			case 3:
				$xmlString .= '	<frontArea>'.$configAreaData->getFaX().','.$configAreaData->getFaY().','.$configAreaData->getFaWidth().','.$configAreaData->getFaHeight().','.$configAreaData->getFaOutputWidth().','.$configAreaData->getFaOutputHeight().'</frontArea>';		
				$xmlString .= '	<backArea>'.$configAreaData->getBaX().','.$configAreaData->getBaY().','.$configAreaData->getBaWidth().','.$configAreaData->getBaHeight().','.$configAreaData->getBaOutputWidth().','.$configAreaData->getBaOutputHeight().'</backArea>';
				if($configLeftImage != '')
					$xmlString .= '	<leftArea>'.$configAreaData->getLeX().','.$configAreaData->getLeY().','.$configAreaData->getLeWidth().','.$configAreaData->getLeHeight().','.$configAreaData->getLeOutputWidth().','.$configAreaData->getLeOutputHeight().'</leftArea>';	
				else
					$xmlString .= '	<rightArea>'.$configAreaData->getRiX().','.$configAreaData->getRiY().','.$configAreaData->getRiWidth().','.$configAreaData->getRiHeight().','.$configAreaData->getRiOutputWidth().','.$configAreaData->getRiOutputHeight().'</rightArea>';		
			break;
			case 2:
				default: 
				$xmlString .= '	<frontArea>'.$configAreaData->getFaX().','.$configAreaData->getFaY().','.$configAreaData->getFaWidth().','.$configAreaData->getFaHeight().','.$configAreaData->getFaOutputWidth().','.$configAreaData->getFaOutputHeight().'</frontArea>';		
				$xmlString .= '	<backArea>'.$configAreaData->getBaX().','.$configAreaData->getBaY().','.$configAreaData->getBaWidth().','.$configAreaData->getBaHeight().','.$configAreaData->getBaOutputWidth().','.$configAreaData->getBaOutputHeight().'</backArea>';
			break;			
		}	
		$xmlString .= '	</Area>';
		$xmlString .= '	<printingMethods>'.$this->getPritingMethods($simpleProduct,$user).'</printingMethods>';	
		$xmlString .= '	</product>';
		$xmlString .= '</allProducts>';
		return $xmlString;
	}	
	
	public function getConfigurable3dProductFromId($productId,$user)
	{
		$storeId = Mage::app()->getStore()->getStoreId();
		
		/*If product id not exist in case of design idea then get the first product id from collection*/
		$designtoolAttrSetName = "3dDesigner";
		//Get Attribute set Id from Attribute set name
		$designtoolAttributeSetId = Mage::getModel('eav/entity_attribute_set')
                            ->load($designtoolAttrSetName, 'attribute_set_name')
                            ->getAttributeSetId();						
		
		$productCollection = Mage::getModel('catalog/product')
					->getCollection()
					->addAttributeToFilter('entity_id', $productId)
					->AddFieldToFilter('is_customizable', 1)					
					->addAttributeToFilter('type_id', 'configurable')
					->addAttributeToFilter('attribute_set_id',$designtoolAttributeSetId)					
					->addAttributeToSelect('*');
		$productCount = count($productCollection->getData());
		
		if($productCount==0 && $user!='')		
		{
			$productCollection = Mage::getModel('catalog/product')
							->getCollection()	
							->AddFieldToFilter('is_customizable', 1)
							->AddFieldToFilter('status', 1)
							->addAttributeToFilter('type_id', 'configurable')
							->addAttributeToFilter('attribute_set_id',$designtoolAttributeSetId)					
							->addAttributeToSelect('*');
						
			Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($productCollection);		
			$firstProduct = $productCollection->getFirstItem();	
			$productId = $firstProduct->getEntityId();
		}
		
		$configProductId = $productId;
		$xmlString = '<?xml version="1.0" encoding="iso-8859-1"?><allProducts>';
		$xmlString .= '	<product>';
		$configProduct = Mage::getModel('catalog/product')->load($configProductId);
		$isMulticolor = $configProduct->getMulticolor();
		$xmlString .= '	<name>'.$configProduct->getName().'</name>';
		$xmlString .= '	<productID>'.$configProduct->getEntityId().'</productID>';
		$xmlString .= '	<code>'.$configProduct->getSku().'</code>';
		/*get Attribute Id of the color*/					
		$colorAttribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', 'color');
		$colorAttibuteId = $colorAttribute->getAttributeId();
		/*get Attribute Id of the size*/					
		$sizeAttribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', 'size');
		$sizeAttributeId = $sizeAttribute->getAttributeId();
		$xmlString .= '	<colorId>'.$colorAttibuteId.'</colorId>';
		$xmlString .= '	<sizeId>'.$sizeAttributeId.'</sizeId>';
		$xmlString .= '	<isNameNumber>'.$configProduct->getIsNameNumber().'</isNameNumber>';
		$xmlString .= '	<shortDesc>'.htmlspecialchars($configProduct->getShortDescription()).'</shortDesc>';
		$xmlString .= '	<longDesc>'.htmlspecialchars($configProduct->getDescription()).'</longDesc>';	
		$xmlString .= '<multiColor>no</multiColor>';

		if($configProduct->getThumbnail() == 'no_selection' or $configProduct->getThumbnail()== '')
			$thumbnailImage = '';
		else
			$thumbnailImage = Mage::getModel('catalog/product_media_config')->getMediaUrl($configProduct->getThumbnail());

		if($configProduct->getStatus() == '1')
			$status = 'yes';
		else
			$status = 'no';	

		$xmlString .= '	<defaultThumb>'.$thumbnailImage.'</defaultThumb>';		
		$xmlString .= '	<allImages><image>';
		$xmlString .= '	<thumbImage>'.$thumbnailImage.'</thumbImage>';
		$xmlString .= '	</image></allImages>';
		$noOfSides = $configProduct->getAttributeText('no_of_sides');
		$noOfSides = $configProduct->getAttributeText('customizable_sides');
		
		// if ($configProduct->getAttributeText('is_allow_front') == "Yes")
		// {
			// $isAllowFront = 1;
		// }
		// else
		// {
			// $isAllowFront = 0;
		// }
		
		switch($noOfSides)
		{
			case 'Front':
				$xmlString .= '	<noofSides>1</noofSides>';				
			break;
			case 'Back':
				$xmlString .= '	<noofSides>1</noofSides>';				
			break;
			case 'Front & Back':			
				$xmlString .= '	<noofSides>2</noofSides>';
			break;			
		}	
		$isAllowFront = $configProduct->getIsAllowFront();
		$xmlString .= '	<isAllowFront>'.$isAllowFront.'</isAllowFront>';
		$materialType = $configProduct->getAttributeText('material_type');
		$xmlString .= '	<materialType>'.$materialType.'</materialType>';
		$colorImage = Mage::helper('fileattributes')->getFileUrl($configProduct->getFront_3d());
		$simpleFrontImage = $configProduct->getFront_3d();
		if($simpleFrontImage== '')
			$simpleFront3dImage = '';
		else
			$simpleFront3dImage = Mage::helper('fileattributes')->getFileUrl($configProduct->getFront_3d());

		$simpleBackImage = $configProduct->getBack_3d();		
		if($simpleBackImage== '')
			$simpleBack3dImage = '';
		else
			$simpleBack3dImage = Mage::helper('fileattributes')->getFileUrl($configProduct->getBack_3d());			
			
		$simpleMainImage = $configProduct->getMain_3d();		
		if($simpleMainImage== '')
			$simpleMainMobileImage = '';
		else
			$simpleMainMobileImage = Mage::helper('fileattributes')->getFileUrl($configProduct->getMain_3d());
		
		$simpleAWDImage = $configProduct->getAwd_3d();		
		if($simpleAWDImage== '')
			$simpleAWDMobileImage = '';
		else
			$simpleAWDMobileImage = Mage::helper('fileattributes')->getFileUrl($configProduct->getAwd_3d());
			
		//$xmlString .= '	<noofSides>'.$noOfSides.'</noofSides>';
		$xmlString .= '	<mainImage>'.$simpleMainMobileImage.'</mainImage>';	
		$xmlString .= '	<awdImage>'.$simpleAWDMobileImage.'</awdImage>';	
		$xmlString .= '	<productImages><image>';
		switch($noOfSides)
		{
			case 'Front':
				$xmlString .= '	<frontImage>'.$simpleFront3dImage.'</frontImage>';				
			break;
			case 'Back':
				$xmlString .= '	<backImage>'.$simpleBack3dImage.'</backImage>';				
			break;
			case 'Front & Back':			
				$xmlString .= '	<frontImage>'.$simpleFront3dImage.'</frontImage>';
				$xmlString .= '	<backImage>'.$simpleBack3dImage.'</backImage>';
			break;			
		}	
		$xmlString .= '	</image></productImages>';
		
		$configurableProduct = Mage::getModel('catalog/product_type_configurable')->setProduct($configProduct);
		$configProduct->getTypeInstance()->setStoreFilter($configProduct->getStore());
		$productAttributeOptions = $configProduct->getTypeInstance(true)->getConfigurableAttributesAsArray($configProduct);

		$attributeOptions = array();
		$isColorAttribute = false;
		$isSizeAttribute = false;
		foreach ($productAttributeOptions as $productAttribute) {
			if($productAttribute['attribute_code'] == 'color'):
				$isColorAttribute = true;
			endif;

			if($productAttribute['attribute_code'] == 'size'):
				$isSizeAttribute = true;
			endif;
			
			foreach ($productAttribute['values'] as $attribute) {			
				//$attributeOptions[] = $attribute;
				$attributeOptions[$attribute['value_index']] = $attribute;
			}
		}

		/*get associate product collection with status enabled products*/
		$childProductCollection = $configurableProduct->getUsedProductCollection()
							->addAttributeToSelect('*')							
							->addFilterByRequiredOptions();	
		if($user=='')
		{
			$childProductCollection->addAttributeToFilter('status', array('eq' => 1));
		}
		//Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($childProductCollection);						
		/*filter associate products collection by "in stock" product*/		
		if($user=='')
		{
			Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($childProductCollection);
		}
		/*get All product ids of associate products*/
		$childProdIds = $childProductCollection->getAllIds();	

		//if($isColorAttribute == 1) {
		/*Get product collection by color attribute*/
		$associateProductCollection = Mage::getModel('catalog/product')->getCollection()
				->AddAttributeToSelect('*')
				->addAttributeToFilter('type_id','simple')				
				->AddFieldToFilter('entity_id',$childProdIds)
				->addOrder('entity_id','ASC');
		if($user=='')
		{
			$associateProductCollection->AddFieldToFilter('status', 1);
		}
				
		$associateProductCollection->groupbyAttribute('color');				
		/* echo "<pre>";		
		print_r($childProdIds);		
		print_r($associateProductCollection->getData());
		die; */
		//Color XML start
		$xmlString .= '	<allColors>';
		foreach($associateProductCollection as $associateProduct){		
		$xmlString .= '	<color>';
		//echo "<br />";
		$productModel = Mage::getModel('catalog/product');
		$colorAttribute = $productModel->getResource()->getAttribute("color");
		$colorId = $associateProduct->getColor();
			if ($colorAttribute->usesSource()) {
				$colorLabel = $colorAttribute->getSource()->getOptionText($colorId);
				$colorName = explode('(', $colorLabel);
				$colorText = $colorName[0];					
				$colorTemp = array_reverse($colorName);
				$colorName = explode(')', $colorTemp[0]);
			}
			$xmlString .= '	<optionID>'.$colorId.'</optionID>';
					
			// $xmlString .= ' <optionName>'.$colorName[0].'</optionName>'; 
			$_collection = Mage::getResourceModel('eav/entity_attribute_option_collection')
			->setStoreFilter(0)
			->load();

			foreach( $_collection->toOptionArray() as $_cur_option ) {     
				if ($_cur_option['value'] == $colorId)
				{ 
					$colorName = explode('(', $_cur_option['label']);
					$colorText = $colorName[0];     
					$colorTemp = array_reverse($colorName);
					$colorName = explode(')', $colorTemp[0]);
					$xmlString .= ' <optionName>'.$colorName[0].'</optionName>'; 
				}      
			}			
		$colorOptionCollection = Mage::getResourceModel('eav/entity_attribute_option_collection')
							->setAttributeFilter($colorAttibuteId)
							->setIdFilter($colorId)
							->setPositionOrder('ASC')
							->setStoreFilter(Mage::app()->getStore()->getStoreId(), false);
		if($colorOptionCollection->getSize() > 0){
			$colorText = $colorOptionCollection->getFirstItem()->getValue();
		}	
		$xmlString .= '	<priceModifier>0</priceModifier>';
		$xmlString .= '	<colorName>'.$colorText.'</colorName>';
		/* 
		echo $attributeOptions[$colorId]['is_percent'];
		echo "<br />";
		echo $attributeOptions[$colorId]['pricing_value'];
		echo "<br />"; */		
			
		//if($isSizeAttribute == 1) {
			$sizeProductCollection = Mage::getModel('catalog/product')->getCollection()
						->AddAttributeToSelect('*')
						->addAttributeToFilter('type_id','simple')						
						->addAttributeToFilter('color',$colorId)
						->AddFieldToFilter('entity_id',$childProdIds)
						->groupbyAttribute('size');	
			if($user=='')
				{
					$sizeProductCollection->AddFieldToFilter('status', 1);
				}
			$xmlString .= '	<sizes>';
			foreach($sizeProductCollection as $sizeProduct){
				$child = Mage::getModel('catalog/product')->load($sizeProduct->getId());
				$availableQty = $child->getStockItem()->getQty();	
				$minSaleQty = $child->getStockItem()->getMinSaleQty();				
				$isConfigSetting = $child->getStockItem()->getUseConfigMaxSaleQty();
				$maxSaleQty = $child->getStockItem()->getMaxSaleQty();
				$minQty = min($availableQty,$maxSaleQty);
				$sizeAttribute = $productModel->getResource()->getAttribute("size");
				$sizeId = $sizeProduct->getSize();
				if ($sizeAttribute->usesSource()) {
					$sizeLabel = $sizeAttribute->getSource()->getOptionText($sizeId);
				}
				$sizeOptionCollection = Mage::getResourceModel('eav/entity_attribute_option_collection')
							->setAttributeFilter($sizeAttibuteId)
							->setIdFilter($sizeId)
							->setPositionOrder('ASC')
							->setStoreFilter(Mage::app()->getStore()->getStoreId(), false);
				if($sizeOptionCollection->getSize() > 0){
					$sizeLabel = $sizeOptionCollection->getFirstItem()->getValue();
				}
				$xmlString .= '	<size>';
				$xmlString .= '	<productID>'.$sizeProduct->getId().'</productID>';
				$xmlString .= '	<minQty>'.$minSaleQty.'</minQty>';
				$xmlString .= '	<maxQty>'.$minQty.'</maxQty>';
				$xmlString .= '	<optionID>'.$sizeId.'</optionID>';
				$xmlString .= '	<optionName>'.$sizeLabel.'</optionName>';
				$productPrice = $this->getFinalPrice($qty=1,$configProduct,$colorId,$sizeId);				
				$xmlString .= '	<priceModifier>'.Mage::helper('core')->currency($productPrice,true,false).'</priceModifier>';
				$xmlString .= '	</size>';			
				//echo $attributeOptions[$sizeId]['pricing_value'];
				//echo "<br />";
						
			}	
			$xmlString .= '	</sizes>';
		//}
		$xmlString .= '	</color>';
		}
		//}

		$xmlString .= '	</allColors>';
		/*get configure area from designtool_configarea table*/
		$configAreaData = Mage::getModel('design/configarea')->load($configProductId,'product_id');
		$xmlString .= '	<Area>';	
		switch($noOfSides)
		{
			case 4:
				$xmlString .= '	<frontArea>'.$configAreaData->getFaX().','.$configAreaData->getFaY().','.$configAreaData->getFaWidth().','.$configAreaData->getFaHeight().','.$configAreaData->getFaOutputWidth().','.$configAreaData->getFaOutputHeight().'</frontArea>';		
				$xmlString .= '	<backArea>'.$configAreaData->getBaX().','.$configAreaData->getBaY().','.$configAreaData->getBaWidth().','.$configAreaData->getBaHeight().','.$configAreaData->getBaOutputWidth().','.$configAreaData->getBaOutputHeight().'</backArea>';
				$xmlString .= '	<leftArea>'.$configAreaData->getLeX().','.$configAreaData->getLeY().','.$configAreaData->getLeWidth().','.$configAreaData->getLeHeight().','.$configAreaData->getLeOutputWidth().','.$configAreaData->getLeOutputHeight().'</leftArea>';		
				$xmlString .= '	<rightArea>'.$configAreaData->getRiX().','.$configAreaData->getRiY().','.$configAreaData->getRiWidth().','.$configAreaData->getRiHeight().','.$configAreaData->getRiOutputWidth().','.$configAreaData->getRiOutputHeight().'</rightArea>';			
			break;
			case 3:
				$xmlString .= '	<frontArea>'.$configAreaData->getFaX().','.$configAreaData->getFaY().','.$configAreaData->getFaWidth().','.$configAreaData->getFaHeight().','.$configAreaData->getFaOutputWidth().','.$configAreaData->getFaOutputHeight().'</frontArea>';		
				$xmlString .= '	<backArea>'.$configAreaData->getBaX().','.$configAreaData->getBaY().','.$configAreaData->getBaWidth().','.$configAreaData->getBaHeight().','.$configAreaData->getBaOutputWidth().','.$configAreaData->getBaOutputHeight().'</backArea>';
				if($configLeftImage != '')
					$xmlString .= '	<leftArea>'.$configAreaData->getLeX().','.$configAreaData->getLeY().','.$configAreaData->getLeWidth().','.$configAreaData->getLeHeight().','.$configAreaData->getLeOutputWidth().','.$configAreaData->getLeOutputHeight().'</leftArea>';	
				else
					$xmlString .= '	<rightArea>'.$configAreaData->getRiX().','.$configAreaData->getRiY().','.$configAreaData->getRiWidth().','.$configAreaData->getRiHeight().','.$configAreaData->getRiOutputWidth().','.$configAreaData->getRiOutputHeight().'</rightArea>';		
			break;
			case 2:
				default: 
				$xmlString .= '	<frontArea>'.$configAreaData->getFaX().','.$configAreaData->getFaY().','.$configAreaData->getFaWidth().','.$configAreaData->getFaHeight().','.$configAreaData->getFaOutputWidth().','.$configAreaData->getFaOutputHeight().'</frontArea>';		
				$xmlString .= '	<backArea>'.$configAreaData->getBaX().','.$configAreaData->getBaY().','.$configAreaData->getBaWidth().','.$configAreaData->getBaHeight().','.$configAreaData->getBaOutputWidth().','.$configAreaData->getBaOutputHeight().'</backArea>';
			break;			
		}	
		$xmlString .= '	</Area>';
		$xmlString .= '	<printingMethods>'.$this->getPritingMethods($configProduct,$user).'</printingMethods>';	
		$xmlString .= '	</product>';
		$xmlString .= '</allProducts>';
		return $xmlString;
	}
	
	public function getPritingMethods($product,$user)
    {
		$printingMethods = null;
		// $product = Mage::getModel('catalog/product')->load(341);
		$printingMethodCollection = Mage::getResourceSingleton('printingmethod/printingmethod_collection')
				// ->addStoreFilter(Mage::app()->getStore())
				->addFilter('status', 1)
				->addProductFilter($product);	
		
		foreach($printingMethodCollection as $printingMethod){			
			$printingmethodtitleCollection = Mage::getModel('printingmethod/printingmethodtitle')->getCollection()
												->addFieldToFilter('printing_method_id',$printingMethod->getEntityId())
												->addFieldToFilter('attribute_id',1)
												->addFieldToFilter('store_id',Mage::app()->getStore()->getStoreId());
			
			
			if($printingmethodtitleCollection->getSize() > 0 && $user=='')
			{
				$printingmethodName = $printingmethodtitleCollection->getFirstItem()->getTitle();			
			}else{		
				$printingmethodtitleCollection = Mage::getModel('printingmethod/printingmethodtitle')->getCollection()
												->addFieldToFilter('printing_method_id',$printingMethod->getEntityId())
												->addFieldToFilter('attribute_id',1)
												->addFieldToFilter('store_id',0);
				$printingmethodName = $printingmethodtitleCollection->getFirstItem()->getTitle();					
			}	
			
			$printingMethods .= '<printingMethod>';
			$printingMethods .= '<name>'.$printingmethodName.'</name>';
			$printingMethods .= '<printMethodId>'.$printingMethod->getEntityId().'</printMethodId>';
			/* $pricingLogic = $printingMethod->getPricingLogic();
			$printingLogics = Mage::helper('printingmethod')->getPricingLogics();			
			foreach($printingLogics as $printingLogic){
				if($printingLogic['value'] == $pricingLogic){
					$pricingLogicLabel = $printingLogic['label'];
				}
			}
			
			$printableColors = Mage::helper('printingmethod')->getPrintableColors();
			foreach($printableColors as $printableColor){
				if($printableColor['value'] == $printingMethod->getPrintableColors()){
					$printableColorLabel = $printableColor['label'];
				}
			} */
			
			$printingMethods .= '<pricingLogic>'.$printingMethod->getPricingLogic().'</pricingLogic>';			
			$printingMethods .= '<printableColorType>'.$printingMethod->getPrintableColors().'</printableColorType>';
			$printableColors = Mage::getModel('printingmethod/printablecolors')
					->getCollection()
					->addFieldToFilter('printing_method_id',$printingMethod->getEntityId())
					->setOrder('sort_order', 'DESC');
			$printingMethods .= '<printableColors>';
			foreach($printableColors as $printableColor){				
				$printableColorCollection = Mage::getModel('printingmethod/printablecolorstitle')->getCollection()
												->addFieldToFilter('color_id',$printableColor->getId())
												->addFieldToFilter('store_id',Mage::app()->getStore()->getStoreId());
			
				if($printableColorCollection->getSize() > 0 && $user=='')
				{
					$printableColorName = $printableColorCollection->getFirstItem()->getTitle();			
				}else{		
					$printableColorCollection = Mage::getModel('printingmethod/printablecolorstitle')->getCollection()
													->addFieldToFilter('color_id',$printableColor->getId())
													->addFieldToFilter('store_id',0);
					$printableColorName = $printableColorCollection->getFirstItem()->getTitle();					
				}	
			
				$printingMethods .= '<printableColor>';
				$printingMethods .= '<colorName>'.$printableColorName.'</colorName>';
				$printingMethods .= '<colorCode>'.$printableColor->getColorCode().'</colorCode>';
				$printingMethods .= '</printableColor>';
			}
			$printingMethods .= '</printableColors>';
			$printingMethods .= '<isImageUpload>'.$printingMethod->getIsImageUpload().'</isImageUpload>';
			$printMaxQty = '';
			if($printingMethod->getMaxQty() == 0){
				$printMaxQty = 999999;
			}else{
				$printMaxQty = $printingMethod->getMaxQty();
			}
			$printingMethods .= '<minQty>'.$printingMethod->getMinQty().'</minQty>';
			$printingMethods .= '<maxQty>'.$printMaxQty.'</maxQty>';
			// $printingMethods .= '<isNameNumber>'.$printingMethod->getIsNameNumber().'</isNameNumber>';
			$printingMethods .= '<namePrice>'.Mage::helper('core')->currency($printingMethod->getNamePrice(),true,false).'</namePrice>';
			$printingMethods .= '<numberPrice>'.Mage::helper('core')->currency($printingMethod->getNumberPrice(),true,false).'</numberPrice>';
			$printingMethods .= '<isAlert>'.$printingMethod->getIsAlert().'</isAlert>';
			
			$printingmethodAlertCollection = Mage::getModel('printingmethod/printingmethodtitle')->getCollection()
												->addFieldToFilter('printing_method_id',$printingMethod->getEntityId())
												->addFieldToFilter('attribute_id',2)
												->addFieldToFilter('store_id',Mage::app()->getStore()->getStoreId());
			
			if($printingmethodAlertCollection->getSize() > 0 && $user=='')
			{
				$printingmethodAlertMessage = $printingmethodAlertCollection->getFirstItem()->getTitle();			
			}else{		
				$printingmethodAlertCollection = Mage::getModel('printingmethod/printingmethodtitle')->getCollection()
												->addFieldToFilter('printing_method_id',$printingMethod->getEntityId())
												->addFieldToFilter('attribute_id',2)
												->addFieldToFilter('store_id',0);
				$printingmethodAlertMessage = $printingmethodAlertCollection->getFirstItem()->getTitle();					
			}				
			$printingMethods .= '<alertMessage>'.$printingmethodAlertMessage.'</alertMessage>';
			$printingMethods .= '</printingMethod>';
		}
		
		return $printingMethods;
	}
	
	public function renderChildrenCategory($catId)
	{		
		$children = Mage::getModel('catalog/category')->load($catId)->getChildren();
		
		if(!empty($children)) {
			$categories = explode(',', $children);
		
			foreach($categories as $categoryId) {
				$category = Mage::getModel('catalog/category')->load($categoryId);
				if (hasProducts($categoryId)) {
					$xmlString .= '<category>';
					$xmlString .= '<catName>'.$category->getName().'</catName>';
					$xmlString .=  '<catID>'.$category->getId().'</catID>';
					$xmlString .=  '<orderNo>'.$category->getPosition().'</orderNo>';
					$xmlString .=  '<catDesc>'.$category->getDescription().'</catDesc>';		
					$categoryImage = Mage::getModel("catalog/category")->load($category->getId())->getImage();
					if( $categoryImage!='')
						$xmlString .=  '<catThumb>'.$path.'/media/catalog/category/'. $categoryImage.'</catThumb>';
						
					$xmlString .=  '<type>'.'subcategory'.'</type>';
				}
					$this->renderChildrenCategory($categoryId);				
				if (hasProducts($categoryId)) {
					$xmlString .= "</category>";
				}
			}
		
		}		
		return $xmlString;
	}
	
	public function hasProducts($categoryId) {
		echo "categoryId".$categoryId;
		$designtoolAttrSetName = "3dDesigner";
		//Get Attribute set Id from Attribute set name
		$designtoolAttributeSetId = Mage::getModel('eav/entity_attribute_set')
                            ->load($designtoolAttrSetName, 'attribute_set_name')
                            ->getAttributeSetId();
		$products = Mage::getModel('catalog/category')->load($categoryId)
			->getProductCollection()
			->addAttributeToSelect('entity_id')
			->addAttributeToFilter('attribute_set_id',$designtoolAttributeSetId)
			->addAttributeToFilter('status', 1)
			->addAttributeToFilter('visibility', 4);
		//echo $products->getSelect();	
		
		return ( $products->count() > 0 )  ? true : false;
	}

} ?>