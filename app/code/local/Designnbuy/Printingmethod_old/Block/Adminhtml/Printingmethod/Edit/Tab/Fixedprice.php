<?php 
/**
 * Designnbuy_Printingmethod extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @copyright  	Copyright (c) 2014
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * store selection tab
 *
 * @category	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @author Ultimate Module Creator
 */
class Designnbuy_Printingmethod_Block_Adminhtml_Printingmethod_Edit_Tab_Fixedprice extends Mage_Adminhtml_Block_Widget_Form{
	/**
	 * prepare the form
	 * @access protected
	 * @return Designnbuy_Printingmethod_Block_Adminhtml_Printingmethod_Edit_Tab_Fixedprice
	 * @author Design N Buy
	 */
	public function __construct()
    {
        parent::__construct();
       
        $this->setTemplate('printingmethod/fixedprice.phtml');
    }   
   protected function _getProduct()
    {     
		//return Mage::registry('producttemplatecreator_data')->getProduct();		
    }
    
  
    
    public function getProductId()
    {
       
		
    }
    
    public function getPritingMethodId()
    {
        return $this->getRequest()->getParam('id');
    }
    
    public function getTabTitle()
    {
        return $this->__('Custom Options Images');
    }
   
    
    public function isHidden()
    {
        return ($this->getRequest()->getParam('store') != 0) ? true : false;
    }    
    
	
	/** options for every row, they will be rendered as dynamic row with inputs */
    public function getFixedPriceValues()
    {       
		$values = array();
		$value = array();
		$resource = Mage::getSingleton('core/resource');
		$adapter = $resource->getConnection('core_read');	
		
		$fixedPriceTable = $resource->getTableName('printingmethod/fixedprice');
		
		$select = $adapter->select()
					->from($fixedPriceTable, array('*')) // select * from tablename or use array('id','name') selected values
					->where('printing_method_id=?',$this->getPritingMethodId())              // where id =1
					->order('id DESC');              // where id =1
		
		$fixedPrices = $adapter->fetchAll($select); // return all rows
		foreach($fixedPrices as $fixedPrice){
			$values[] = new Varien_Object($fixedPrice);				
		}
		
        return $values;
    }
	
    /*protected function _getSelectOptionTypes()
    {
        return Mage::helper('swatches_customoptions')->getSelectOptionTypes();
    } */    
    /**
     * Determines whether to display the tab
     * Add logic here to decide whether you want the tab to display
     *
     * @return bool
     */
	//Code added to show only  configurable product configure area starts
    public function canShowTab()
    {    
		return true;		
    }
	
}