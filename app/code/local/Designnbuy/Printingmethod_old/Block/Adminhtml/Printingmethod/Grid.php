<?php
/**
 * Designnbuy_Printingmethod extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @copyright  	Copyright (c) 2014
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Printing Method admin grid block
 *
 * @category	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @author Ultimate Module Creator
 */
class Designnbuy_Printingmethod_Block_Adminhtml_Printingmethod_Grid extends Mage_Adminhtml_Block_Widget_Grid{
	/**
	 * constructor
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function __construct(){
		parent::__construct();
		$this->setId('printingmethodGrid');
		$this->setDefaultSort('entity_id');
		$this->setDefaultDir('ASC');
		$this->setSaveParametersInSession(true);
		$this->setUseAjax(true);
	}
	/**
	 * prepare collection
	 * @access protected
	 * @return Designnbuy_Printingmethod_Block_Adminhtml_Printingmethod_Grid
	 * @author Ultimate Module Creator
	 */
	protected function _prepareCollection(){
		$collection = Mage::getModel('printingmethod/printingmethod')->getCollection();
		
		$collection
				->getSelect()
				->joinLeft(array('pmv'=>Mage::getConfig()->getTablePrefix().'printingmethod_printingmethod_title'),'pmv.printing_method_id = main_table.entity_id',array('pmv.*'))
				->where('pmv.store_id = 0 and pmv.attribute_id = 1')		
				->group('main_table.entity_id');	
		
		$this->setCollection($collection);
		return parent::_prepareCollection();
	}
	/**
	 * prepare grid collection
	 * @access protected
	 * @return Designnbuy_Printingmethod_Block_Adminhtml_Printingmethod_Grid
	 * @author Ultimate Module Creator
	 */
	protected function _prepareColumns(){
		$this->addColumn('entity_id', array(
			'header'	=> Mage::helper('printingmethod')->__('Id'),
			'index'		=> 'entity_id',
			'type'		=> 'number'
		));
		$this->addColumn('title', array(
			'header'=> Mage::helper('printingmethod')->__('Printing Method Name'),
			'index' => 'title',
			'type'	 	=> 'text',

		));
		$this->addColumn('pricing_logic', array(
			'header'=> Mage::helper('printingmethod')->__('Pricing Logic'),
			'index' => 'pricing_logic',
			'type'	 	=> 'options',
			'options'    => array('1' => 'Fixed','2' => 'No. of Colors','3' => 'Artwork Size')
			
		));
		$this->addColumn('printable_colors', array(
			'header'=> Mage::helper('printingmethod')->__('Printable Colors'),
			'index' => 'printable_colors',
			'type'	 	=> 'options',
			'options'    => array('1' => 'Full','2' => 'Custom')

		));
		
		$this->addColumn('status', array(
			'header'	=> Mage::helper('printingmethod')->__('Status'),
			'index'		=> 'status',
			'type'		=> 'options',
			'options'	=> array(
				'1' => Mage::helper('printingmethod')->__('Enabled'),
				'0' => Mage::helper('printingmethod')->__('Disabled'),
			)
		));
		/*
		if (!Mage::app()->isSingleStoreMode()) {
			$this->addColumn('store_id', array(
				'header'=> Mage::helper('printingmethod')->__('Store Views'),
				'index' => 'store_id',
				'type'  => 'store',
				'store_all' => true,
				'store_view'=> true,
				'sortable'  => false,
				'filter_condition_callback'=> array($this, '_filterStoreCondition'),
			));
		}
		
		$this->addColumn('created_at', array(
			'header'	=> Mage::helper('printingmethod')->__('Created at'),
			'index' 	=> 'created_at',
			'width' 	=> '120px',
			'type'  	=> 'datetime',
		));
		$this->addColumn('updated_at', array(
			'header'	=> Mage::helper('printingmethod')->__('Updated at'),
			'index' 	=> 'updated_at',
			'width' 	=> '120px',
			'type'  	=> 'datetime',
		));
		*/
		$this->addColumn('action',
			array(
				'header'=>  Mage::helper('printingmethod')->__('Action'),
				'width' => '100',
				'type'  => 'action',
				'getter'=> 'getId',
				'actions'   => array(
					array(
						'caption'   => Mage::helper('printingmethod')->__('Edit'),
						'url'   => array('base'=> '*/*/edit'),
						'field' => 'id'
					)
				),
				'filter'=> false,
				'is_system'	=> true,
				'sortable'  => false,
		));
		$this->addExportType('*/*/exportCsv', Mage::helper('printingmethod')->__('CSV'));
		$this->addExportType('*/*/exportExcel', Mage::helper('printingmethod')->__('Excel'));
		$this->addExportType('*/*/exportXml', Mage::helper('printingmethod')->__('XML'));
		return parent::_prepareColumns();
	}
	/**
	 * prepare mass action
	 * @access protected
	 * @return Designnbuy_Printingmethod_Block_Adminhtml_Printingmethod_Grid
	 * @author Ultimate Module Creator
	 */
	protected function _prepareMassaction(){
		$this->setMassactionIdField('entity_id');
		$this->getMassactionBlock()->setFormFieldName('printingmethod');
		$this->getMassactionBlock()->addItem('delete', array(
			'label'=> Mage::helper('printingmethod')->__('Delete'),
			'url'  => $this->getUrl('*/*/massDelete'),
			'confirm'  => Mage::helper('printingmethod')->__('Are you sure?')
		));
		$this->getMassactionBlock()->addItem('status', array(
			'label'=> Mage::helper('printingmethod')->__('Change status'),
			'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
			'additional' => array(
				'status' => array(
						'name' => 'status',
						'type' => 'select',
						'class' => 'required-entry',
						'label' => Mage::helper('printingmethod')->__('Status'),
						'values' => array(
								'1' => Mage::helper('printingmethod')->__('Enabled'),
								'0' => Mage::helper('printingmethod')->__('Disabled'),
						)
				)
			)
		));
		return $this;
	}
	/**
	 * get the row url
	 * @access public
	 * @param Designnbuy_Printingmethod_Model_Printingmethod
	 * @return string
	 * @author Ultimate Module Creator
	 */
	public function getRowUrl($row){
		return $this->getUrl('*/*/edit', array('id' => $row->getId()));
	}
	/**
	 * get the grid url
	 * @access public
	 * @return string
	 * @author Ultimate Module Creator
	 */
	public function getGridUrl(){
		return $this->getUrl('*/*/grid', array('_current'=>true));
	}
	/**
	 * after collection load
	 * @access protected
	 * @return Designnbuy_Printingmethod_Block_Adminhtml_Printingmethod_Grid
	 * @author Ultimate Module Creator
	 */
	protected function _afterLoadCollection(){
		$this->getCollection()->walk('afterLoad');
		parent::_afterLoadCollection();
	}
	/**
	 * filter store column
	 * @access protected
	 * @param Designnbuy_Printingmethod_Model_Resource_Printingmethod_Collection $collection
	 * @param Mage_Adminhtml_Block_Widget_Grid_Column $column
	 * @return Designnbuy_Printingmethod_Block_Adminhtml_Printingmethod_Grid
	 * @author Ultimate Module Creator
	 */
	protected function _filterStoreCondition($collection, $column){
		if (!$value = $column->getFilter()->getValue()) {
        	return;
		}
		$collection->addStoreFilter($value);
		return $this;
    }
}