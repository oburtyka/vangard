<?php
/**
 * Designnbuy_Printingmethod extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @copyright  	Copyright (c) 2014
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Printing Method edit form tab
 *
 * @category	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @author Ultimate Module Creator
 */
class Designnbuy_Printingmethod_Block_Adminhtml_Printingmethod_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form{	
	/**
	 * prepare the form
	 * @access protected
	 * @return Printingmethod_Printingmethod_Block_Adminhtml_Printingmethod_Edit_Tab_Form
	 * @author Ultimate Module Creator
	 */
	protected function _prepareForm(){
		$form = new Varien_Data_Form();
		$form->setHtmlIdPrefix('printingmethod_');
		$form->setFieldNameSuffix('printingmethod');			
		
		$this->setForm($form);
		
		$storeObj = new Varien_Object();		
		$storeObj->setId($this->getRequest()->getParam("id"));
		$storeObj->setStoreId($this->getRequest()->getParam("store"));
		$form->setDataObject($storeObj);
		
		$fieldset = $form->addFieldset('printingmethod_form', array('legend'=>Mage::helper('printingmethod')->__('Printing Method')));
		$formData = Mage::registry('printingmethod_data');
				
		$name = $fieldset->addField('printing_method', 'text', array(
			'label' => Mage::helper('printingmethod')->__('Printing Method Name'),
			'name'  => 'printing_method',
			'required'  => true,
			'class' => 'required-entry',

		));		
		if ($name)
		{
			$form->getElement('printing_method')->setRenderer(Mage::app()->getLayout()
				->createBlock('printingmethod/adminhtml_printingmethod_edit_tab_renderer_name'));				
		}
		$fieldset->addField('pricing_logic', 'select', array( 
			'label' => Mage::helper('printingmethod')->__('Pricing Logic'), 
			'class' => 'required-entry', 
			'required' => true, 
			'name' => 'pricing_logic', 
			'onclick' => "", 
			'onchange' => "", 
			'value' => '1', 
			'values' => array(
						''=> Mage::helper('printingmethod')->__('Please Select..'),
						'1' => Mage::helper('printingmethod')->__('Fixed'),
						'2' => Mage::helper('printingmethod')->__('No. of Colors'),
						'3' => Mage::helper('printingmethod')->__('Artwork Size')
						), 
			'disabled' => false, 
			'readonly' => false, 
			'after_element_html' => '<small>Comments</small>', 
			'tabindex' => 1 
		));
		
		$fieldset->addField('printable_colors', 'select', array( 
			'label' => Mage::helper('printingmethod')->__('Printable Colors'), 
			'class' => 'required-entry', 
			'required' => true, 
			'name' => 'printable_colors', 
			'onclick' => "", 
			'onchange' => "", 
			'value' => '1', 
			'values' => array(
						''=> Mage::helper('printingmethod')->__('Please Select..'),
						'1' => Mage::helper('printingmethod')->__('Full'),
						'2' => Mage::helper('printingmethod')->__('Custom')					
						), 
			'disabled' => false, 
			'readonly' => false, 
			'after_element_html' => '<small>Comments</small>', 
			'tabindex' => 1 
		));
		
		$fieldset->addField('min_qty', 'text', array(
			'label' => Mage::helper('printingmethod')->__('Minimum Quantity'),
			'name'  => 'min_qty',
			'required'  => true,
			'class' => 'required-entry validate-zero-or-greater',
			'after_element_html' => '<small>Min. Order Qunatity should not be less than Product Settings.</small>',
		));
		
		$fieldset->addField('max_qty', 'text', array(
			'label' => Mage::helper('printingmethod')->__('Maximum Quantity'),
			'name'  => 'max_qty',
			'required'  => true,
			'class' => 'required-entry validate-zero-or-greater',
			'after_element_html' => '<small>Max. Order Quantity should not be more than Product Settings.</small>',
		));
		/*
		$fieldset->addField('is_name_number', 'checkbox', array(
          'label'     => Mage::helper('printingmethod')->__('Enable Name/Number'),
          'name'      => 'is_name_number',
          'checked' => $this->getIsAlert() == 1 ? 'true' : 'false',
          'onclick'    => 'this.value = this.checked ? 1 : 0;',
          'onchange' => "",
          //'value'  => '1',
          'disabled' => false,
          'after_element_html' => '<small>Select to show name and number feature in design studio. Name & Number only applicable for 2D designer(designtool attribute set) product.</small>',
          'tabindex' => 1
        ))->setIsChecked(!empty($formData['is_name_number']));
		*/
		$fieldset->addField('name_price', 'text', array(
			'label' => Mage::helper('printingmethod')->__('Charge for Name'),
			'name'  => 'name_price',
			'required'  => true,
			'class' => 'required-entry validate-zero-or-greater',

		));
		
		$fieldset->addField('number_price', 'text', array(
			'label' => Mage::helper('printingmethod')->__('Charge for Number'),
			'name'  => 'number_price',
			'required'  => true,
			'class' => 'required-entry validate-zero-or-greater',

		));
		
		$fieldset->addField('is_image_upload', 'checkbox', array(
          'label'     => Mage::helper('printingmethod')->__('Enable Image Upload'),
          'name'      => 'is_image_upload',
          'checked' => $this->getIsAlert() == 1 ? 'true' : 'false',
          'onclick'    => 'this.value = this.checked ? 1 : 0;',
          'onchange' => "",
          //'value'  => '1',
          'disabled' => false,
          'after_element_html' => '<small>Select to show image upload feature in design studio.</small>',
          'tabindex' => 1
        ))->setIsChecked(!empty($formData['is_image_upload']));
		
		$fieldset->addField('image_price', 'text', array(
			'label' => Mage::helper('printingmethod')->__('Charge for Image Upload'),
			'name'  => 'image_price',
			'required'  => true,
			'class' => 'required-entry validate-zero-or-greater',

		));
		
		$fieldset->addField('artwork_setup_price_type', 'select', array( 
			'label' => Mage::helper('printingmethod')->__('Artwork Setup Fee Type'), 
			'class' => 'required-entry', 
			'required' => true, 
			'name' => 'artwork_setup_price_type', 
			'onclick' => "", 
			'onchange' => "", 
			'value' => '1', 
			'values' => array(
						''=> Mage::helper('printingmethod')->__('Please Select..'),
						'1' => Mage::helper('printingmethod')->__('Fixed'),
						'2' => Mage::helper('printingmethod')->__('Per Color')					
						), 
			'disabled' => false, 
			'readonly' => false, 
			'after_element_html' => '<small>Consider this as screen setup fee for Screen Printing or digitization fee for embroidery. It can be defined as fixed fee or based on no. Of colors in artwork.</small>', 
			'tabindex' => 1 
		));		
		
		$fieldset->addField('artwork_setup_price', 'text', array(
			'label' => Mage::helper('printingmethod')->__('Artwork Setup Fee'),
			'name'  => 'artwork_setup_price',
			'required'  => true,
			'class' => 'required-entry validate-zero-or-greater',

		));
		
		$fieldset->addField('is_alert', 'checkbox', array(
          'label'     => Mage::helper('printingmethod')->__('Enable Alert'),
          'name'      => 'is_alert',
          'checked' => $this->getIsAlert() == 1 ? 'true' : 'false',
          'onclick'    => 'this.value = this.checked ? 1 : 0;',
          'onchange' => "",
          //'value'  => '1',
          'disabled' => false,
          'after_element_html' => '<small>Select to show alert message in design studio.</small>',
          'tabindex' => 1
        ))->setIsChecked(!empty($formData['is_alert']));
		

		$message = $fieldset->addField('alert_message', 'textarea', array(
			'label' => Mage::helper('printingmethod')->__('Alert Message'),
			'name'  => 'alert_message',
			'after_element_html' => '<small>This alert is shown to user in design studio when he switch from this printing method to another.</small>',

		));
		if ($message)
		{
			$form->getElement('alert_message')->setRenderer(Mage::app()->getLayout()
				->createBlock('printingmethod/adminhtml_printingmethod_edit_tab_renderer_message'));				
		}
		$fieldset->addField('status', 'select', array(
			'label' => Mage::helper('printingmethod')->__('Status'),
			'name'  => 'status',
			'values'=> array(
				array(
					'value' => 1,
					'label' => Mage::helper('printingmethod')->__('Enabled'),
				),
				array(
					'value' => 0,
					'label' => Mage::helper('printingmethod')->__('Disabled'),
				),
			),
		));
		if (Mage::getSingleton('adminhtml/session')->getPrintingmethodData()){
			$form->setValues(Mage::getSingleton('adminhtml/session')->getPrintingmethodData());
			Mage::getSingleton('adminhtml/session')->setPrintingmethodData(null);
		}
		elseif (Mage::registry('current_printingmethod')){
			if(Mage::app()->getRequest()->getParam('store')!=''){
			$storeId = Mage::app()->getRequest()->getParam('store');
		}else{
			$storeId = 0;
		}
		/*For Printing Method Title Consider attribute_id = 1*/
		$printingmethodtitle = Mage::getModel('printingmethod/printingmethodtitle')->getCollection()
										->addFieldToFilter('printing_method_id',$this->getRequest()->getParam('id'))
										->addFieldToFilter('attribute_id',1)
										->addFieldToFilter('store_id',$storeId);		
		
		if($printingmethodtitle->getSize() > 0)
		{
			$data = Mage::registry('current_printingmethod')->getData();
			$data['printing_method'] = $printingmethodtitle->getFirstItem()->getTitle();			
		}else{		
			$printingmethodtitle = Mage::getModel('printingmethod/printingmethodtitle')->getCollection()
										->addFieldToFilter('printing_method_id',$this->getRequest()->getParam('id'))
										->addFieldToFilter('attribute_id',1)
										->addFieldToFilter('store_id',0);
			$data = Mage::registry('current_printingmethod')->getData();	
			$data['printing_method'] = $printingmethodtitle->getFirstItem()->getTitle();					
		}
		
		/*For Printing Method Alert Message Consider attribute_id = 2*/
		$printingmethodAlert = Mage::getModel('printingmethod/printingmethodtitle')->getCollection()
										->addFieldToFilter('printing_method_id',$this->getRequest()->getParam('id'))
										->addFieldToFilter('attribute_id',2)
										->addFieldToFilter('store_id',$storeId);		
	
		if($printingmethodAlert->getSize() > 0)
		{
			$data['alert_message'] = $printingmethodAlert->getFirstItem()->getTitle();			
		}else{		
			$printingmethodAlert = Mage::getModel('printingmethod/printingmethodtitle')->getCollection()
										->addFieldToFilter('printing_method_id',$this->getRequest()->getParam('id'))
										->addFieldToFilter('attribute_id',2)
										->addFieldToFilter('store_id',0);
			$data['alert_message'] = $printingmethodAlert->getFirstItem()->getTitle();					
		}	
		
        $form->setValues($data);
			
		// $form->setValues(Mage::registry('current_printingmethod')->getData());
		}
		return parent::_prepareForm();
	}
}