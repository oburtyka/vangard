<?php
class Designnbuy_Printingmethod_Block_Adminhtml_Printingmethod_Edit_Tab_Renderer_Message extends Mage_Adminhtml_Block_Widget implements Varien_Data_Form_Element_Renderer_Interface {
	public function render(Varien_Data_Form_Element_Abstract $element) {

		if ($this->canDisplayUseDefault($element) 
				&& $this->nameInStore($element)==false):
			$element->setDisabled(true);
		endif;
		
		$disabled = true;
		$title = 'use_default';
		$idName = "message_default[]";
		$html = '<td class="label">';
		$html.= ''.$element->getLabelHtml().'';
		$html.= '</td>';
		$html.= '<td class="value">';
		$html.= ''.$element->getElementHtml().'';		
		$html.= '</td>';
		//echo "ajay".$this->canDisplayUseDefault($element);
		$html.= '<td class="scope-label"><span class="nobr">[STORE VIEW]</span></td>';
		if ($this->canDisplayUseDefault($element)):			
			$html.= '<td class="value use-default"><input class="checkbox config-inherit" id="message_default" onclick="toggleValueElements(this, this.parentNode.parentNode)" type="checkbox" name="'.$idName.'" value="message" ' . ($this->nameInStore($element) ? '' : 'checked="checked"') . ' />
			<label for="message_default" class="normal">'.$this->__('Use Default Value').'</label></td>';
		endif;
		return $html;
	}
	
	public function nameInStore($element)
    {
		$formData = $element->getForm()->getDataObject();		
		$printingmethodtitle = Mage::getModel('printingmethod/printingmethodtitle')->getCollection()
										->addFieldToFilter('printing_method_id',$formData->getId())
										->addFieldToFilter('attribute_id',2)
										->addFieldToFilter('store_id',$formData->getStoreId());		
		
		if($printingmethodtitle->getSize() > 0 
			&& $formData->getId()
			&& $formData->getStoreId())
        {
			return true;
        }   			
        return false;
    }
	
	public function canDisplayUseDefault($element)
    {		
		$formData = $element->getForm()->getDataObject();
            if ($this->getData()
				&& $formData->getId()
                && $formData->getStoreId()) {
                return true;
            }        
        return false;
    }
}
?>