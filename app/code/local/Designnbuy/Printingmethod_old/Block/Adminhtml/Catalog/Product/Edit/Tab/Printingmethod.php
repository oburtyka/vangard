<?php
/**
 * Designnbuy_Printingmethod extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @copyright  	Copyright (c) 2014
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Printing Method tab on product edit form
 *
 * @category	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @author Ultimate Module Creator
 */
class Designnbuy_Printingmethod_Block_Adminhtml_Catalog_Product_Edit_Tab_Printingmethod extends Mage_Adminhtml_Block_Widget_Grid{
	/**
	 * Set grid params
	 * @access protected
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function __construct(){
		parent::__construct();
		$this->setId('printingmethod_grid');
		$this->setDefaultSort('position');
		$this->setDefaultDir('ASC');
		$this->setUseAjax(true);
		if ($this->getProduct()->getId()) {
			$this->setDefaultFilter(array('in_printingmethods'=>1));
		}
	}
	/**
	 * prepare the printingmethod collection
	 * @access protected 
	 * @return Designnbuy_Printingmethod_Block_Adminhtml_Catalog_Product_Edit_Tab_Printingmethod
	 * @author Ultimate Module Creator
	 */
	protected function _prepareCollection() {
		$collection = Mage::getResourceModel('printingmethod/printingmethod_collection');
		if ($this->getProduct()->getId()){
			$constraint = 'related.product_id='.$this->getProduct()->getId();
			}
			else{
				$constraint = 'related.product_id=0';
			}
		$collection->getSelect()->joinLeft(
			array('related'=>$collection->getTable('printingmethod/printingmethod_product')),
			'related.printingmethod_id=main_table.entity_id AND '.$constraint,
			array('position')
		);
		$this->setCollection($collection);
		parent::_prepareCollection();
		return $this;
	}
	/**
	 * prepare mass action grid
	 * @access protected
	 * @return Designnbuy_Printingmethod_Block_Adminhtml_Catalog_Product_Edit_Tab_Printingmethod
	 * @author Ultimate Module Creator
	 */ 
	protected function _prepareMassaction(){
		return $this;
	}
	/**
	 * prepare the grid columns
	 * @access protected
	 * @return Designnbuy_Printingmethod_Block_Adminhtml_Catalog_Product_Edit_Tab_Printingmethod
	 * @author Ultimate Module Creator
	 */
	protected function _prepareColumns(){
		$this->addColumn('in_printingmethods', array(
			'header_css_class'  => 'a-center',
			'type'  => 'checkbox',
			'name'  => 'in_printingmethods',
			'values'=> $this->_getSelectedPrintingmethods(),
			'align' => 'center',
			'index' => 'entity_id'
		));
		$this->addColumn('printing_method', array(
			'header'=> Mage::helper('printingmethod')->__('Printing Method Name'),
			'align' => 'left',
			'index' => 'printing_method',
		));
		$this->addColumn('position', array(
			'header'		=> Mage::helper('printingmethod')->__('Position'),
			'name'  		=> 'position',
			'width' 		=> 60,
			'type'		=> 'number',
			'validate_class'=> 'validate-number',
			'index' 		=> 'position',
			'editable'  	=> true,
		));
	}
	/**
	 * Retrieve selected printingmethods
	 * @access protected
	 * @return array
	 * @author Ultimate Module Creator
	 */
	protected function _getSelectedPrintingmethods(){
		$printingmethods = $this->getProductPrintingmethods();
		if (!is_array($printingmethods)) {
			$printingmethods = array_keys($this->getSelectedPrintingmethods());
		}
		return $printingmethods;
	}
 	/**
	 * Retrieve selected printingmethods
	 * @access protected
	 * @return array
	 * @author Ultimate Module Creator
	 */
	public function getSelectedPrintingmethods() {
		$printingmethods = array();
		//used helper here in order not to override the product model
		$selected = Mage::helper('printingmethod/product')->getSelectedPrintingmethods(Mage::registry('current_product'));
		if (!is_array($selected)){
			$selected = array();
		}
		foreach ($selected as $printingmethod) {
			$printingmethods[$printingmethod->getId()] = array('position' => $printingmethod->getPosition());
		}
		return $printingmethods;
	}
	/**
	 * get row url
	 * @access public
	 * @return string
	 * @author Ultimate Module Creator
	 */
	public function getRowUrl($item){
		return '#';
	}
	/**
	 * get grid url
	 * @access public
	 * @return string
	 * @author Ultimate Module Creator
	 */
	public function getGridUrl(){
		return $this->getUrl('*/*/printingmethodsGrid', array(
			'id'=>$this->getProduct()->getId()
		));
	}
	/**
	 * get the current product
	 * @access public
	 * @return Mage_Catalog_Model_Product
	 * @author Ultimate Module Creator
	 */
	public function getProduct(){
		return Mage::registry('current_product');
	}
	/**
	 * Add filter
	 * @access protected
	 * @param object $column
	 * @return Designnbuy_Printingmethod_Block_Adminhtml_Catalog_Product_Edit_Tab_Printingmethod
	 * @author Ultimate Module Creator
	 */
	protected function _addColumnFilterToCollection($column){
		if ($column->getId() == 'in_printingmethods') {
			$printingmethodIds = $this->_getSelectedPrintingmethods();
			if (empty($printingmethodIds)) {
				$printingmethodIds = 0;
			}
			if ($column->getFilter()->getValue()) {
				$this->getCollection()->addFieldToFilter('entity_id', array('in'=>$printingmethodIds));
			} 
			else {
				if($printingmethodIds) {
					$this->getCollection()->addFieldToFilter('entity_id', array('nin'=>$printingmethodIds));
				}
			}
		} 
		else {
			parent::_addColumnFilterToCollection($column);
		}
		return $this;
	}
}