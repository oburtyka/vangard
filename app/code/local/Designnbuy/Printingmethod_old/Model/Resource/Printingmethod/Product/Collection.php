<?php 
/**
 * Designnbuy_Printingmethod extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @copyright  	Copyright (c) 2014
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Printing Method - product relation resource model collection
 *
 * @category	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @author Ultimate Module Creator
 */
class Designnbuy_Printingmethod_Model_Resource_Printingmethod_Product_Collection extends Mage_Catalog_Model_Resource_Product_Collection{
	/**
	 * remember if fields have been joined
	 * @var bool
	 */
	protected $_joinedFields = false;
	/**
	 * join the link table
	 * @access public
	 * @return Designnbuy_Printingmethod_Model_Resource_Printingmethod_Product_Collection
	 * @author Ultimate Module Creator
	 */
	public function joinFields(){
		if (!$this->_joinedFields){
			$this->getSelect()->join(
				array('related' => $this->getTable('printingmethod/printingmethod_product')),
				'related.product_id = e.entity_id',
				array('position')
			);
			$this->_joinedFields = true;
		}
		return $this;
	}
	/**
	 * add printingmethod filter
	 * @access public
	 * @param Designnbuy_Printingmethod_Model_Printingmethod | int $printingmethod
	 * @return Designnbuy_Printingmethod_Model_Resource_Printingmethod_Product_Collection
	 * @author Ultimate Module Creator
	 */
	public function addPrintingmethodFilter($printingmethod){
		if ($printingmethod instanceof Designnbuy_Printingmethod_Model_Printingmethod){
			$printingmethod = $printingmethod->getId();
		}
		if (!$this->_joinedFields){
			$this->joinFields();
		}
		$this->getSelect()->where('related.printingmethod_id = ?', $printingmethod);
		return $this;
	}
}