<?php
class Designnbuy_Printingmethod_Model_Observer{
	public function invoiceSaveAfter(Varien_Event_Observer $observer)
	{
		$invoice = $observer->getEvent()->getInvoice();
		if ($invoice->getBaseSetupfeeAmount()) {
			$order = $invoice->getOrder();
			$order->setSetupfeeAmountInvoiced($order->getSetupfeeAmountInvoiced() + $invoice->getSetupfeeAmount());
			$order->setBaseSetupfeeAmountInvoiced($order->getBaseSetupfeeAmountInvoiced() + $invoice->getBaseSetupfeeAmount());
		}
		return $this;
	}
	public function creditmemoSaveAfter(Varien_Event_Observer $observer)
	{
		/* @var $creditmemo Mage_Sales_Model_Order_Creditmemo */
		$creditmemo = $observer->getEvent()->getCreditmemo();
		if ($creditmemo->getSetupfeeAmount()) {
			$order = $creditmemo->getOrder();
			$order->setSetupfeeAmountRefunded($order->getSetupfeeAmountRefunded() + $creditmemo->getSetupfeeAmount());
			$order->setBaseSetupfeeAmountRefunded($order->getBaseSetupfeeAmountRefunded() + $creditmemo->getBaseSetupfeeAmount());
		}
		return $this;
	}
	public function updatePaypalTotal($evt){
		$cart = $evt->getPaypalCart();
		$cart->updateTotal(Mage_Paypal_Model_Cart::TOTAL_SUBTOTAL,$cart->getSalesEntity()->getSetupfeeAmount());
	}
}