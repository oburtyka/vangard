<?php
/**
 * Designnbuy_Printingmethod extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @copyright  	Copyright (c) 2014
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Printing Method admin controller
 *
 * @category	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @author Ultimate Module Creator
 */
class Designnbuy_Printingmethod_Adminhtml_Printingmethod_PrintingmethodController extends Designnbuy_Printingmethod_Controller_Adminhtml_Printingmethod{
	/**
	 * init the printingmethod
	 * @access protected
	 * @return Designnbuy_Printingmethod_Model_Printingmethod
	 */
	protected function _initPrintingmethod(){
		$printingmethodId  = (int) $this->getRequest()->getParam('id');
		$printingmethod	= Mage::getModel('printingmethod/printingmethod');
		if ($printingmethodId) {
			$printingmethod->load($printingmethodId);
		}
		Mage::register('current_printingmethod', $printingmethod);
		return $printingmethod;
	}
 	/**
	 * default action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function indexAction() {
		$this->loadLayout();
		$this->_title(Mage::helper('printingmethod')->__('Printingmethod'))
			 ->_title(Mage::helper('printingmethod')->__('Printing Methods'));
		$this->renderLayout();
	}
	/**
	 * grid action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function gridAction() {
		$this->loadLayout()->renderLayout();
	}
	/**
	 * edit printing method - action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function editAction() {
		$printingmethodId	= $this->getRequest()->getParam('id');
		$printingmethod  	= $this->_initPrintingmethod();
		if ($printingmethodId && !$printingmethod->getId()) {
			$this->_getSession()->addError(Mage::helper('printingmethod')->__('This printing method no longer exists.'));
			$this->_redirect('*/*/');
			return;
		}
		$data = Mage::getSingleton('adminhtml/session')->getFormData(true);
		if (!empty($data)) {
			$printingmethod->setData($data);
		}
		Mage::register('printingmethod_data', $printingmethod);
		$this->loadLayout();
		if ($printingmethod->getId()){
			$this->_addContent($this->getLayout()->createBlock('printingmethod/adminhtml_printingmethod_edit'))
			->_addLeft($this->getLayout()->createBlock('adminhtml/store_switcher'))
			->_addLeft($this->getLayout()->createBlock('printingmethod/adminhtml_printingmethod_edit_tabs'));
		}else{
			$this->_addContent($this->getLayout()->createBlock('printingmethod/adminhtml_printingmethod_edit'))
			->_addLeft($this->getLayout()->createBlock('printingmethod/adminhtml_printingmethod_edit_tabs'));
		}
		
		$this->_title(Mage::helper('printingmethod')->__('Printingmethod'))
			 ->_title(Mage::helper('printingmethod')->__('Printing Methods'));
		if ($printingmethod->getId()){
			$this->_title($printingmethod->getPrintingMethod());
		}
		else{
			$this->_title(Mage::helper('printingmethod')->__('Add printing method'));
		}
		if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) { 
			$this->getLayout()->getBlock('head')->setCanLoadTinyMce(true); 
		}
		
		$this->renderLayout();
	}
	/**
	 * new printing method action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function newAction() {
		$this->_forward('edit');
	}
	/**
	 * save printing method - action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function saveAction() {
		if ($data = $this->getRequest()->getPost()) {	
			
			$resource = Mage::getSingleton('core/resource');
			$adapter = $resource->getConnection('core_write');	
			
			try {
				$data['printingmethod']['stores'][0] = 0;
				
				$printableColor = $this->getRequest()->getPost('color');
				
				$colorAction = $this->getRequest()->getPost('colorAction');
				
				$deleteIds = $colorAction['delete'];
				$printableColorModel = Mage::getModel('printingmethod/printablecolors');
				$findOption   = 'option_';				
				if ($deleteIds) {			
					foreach($deleteIds as $id => $deleteId) {						
						if($deleteId == 1)
						{
							$printableColorModel->load($id);							
							$printableColorTitleCollection = Mage::getModel('printingmethod/printablecolorstitle')->getCollection()
													->addFieldToFilter('color_id',$id);
							foreach($printableColorTitleCollection as $printableColorTitles) {
								$printableColorModel->delete();
								$printableColorTitle = Mage::getModel('printingmethod/printablecolorstitle')
														->setId($printableColorTitles->getId());
								$printableColorTitle->delete();								
							}																				
						}
					}
				}	
				
				foreach($printableColor['color_code'] as $key => $colorCode){					
					$pos = strpos($key, $findOption);				
					
					$printableColorModel = Mage::getModel('printingmethod/printablecolors');
					if ($pos === false) {
						$printableColorModel->setId($key);
					}
					
					$printableColorModel->setPrintingMethodId($printableColor[$key]['printing_method_id']);	
					$printableColorModel->setColorCode($colorCode);	
					$printableColorModel->setColorName($printableColor['value'][$key][0]);	
					$printableColorModel->setSortOrder($printableColor['sort_order'][$key]);	
					$printableColorModel->save();
					
					foreach($printableColor['value'][$key] as $printkey => $printableColorName){						
						
						$printableColorData = Mage::getModel('printingmethod/printablecolorstitle')->getCollection()
												->addFieldToFilter('color_id',$printableColorModel->getId())
												->addFieldToFilter('store_id',$printkey);
						
						if($printableColorData->getSize() > 0){
							if($printableColorName!=''){
								$printColorStoreId = $printableColorData->getFirstItem()->getId();
								$printablecolorstitle = Mage::getModel('printingmethod/printablecolorstitle');
								$printablecolorstitle->setTitle($printableColorName);
								$printablecolorstitle->setId($printColorStoreId);
								$printablecolorstitle->save();
							}
						}else{
							if($printableColorName!=''){
								$storeTitleData['color_id'] = $printableColorModel->getId();
								$storeTitleData['store_id'] = $printkey;
								$storeTitleData['title'] = $printableColorName;
								$printablecolorstitle = Mage::getModel('printingmethod/printablecolorstitle');
								$printablecolorstitle->setData($storeTitleData);						
								$printablecolorstitle->save();
							}
						}
					}
				}
				$printingMetthodData = $data['printingmethod'];
				
				$printingMetthodData['is_alert'] = isset($data['printingmethod']['is_alert']) ? 1 : 0;
				$printingMetthodData['is_name_number'] = isset($data['printingmethod']['is_name_number']) ? 1 : 0;
				$printingMetthodData['is_image_upload'] = isset($data['printingmethod']['is_image_upload']) ? 1 : 0;
				$printingmethod = $this->_initPrintingmethod();
				
				$printingmethod->addData($printingMetthodData);
				$products = $this->getRequest()->getPost('products', -1);
				if ($products != -1) {
					$printingmethod->setProductsData(Mage::helper('adminhtml/js')->decodeGridSerializedInput($products));
				}
				// $printingmethod->save();
				
				if( Mage::app()->getRequest()->getParam('store') == 0 || Mage::app()->getRequest()->getParam('store') == ''){
					$printingmethod->save();
				}
				
				if($data['name_default'][0]==''){
					if(Mage::app()->getRequest()->getParam('store')!=''){
						$printingmethodtitle['store_id'] = Mage::app()->getRequest()->getParam('store');
					}else{
						$printingmethodtitle['store_id'] = 0;
					}
					$printingmethodtitle['title'] = $data['printingmethod']['printing_method'];
					$printingmethodtitle['printing_method_id'] = $printingmethod->getId();
					$printingmethodtitle['attribute_id'] = 1;
					
					$printingmethodtitleCollection = Mage::getModel('printingmethod/printingmethodtitle')->getCollection()
												->addFieldToFilter('printing_method_id',$printingmethodtitle['printing_method_id'])
												->addFieldToFilter('store_id',$printingmethodtitle['store_id'])
												->addFieldToFilter('attribute_id',1);
					/*For Printing Method Title Consider attribute_id = 1*/
					$printingMethodTitleData = $printingmethodtitleCollection->getFirstItem();
					$printingmethodTitleModel = Mage::getModel('printingmethod/printingmethodtitle');
					$printingmethodTitleModel->setData($printingmethodtitle);
					if(!empty($printingMethodTitleData)){
						$printingmethodTitleModel->setId($printingMethodTitleData->getId());
					}
					
					$printingmethodTitleModel->save();
				}else{					
					$printingmethodtitleCollection = Mage::getModel('printingmethod/printingmethodtitle')->getCollection()
												->addFieldToFilter('printing_method_id',Mage::app()->getRequest()->getParam('id'))
												->addFieldToFilter('attribute_id',1)
												->addFieldToFilter('store_id',Mage::app()->getRequest()->getParam('store'));
					/*For Printing Method Title Consider attribute_id = 1*/
					
					$printingmethodTitleId = $printingmethodtitleCollection->getFirstItem()->getId();
					Mage::getModel('printingmethod/printingmethodtitle')->setId($printingmethodTitleId)->delete();						
				}
				
				if($data['message_default'][0]==''){
					if(Mage::app()->getRequest()->getParam('store')!=''){
						$printingmethodAlert['store_id'] = Mage::app()->getRequest()->getParam('store');
					}else{
						$printingmethodAlert['store_id'] = 0;
					}
					$printingmethodAlert['title'] = $data['printingmethod']['alert_message'];
					$printingmethodAlert['printing_method_id'] = $printingmethod->getId();
					$printingmethodAlert['attribute_id'] = 2;
										
					$printingmethodAlertCollection = Mage::getModel('printingmethod/printingmethodtitle')->getCollection()
															->addFieldToFilter('printing_method_id',$printingmethodAlert['printing_method_id'])
												->addFieldToFilter('store_id',$printingmethodAlert['store_id'])
												->addFieldToFilter('attribute_id',2);					
					/*For Printing Method Alert Message Consider attribute_id = 2*/
					
					$printingMethodAlertData = $printingmethodAlertCollection->getFirstItem();
					$printingmethodAlertModel = Mage::getModel('printingmethod/printingmethodtitle');
					$printingmethodAlertModel->setData($printingmethodAlert);
					if(!empty($printingMethodAlertData)){
						$printingmethodAlertModel->setId($printingMethodAlertData->getId());
					}
					
					$printingmethodAlertModel->save();
				}else{					
					$printingmethodAlertCollection = Mage::getModel('printingmethod/printingmethodtitle')->getCollection()
												->addFieldToFilter('printing_method_id',Mage::app()->getRequest()->getParam('id'))
												->addFieldToFilter('attribute_id',2)
												->addFieldToFilter('store_id',Mage::app()->getRequest()->getParam('store'));
					/*For Printing Method Alert Message Consider attribute_id = 2*/
					
					$printingmethodAlertId = $printingmethodAlertCollection->getFirstItem()->getId();
					Mage::getModel('printingmethod/printingmethodtitle')->setId($printingmethodAlertId)->delete();						
				}
				
				$qcPriceTable = $resource->getTableName('printingmethod/qcprice');
				if (isset($data['qcPrice'])) {
					$qcPrice = $data['qcPrice'];
					foreach ($qcPrice['qty'] as $optionId => $values) {
						$intOptionId = (int) $optionId;					
						if (!empty($qcPrice['delete'][$optionId])) {
							if ($intOptionId) {						
								$adapter->delete($qcPriceTable, array('id = ?' => $intOptionId));
							}
							continue;
						}
						
						if (!$intOptionId) {
							$qcPriceData = array(
							   'id'  => $optionId,
							   'qty_range_id'  => $values,
							   'printing_method_id'    => $this->getRequest()->getParam('id'),
							   'noofcolor'    => $data['qcPrice']['noofcolor'][$optionId],
							   'first_side_price'    => $data['qcPrice']['first_side_price'][$optionId],
							   'second_side_price'    => $data['qcPrice']['second_side_price'][$optionId],
							   'third_side_price'    => $data['qcPrice']['third_side_price'][$optionId],
							   'fourth_side_price'    => $data['qcPrice']['fourth_side_price'][$optionId],
							);						
							$adapter->insert($qcPriceTable, $qcPriceData);
							$intOptionId = $adapter->lastInsertId($qcPriceTable);
						} else {
							$qcPriceData = array(
							   // 'id'  => $optionId,
							   'qty_range_id'  => $values,
							   'printing_method_id'    => $this->getRequest()->getParam('id'),
							   'noofcolor'    => $data['qcPrice']['noofcolor'][$optionId],
							   'first_side_price'    => $data['qcPrice']['first_side_price'][$optionId],
							   'second_side_price'    => $data['qcPrice']['second_side_price'][$optionId],
							   'third_side_price'    => $data['qcPrice']['third_side_price'][$optionId],
							   'fourth_side_price'    => $data['qcPrice']['fourth_side_price'][$optionId],
							);
							
							$where = array('id =?' => $intOptionId);
							$adapter->update($qcPriceTable, $qcPriceData, $where);
						}
						
					}
				}
				
				$qaPriceTable = $resource->getTableName('printingmethod/qaprice');
				if (isset($data['qaPrice'])) {
					$qaPrice = $data['qaPrice'];
					foreach ($qaPrice['qty'] as $optionId => $values) {
						$intOptionId = (int) $optionId;					
						if (!empty($qaPrice['delete'][$optionId])) {
							if ($intOptionId) {						
								$adapter->delete($qaPriceTable, array('id = ?' => $intOptionId));
							}
							continue;
						}
						
						if (!$intOptionId) {
							$qaPriceData = array(
							   'id'  => $optionId,
							   'qty_range_id'  => $values,
							   'printing_method_id'    => $this->getRequest()->getParam('id'),
							   'area'    => $data['qaPrice']['area'][$optionId],
							   'first_side_price'    => $data['qaPrice']['first_side_price'][$optionId],
							   'second_side_price'    => $data['qaPrice']['second_side_price'][$optionId],
							   'third_side_price'    => $data['qaPrice']['third_side_price'][$optionId],
							   'fourth_side_price'    => $data['qaPrice']['fourth_side_price'][$optionId],
							);						
							$adapter->insert($qaPriceTable, $qaPriceData);
							$intOptionId = $adapter->lastInsertId($qaPriceTable);
						} else {
							$qaPriceData = array(
							   // 'id'  => $optionId,
							   'qty_range_id'  => $values,
							   'printing_method_id'    => $this->getRequest()->getParam('id'),
							   'area'    => $data['qaPrice']['area'][$optionId],
							   'first_side_price'    => $data['qaPrice']['first_side_price'][$optionId],
							   'second_side_price'    => $data['qaPrice']['second_side_price'][$optionId],
							   'third_side_price'    => $data['qaPrice']['third_side_price'][$optionId],
							   'fourth_side_price'    => $data['qaPrice']['fourth_side_price'][$optionId],
							);
							
							$where = array('id =?' => $intOptionId);
							$adapter->update($qaPriceTable, $qaPriceData, $where);
						}
						
					}
				}
				
				$fixedPriceTable = $resource->getTableName('printingmethod/fixedprice');
				if (isset($data['fixedPrice'])) {
					$fixedPrice = $data['fixedPrice'];
					foreach ($fixedPrice['qty'] as $optionId => $values) {
						$intOptionId = (int) $optionId;					
						if (!empty($fixedPrice['delete'][$optionId])) {
							if ($intOptionId) {						
								$adapter->delete($fixedPriceTable, array('id = ?' => $intOptionId));
							}
							continue;
						}
						
						if (!$intOptionId) {
							$fixedPriceData = array(
							   'id'  => $optionId,
							   'qty_range_id'  => $values,
							   'printing_method_id'    => $this->getRequest()->getParam('id'),							   
							   'first_side_price'    => $data['fixedPrice']['first_side_price'][$optionId],
							   'second_side_price'    => $data['fixedPrice']['second_side_price'][$optionId],
							   'third_side_price'    => $data['fixedPrice']['third_side_price'][$optionId],
							   'fourth_side_price'    => $data['fixedPrice']['fourth_side_price'][$optionId],
							);						
							$adapter->insert($fixedPriceTable, $fixedPriceData);
							$intOptionId = $adapter->lastInsertId($fixedPriceTable);
						} else {
							$fixedPriceData = array(
							   // 'id'  => $optionId,
							   'qty_range_id'  => $values,
							   'printing_method_id'    => $this->getRequest()->getParam('id'),							  
							   'first_side_price'    => $data['fixedPrice']['first_side_price'][$optionId],
							   'second_side_price'    => $data['fixedPrice']['second_side_price'][$optionId],
							   'third_side_price'    => $data['fixedPrice']['third_side_price'][$optionId],
							   'fourth_side_price'    => $data['fixedPrice']['fourth_side_price'][$optionId],
							);
							
							$where = array('id =?' => $intOptionId);
							$adapter->update($fixedPriceTable, $fixedPriceData, $where);
						}
						
					}
				}
				
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('printingmethod')->__('Printing Method was successfully saved'));
				Mage::getSingleton('adminhtml/session')->setFormData(false);
				if ($this->getRequest()->getParam('back')) {
					$this->_redirect('*/*/edit', array('id' => $printingmethod->getId(),'store' => $this->getRequest()->getParam('store')));
					return;
				}
				$this->_redirect('*/*/');
				return;
			} 
			catch (Mage_Core_Exception $e){
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				Mage::getSingleton('adminhtml/session')->setFormData($data);
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id'),'store' => $this->getRequest()->getParam('store')));
				return;
			}
			catch (Exception $e) {				
				Mage::logException($e);
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('printingmethod')->__('There was a problem saving the printing method.'));
				Mage::getSingleton('adminhtml/session')->setFormData($data);
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id'),'store' => $this->getRequest()->getParam('store')));
				return;
			}
		}
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('printingmethod')->__('Unable to find printing method to save.'));
		$this->_redirect('*/*/');
	}
	/**
	 * delete printing method - action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function deleteAction() {
		if( $this->getRequest()->getParam('id') > 0) {
			try {
				$printingmethod = Mage::getModel('printingmethod/printingmethod');
				
				$printingmethodtitleCollection = Mage::getModel('printingmethod/printingmethodtitle')->getCollection()
												->addFieldToFilter('printing_method_id',$this->getRequest()->getParam('id'));
				
				foreach($printingmethodtitleCollection as $printingmethodtitles){
					$printingmethodtitles->delete();
				}				
												
				$printableColorCollection = Mage::getModel('printingmethod/printablecolors')->getCollection()
										->addFieldToFilter('printing_method_id',$this->getRequest()->getParam('id'));
				
				foreach($printableColorCollection as $printableColors){
					$printableColorTitleCollection = Mage::getModel('printingmethod/printablecolorstitle')->getCollection()
													->addFieldToFilter('color_id',$printableColors->getId());
					foreach($printableColorTitleCollection as $printableColorTitles){
						$printableColorTitles->delete();
					}
					$printableColors->delete();
				}		
				
				$printingmethod->setId($this->getRequest()->getParam('id'))->delete();				
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('printingmethod')->__('Printing Method was successfully deleted.'));
				$this->_redirect('*/*/');
				return; 
			}
			catch (Mage_Core_Exception $e){
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
			}
			catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('printingmethod')->__('There was an error deleteing printing method.'));
				$this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
				Mage::logException($e);
				return;
			}
		}
		Mage::getSingleton('adminhtml/session')->addError(Mage::helper('printingmethod')->__('Could not find printing method to delete.'));
		$this->_redirect('*/*/');
	}
	/**
	 * mass delete printing method - action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function massDeleteAction() {
		$printingmethodIds = $this->getRequest()->getParam('printingmethod');
		if(!is_array($printingmethodIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('printingmethod')->__('Please select printing methods to delete.'));
		}
		else {
			try {
				foreach ($printingmethodIds as $printingmethodId) {
					$printingmethod = Mage::getModel('printingmethod/printingmethod');
					$printingmethod->setId($printingmethodId)->delete();
				}
				Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('printingmethod')->__('Total of %d printing methods were successfully deleted.', count($printingmethodIds)));
			}
			catch (Mage_Core_Exception $e){
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
			catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('printingmethod')->__('There was an error deleteing printing methods.'));
				Mage::logException($e);
			}
		}
		$this->_redirect('*/*/index');
	}
	/**
	 * mass status change - action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function massStatusAction(){
		$printingmethodIds = $this->getRequest()->getParam('printingmethod');
		if(!is_array($printingmethodIds)) {
			Mage::getSingleton('adminhtml/session')->addError(Mage::helper('printingmethod')->__('Please select printing methods.'));
		} 
		else {
			try {
				foreach ($printingmethodIds as $printingmethodId) {
				$printingmethod = Mage::getSingleton('printingmethod/printingmethod')->load($printingmethodId)
							->setStatus($this->getRequest()->getParam('status'))
							->setIsMassupdate(true)
							->save();
				}
				$this->_getSession()->addSuccess($this->__('Total of %d printing methods were successfully updated.', count($printingmethodIds)));
			}
			catch (Mage_Core_Exception $e){
				Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
			}
			catch (Exception $e) {
				Mage::getSingleton('adminhtml/session')->addError(Mage::helper('printingmethod')->__('There was an error updating printing methods.'));
				Mage::logException($e);
			}
		}
		$this->_redirect('*/*/index');
	}
	/**
	 * get grid of products action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function productsAction(){
		$this->_initPrintingmethod();
		$this->loadLayout();
		$this->getLayout()->getBlock('printingmethod.edit.tab.product')
			->setPrintingmethodProducts($this->getRequest()->getPost('printingmethod_products', null));
		$this->renderLayout();
	}
	/**
	 * get grid of products action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function productsgridAction(){
		$this->_initPrintingmethod();
		$this->loadLayout();
		$this->getLayout()->getBlock('printingmethod.edit.tab.product')
			->setPrintingmethodProducts($this->getRequest()->getPost('printingmethod_products', null));
		$this->renderLayout();
	}
	/**
	 * export as csv - action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function exportCsvAction(){
		$fileName   = 'printingmethod.csv';
		$content	= $this->getLayout()->createBlock('printingmethod/adminhtml_printingmethod_grid')->getCsv();
		$this->_prepareDownloadResponse($fileName, $content);
	}
	/**
	 * export as MsExcel - action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function exportExcelAction(){
		$fileName   = 'printingmethod.xls';
		$content	= $this->getLayout()->createBlock('printingmethod/adminhtml_printingmethod_grid')->getExcelFile();
		$this->_prepareDownloadResponse($fileName, $content);
	}
	/**
	 * export as xml - action
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function exportXmlAction(){
		$fileName   = 'printingmethod.xml';
		$content	= $this->getLayout()->createBlock('printingmethod/adminhtml_printingmethod_grid')->getXml();
		$this->_prepareDownloadResponse($fileName, $content);
	}
	
	/**
     * Validate product
     *
     */
    public function validateAction()
    {
        $response = new Varien_Object();
        $response->setError(false);
		
		if ($data = $this->getRequest()->getPost()) {
			try {	
				$error = array();
				if (isset($data['qcPrice'])) {
					$qcPrice = $data['qcPrice'];
					foreach ($qcPrice['qty'] as $optionId => $values) {
						if($qcPrice['delete'][$optionId] != 1){
							$qcPriceData[$optionId] = array(							   
								   'qty_range_id'  => $values,							   
								   'noofcolor'    => $data['qcPrice']['noofcolor'][$optionId],							   
							);
						}
					}
					
					$duplciateQcPriceData = $this->findDuplicateData( $qcPriceData );
					
					foreach($duplciateQcPriceData as $key => $duplciateQcPrice){
						$error['error'] = true;
						$error['attribute'] = $key;
						$error['message'] = "Combination for this Quantity Count and Number of Colors already exist.";
					}					
				}
				if (isset($data['qaPrice'])) {
					$qaPrice = $data['qaPrice'];
					foreach ($qaPrice['qty'] as $optionId => $values) {
						if($qaPrice['delete'][$optionId] != 1){
							$qaPriceData[$optionId] = array(
							   'qty_range_id'  => $values,							   
							   'area'    => $data['qaPrice']['area'][$optionId],
							);
						}
					}
					
					$duplciateQaPriceData = $this->findDuplicateData( $qaPriceData );
					
					foreach($duplciateQaPriceData as $key => $duplciateQaPrice){
						$error['error'] = true;
						$error['attribute'] = $key;
						$error['message'] = "Combination for this Quantity Count and Area already exist.";
					}	
				}
				
				// if (!empty($qcPrice['delete'][$optionId])) {
				if (isset($data['fixedPrice'])) {
					$fixedPrice = $data['fixedPrice'];				
					foreach ($fixedPrice['qty'] as $optionId => $values) {
						if($fixedPrice['delete'][$optionId] != 1){
							$fixedPriceData[$optionId] = array(
							   'qty_range_id'  => $values,
							);	
						}
					}
					
					$duplciatefixedPriceData = $this->findDuplicateData( $fixedPriceData );
					
					foreach($duplciatefixedPriceData as $key => $duplciatefixedPrice){
						$error['error'] = true;
						$error['attribute'] = $key;
						$error['message'] = "Combination for this Quantity Count already exist.";
					}	
				}
				
			}
			catch (Exception $e) {
				$this->_getSession()->addError($e->getMessage());
				$this->_initLayoutMessages('adminhtml/session');
				$response->setError(true);
				$response->setMessage($this->getLayout()->getMessagesBlock()->getGroupedHtml());
			}
		}
		// $this->getResponse()->setBody('{"error":true,"attribute":"fixedRow__2","message":"The value of attribute \"SKU\" must be unique"}');
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($error));
    }
	

	public function findDuplicateData($qcPriceData)
	{
		
		$results = array();
		$duplicates = array();
		$reverseQcPriceData = array_reverse($qcPriceData,true);		
		foreach ($reverseQcPriceData as $key => $item) {
			if (in_array($item, $results)) {
				$duplicates[$key] = $item;
			}

			$results[] = $item;
		}				
		return $duplicates;
	}


	public function printablecolorsAction()
    {
			$id = $this->getRequest()->getParam('id');
            // $productId  = (int) $this->getRequest()->getParam('pid');
            // $w2phtml5background = Mage::getModel('w2phtml5background/w2phtml5background');	
            // Mage::register('w2phtml5backgroundimage_data', $w2phtml5background);
            $this->getResponse()->setBody($this->getLayout()->createBlock('printingmethod/adminhtml_printingmethod_edit_tab_printablecolors')->toHtml());
    }
}