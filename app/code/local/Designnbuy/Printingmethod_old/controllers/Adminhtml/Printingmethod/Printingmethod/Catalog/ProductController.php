<?php
/**
 * Designnbuy_Printingmethod extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @copyright  	Copyright (c) 2014
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Printingmethod product admin controller
 *
 * @category	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @author Ultimate Module Creator
 */
require_once ("Mage/Adminhtml/controllers/Catalog/ProductController.php");
class Designnbuy_Printingmethod_Adminhtml_Printingmethod_Printingmethod_Catalog_ProductController extends Mage_Adminhtml_Catalog_ProductController{
	/**
	 * construct
	 * @access protected
	 * @return void
	 * @author Ultimate Module Creator
	 */
	protected function _construct(){
		// Define module dependent translate
		$this->setUsedModuleName('Designnbuy_Printingmethod');
	}
	/**
	 * printingmethods in the catalog page
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function printingmethodsAction(){
		$this->_initProduct();
		$this->loadLayout();
		$this->getLayout()->getBlock('product.edit.tab.printingmethod')
			->setProductPrintingmethods($this->getRequest()->getPost('product_printingmethods', null));
		$this->renderLayout();
	}
	/**
	 * printingmethods grid in the catalog page
	 * @access public
	 * @return void
	 * @author Ultimate Module Creator
	 */
	public function printingmethodsGridAction(){
		$this->_initProduct();
		$this->loadLayout();
		$this->getLayout()->getBlock('product.edit.tab.printingmethod')
			->setProductPrintingmethods($this->getRequest()->getPost('product_printingmethods', null));
		$this->renderLayout();
	}
}