<?php
/**
 * Designnbuy_Printingmethod extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @copyright  	Copyright (c) 2014
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Product helper
 *
 * @category	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @author Ultimate Module Creator
 */
class Designnbuy_Printingmethod_Helper_Product extends Designnbuy_Printingmethod_Helper_Data{
	/**
	 * get the selected printingmethods for a product
	 * @access public
	 * @param Mage_Catalog_Model_Product $product
	 * @return array()
	 * @author Ultimate Module Creator
	 */
	public function getSelectedPrintingmethods(Mage_Catalog_Model_Product $product){
		if (!$product->hasSelectedPrintingmethods()) {
			$printingmethods = array();
			foreach ($this->getSelectedPrintingmethodsCollection($product) as $printingmethod) {
				$printingmethods[] = $printingmethod;
			}
			$product->setSelectedPrintingmethods($printingmethods);
		}
		return $product->getData('selected_printingmethods');
	}
	/**
	 * get printingmethod collection for a product
	 * @access public
	 * @param Mage_Catalog_Model_Product $product
	 * @return Designnbuy_Printingmethod_Model_Resource_Printingmethod_Collection
	 */
	public function getSelectedPrintingmethodsCollection(Mage_Catalog_Model_Product $product){
		$collection = Mage::getResourceSingleton('printingmethod/printingmethod_collection')
			->addProductFilter($product);
		return $collection;
	}
}