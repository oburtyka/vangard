<?php 
/**
 * Designnbuy_Printingmethod extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @copyright  	Copyright (c) 2014
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Printingmethod default helper
 *
 * @category	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @author Ultimate Module Creator
 */
class Designnbuy_Printingmethod_Helper_Data extends Mage_Core_Helper_Abstract{
	/**
	 * get the url to the printing methods list page
	 * @access public
	 * @return string
	 * @author Ultimate Module Creator
	 */
	public function getPrintingmethodsUrl(){
		return Mage::getUrl('printingmethod/printingmethod/index');
	}
	
	public function getPricingLogics(){
		return array(
					'value'=>'1', 'label'=>Mage::helper('printingmethod')->__('Fixed'),
					'value'=>'2', 'label'=>Mage::helper('printingmethod')->__('No. Of Colors'),
					'value'=>'3', 'label'=>Mage::helper('printingmethod')->__('Artwork Size')
			);
           
	}
	
	public function getPrintableColors(){
		return array(
            array('value'=>'1', 'label'=>Mage::helper('printingmethod')->__('Full')),
            array('value'=>'2', 'label'=>Mage::helper('printingmethod')->__('Custom')),		
        );
	}
	
	public function formatFee($amount){
		return Mage::helper('printingmethod')->__('Artwork Setup Fee');
	}
}