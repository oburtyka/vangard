<?php
$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$installer->startSetup();
$installer->run("CREATE TABLE IF NOT EXISTS {$this->getTable('printingmethod_printingmethod_printablecolors')} (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `printing_method_id` smallint(6) NOT NULL,
  `color_name` varchar(255) NOT NULL,
  `color_code` varchar(255) NOT NULL,
  `sort_order` smallint(6) NULL,
  PRIMARY KEY (`id`),
  KEY `id` (`id`) 
) ENGINE=InnoDB DEFAULT CHARSET=utf8");

$installer->run("CREATE TABLE IF NOT EXISTS {$this->getTable('printingmethod_printingmethod_title')} (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`printing_method_id` int(11) NULL,
`attribute_id` int(11) DEFAULT 0,
`store_id` int(11) DEFAULT 0,
`title` varchar(255) NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ; 
");

$installer->run("CREATE TABLE IF NOT EXISTS {$this->getTable('printingmethod_printingmethod_printablecolors_title')} (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`color_id` int(11) NULL,
`store_id` int(11) DEFAULT 0,
`title` varchar(255) NULL,
PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ; 
");

$installer->run("CREATE TABLE IF NOT EXISTS {$this->getTable('printingmethod_printingmethod_qcprice')} (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`printing_method_id` int(11) NULL,
`qty_range_id` int(11) NULL,
`noofcolor` int(11) NULL,
`first_side_price` DECIMAL(10,2) DEFAULT 0.00,
`second_side_price` DECIMAL(10,2) DEFAULT 0.00,
`third_side_price` DECIMAL(10,2) DEFAULT 0.00,
`fourth_side_price` DECIMAL(10,2) DEFAULT 0.00,
PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ; 
");

$installer->run("CREATE TABLE IF NOT EXISTS {$this->getTable('printingmethod_printingmethod_qcprice')} (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`printing_method_id` int(11) NULL,
`qty_range_id` int(11) NULL,
`noofcolor` int(11) NULL,
`first_side_price` DECIMAL(10,2) DEFAULT 0.00,
`second_side_price` DECIMAL(10,2) DEFAULT 0.00,
`third_side_price` DECIMAL(10,2) DEFAULT 0.00,
`fourth_side_price` DECIMAL(10,2) DEFAULT 0.00,
PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ; 
");

$installer->run("CREATE TABLE IF NOT EXISTS {$this->getTable('printingmethod_printingmethod_qaprice')} (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`printing_method_id` int(11) NULL,
`qty_range_id` int(11) NULL,
`area` int(11) NULL,
`first_side_price` DECIMAL(10,2) DEFAULT 0.00,
`second_side_price` DECIMAL(10,2) DEFAULT 0.00,
`third_side_price` DECIMAL(10,2) DEFAULT 0.00,
`fourth_side_price` DECIMAL(10,2) DEFAULT 0.00,
PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ; 
");

$installer->run("CREATE TABLE IF NOT EXISTS {$this->getTable('printingmethod_printingmethod_fixedprice')} (
`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
`printing_method_id` int(11) NULL,
`qty_range_id` int(11) NULL,
`first_side_price` DECIMAL(10,2) DEFAULT 0.00,
`second_side_price` DECIMAL(10,2) DEFAULT 0.00,
`third_side_price` DECIMAL(10,2) DEFAULT 0.00,
`fourth_side_price` DECIMAL(10,2) DEFAULT 0.00,
PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ; 
");
$installer->run("		
ALTER TABLE  `".$this->getTable('sales/order')."` ADD  `setupfee_amount` DECIMAL( 10, 2 ) NOT NULL;
ALTER TABLE  `".$this->getTable('sales/order')."` ADD  `base_setupfee_amount` DECIMAL( 10, 2 ) NOT NULL;
");
$installer->run("
ALTER TABLE  `".$this->getTable('sales/quote_address')."` ADD  `setupfee_amount` DECIMAL( 10, 2 ) NOT NULL;
ALTER TABLE  `".$this->getTable('sales/quote_address')."` ADD  `base_setupfee_amount` DECIMAL( 10, 2 ) NOT NULL;
");

$installer->run("

	ALTER TABLE  `".$this->getTable('sales/invoice')."` ADD  `setupfee_amount` DECIMAL( 10, 2 ) NOT NULL;
	ALTER TABLE  `".$this->getTable('sales/invoice')."` ADD  `base_setupfee_amount` DECIMAL( 10, 2 ) NOT NULL;

");
$installer->run("

	ALTER TABLE  `".$this->getTable('sales/order')."` ADD  `setupfee_amount_invoiced` DECIMAL( 10, 2 ) NOT NULL;
	ALTER TABLE  `".$this->getTable('sales/order')."` ADD  `base_setupfee_amount_invoiced` DECIMAL( 10, 2 ) NOT NULL;

");

$installer->run("

	ALTER TABLE  `".$this->getTable('sales/order')."` ADD  `setupfee_amount_refunded` DECIMAL( 10, 2 ) NOT NULL;
	ALTER TABLE  `".$this->getTable('sales/order')."` ADD  `base_setupfee_amount_refunded` DECIMAL( 10, 2 ) NOT NULL;

	ALTER TABLE  `".$this->getTable('sales/creditmemo')."` ADD  `setupfee_amount` DECIMAL( 10, 2 ) NOT NULL;
	ALTER TABLE  `".$this->getTable('sales/creditmemo')."` ADD  `base_setupfee_amount` DECIMAL( 10, 2 ) NOT NULL;

");		
$this->endSetup();