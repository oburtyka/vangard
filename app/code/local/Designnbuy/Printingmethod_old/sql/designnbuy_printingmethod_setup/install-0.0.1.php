<?php 
/**
 * Designnbuy_Printingmethod extension
 * 
 * NOTICE OF LICENSE
 * 
 * This source file is subject to the MIT License
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/mit-license.php
 * 
 * @category   	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @copyright  	Copyright (c) 2014
 * @license		http://opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Printingmethod module install script
 *
 * @category	Designnbuy
 * @package		Designnbuy_Printingmethod
 * @author Ultimate Module Creator
 */
$installer = $this;
$this->startSetup();

$table = $this->getConnection()
	->newTable($this->getTable('printingmethod/printingmethod'))
	->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'identity'  => true,
		'nullable'  => false,
		'primary'   => true,
		), 'Printing Method ID')
	->addColumn('printing_method', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
		'nullable'  => false,
		), 'Printing Method Name')

	->addColumn('pricing_logic', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'nullable'  => false,
		'unsigned'  => true,
		), 'Pricing Logic')

	->addColumn('printable_colors', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'nullable'  => false,
		'unsigned'  => true,
		), 'Printable Colors')
	
	->addColumn('min_qty', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'nullable'  => false,
		'unsigned'  => true,
		), 'Minimum Quantity')
		
	->addColumn('max_qty', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'nullable'  => false,
		'unsigned'  => true,
		), 'Maximum Quantity')
	
	->addColumn('is_name_number', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'nullable'  => false,
		'unsigned'  => true,
		), 'Enable Name/Number')
	
	->addColumn('name_price', Varien_Db_Ddl_Table::TYPE_DECIMAL, '10,2', array(
		'nullable'  => false,
		), 'Charge for Name')
		
	->addColumn('number_price', Varien_Db_Ddl_Table::TYPE_DECIMAL, '10,2', array(
		'nullable'  => false,
		), 'Charge for Number')
		
	->addColumn('is_image_upload', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'nullable'  => false,
		'unsigned'  => true,
		), 'Enable Image Upload')
	
	->addColumn('image_price', Varien_Db_Ddl_Table::TYPE_DECIMAL, '10,2', array(
		'nullable'  => false,
		), 'Charge for Image Upload')
		
	->addColumn('artwork_setup_price_type', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'nullable'  => false,
		'unsigned'  => true,
		), 'Artwork setup fee')
		
	->addColumn('artwork_setup_price', Varien_Db_Ddl_Table::TYPE_DECIMAL, '10,2', array(
		'nullable'  => false,
		), 'Artwork setup fee')	

	->addColumn('is_alert', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'nullable'  => false,
		'unsigned'  => true,
		), 'Enable Alert')

	->addColumn('alert_message', Varien_Db_Ddl_Table::TYPE_TEXT, '64k', array(
		), 'Alert Message')

	->addColumn('url_key', Varien_Db_Ddl_Table::TYPE_TEXT, 255, array(
		), 'URL key')

	->addColumn('status', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		), 'Status')

	->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
		), 'Printing Method Creation Time')
	->addColumn('updated_at', Varien_Db_Ddl_Table::TYPE_TIMESTAMP, null, array(
		), 'Printing Method Modification Time')
	->setComment('Printing Method Table');
$this->getConnection()->createTable($table);

$table = $this->getConnection()
	->newTable($this->getTable('printingmethod/printingmethod_store'))
	->addColumn('printingmethod_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
		'nullable'  => false,
		'primary'   => true,
		), 'Printing Method ID')
	->addColumn('store_id', Varien_Db_Ddl_Table::TYPE_SMALLINT, null, array(
		'unsigned'  => true,
		'nullable'  => false,
		'primary'   => true,
		), 'Store ID')
	->addIndex($this->getIdxName('printingmethod/printingmethod_store', array('store_id')), array('store_id'))
	->addForeignKey($this->getFkName('printingmethod/printingmethod_store', 'printingmethod_id', 'printingmethod/printingmethod', 'entity_id'), 'printingmethod_id', $this->getTable('printingmethod/printingmethod'), 'entity_id', Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
	->addForeignKey($this->getFkName('printingmethod/printingmethod_store', 'store_id', 'core/store', 'store_id'), 'store_id', $this->getTable('core/store'), 'store_id', Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
	->setComment('Printing Methods To Store Linkage Table');
$this->getConnection()->createTable($table);
$table = $this->getConnection()
	->newTable($this->getTable('printingmethod/printingmethod_product'))
	->addColumn('rel_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'unsigned'  => true,
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
        ), 'Category ID')
	->addColumn('printingmethod_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'unsigned'  => true,
		'nullable'  => false,
		'default'   => '0',
	), 'Printing Method ID')
	->addColumn('product_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'unsigned'  => true,
		'nullable'  => false,
		'default'   => '0',
	), 'Product ID')
	->addColumn('position', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
		'nullable'  => false,
		'default'   => '0',
	), 'Position')
	->addIndex($this->getIdxName('printingmethod/printingmethod_product', array('product_id')), array('product_id'))
	->addForeignKey($this->getFkName('printingmethod/printingmethod_product', 'printingmethod_id', 'printingmethod/printingmethod', 'entity_id'), 'printingmethod_id', $this->getTable('printingmethod/printingmethod'), 'entity_id', Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
	->addForeignKey($this->getFkName('printingmethod/printingmethod_product', 'product_id', 'catalog/product', 'entity_id'),	'product_id', $this->getTable('catalog/product'), 'entity_id', Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
	->setComment('Printing Method to Product Linkage Table');
$this->getConnection()->createTable($table);
$this->endSetup();