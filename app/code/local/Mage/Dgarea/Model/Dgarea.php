<?php

class Mage_Dgarea_Model_Dgarea extends Mage_Core_Model_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('dgarea/dgarea');
    }
	
	public function checkDgArea($id,$data)
	{		
		if($data['square_area']){			
			
			$collection = Mage::getModel('dgarea/dgarea')->getCollection()
						->addFieldToFilter('dgarea_id', array('neq' => array($id)))
						->addFieldToFilter('square_area',array(																
																		array('eq'=>array($data['square_area']))
																));			
			if ($collection->count() > 0) {
				throw new Exception('Square area '.$data['square_area'].' already exists.');
			}
			return true;
		}		
		
	}
	
	
}