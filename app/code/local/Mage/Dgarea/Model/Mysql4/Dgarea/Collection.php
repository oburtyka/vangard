<?php

class Mage_Dgarea_Model_Mysql4_Dgarea_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('dgarea/dgarea');
    }
}