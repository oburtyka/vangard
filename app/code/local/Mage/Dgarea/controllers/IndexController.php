<?php
class Mage_Dgarea_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
    	
    	/*
    	 * Load an object by id 
    	 * Request looking like:
    	 * http://site.com/dgarea?id=15 
    	 *  or
    	 * http://site.com/dgarea/id/15 	
    	 */
    	/* 
		$dgarea_id = $this->getRequest()->getParam('id');

  		if($dgarea_id != null && $dgarea_id != '')	{
			$dgarea = Mage::getModel('dgarea/dgarea')->load($dgarea_id)->getData();
		} else {
			$dgarea = null;
		}	
		*/
		
		 /*
    	 * If no param we load a the last created item
    	 */ 
    	/*
    	if($dgarea == null) {
			$resource = Mage::getSingleton('core/resource');
			$read= $resource->getConnection('core_read');
			$dgareaTable = $resource->getTableName('dgarea');
			
			$select = $read->select()
			   ->from($dgareaTable,array('dgarea_id','title','content','status'))
			   ->where('status',1)
			   ->order('created_time DESC') ;
			   
			$dgarea = $read->fetchRow($select);
		}
		Mage::register('dgarea', $dgarea);
		*/

			
		$this->loadLayout();     
		$this->renderLayout();
    }
}