<?php
class Mage_Dgarea_Block_Dgarea extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getDgarea()     
     { 
        if (!$this->hasData('dgarea')) {
            $this->setData('dgarea', Mage::registry('dgarea'));
        }
        return $this->getData('dgarea');
        
    }
}