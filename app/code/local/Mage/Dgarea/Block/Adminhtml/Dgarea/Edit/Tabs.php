<?php

class Mage_Dgarea_Block_Adminhtml_Dgarea_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('dgarea_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('dgarea')->__('Square Area Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('dgarea')->__('Square Area Information'),
          'title'     => Mage::helper('dgarea')->__('Square Area Information'),
          'content'   => $this->getLayout()->createBlock('dgarea/adminhtml_dgarea_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}