<?php

class Mage_Dgarea_Block_Adminhtml_Dgarea_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'dgarea';
        $this->_controller = 'adminhtml_dgarea';
        
        $this->_updateButton('save', 'label', Mage::helper('dgarea')->__('Save Square Area'));
        $this->_updateButton('delete', 'label', Mage::helper('dgarea')->__('Delete Square Area'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('dgarea_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'dgarea_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'dgarea_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('dgarea_data') && Mage::registry('dgarea_data')->getId() ) {
            return Mage::helper('dgarea')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('dgarea_data')->getTitle()));
        } else {
            return Mage::helper('dgarea')->__('Add Square Area');
        }
    }
}