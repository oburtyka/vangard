<?php

class Mage_Dgarea_Block_Adminhtml_Dgarea_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('dgarea_form', array('legend'=>Mage::helper('dgarea')->__('Square Area information')));
		
	  $fieldset->addField('square_area', 'text', array(
		  'label'     => Mage::helper('dgarea')->__('Square Area'),
		  'class'     => 'required-entry',
		  'required'  => true,
		  'name'      => 'square_area',
	  ));
      /*$fieldset->addField('dgarea_counterx', 'text', array(
          'label'     => Mage::helper('dgarea')->__('Area Counter (X)'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'dgarea_counterx',
      ));
	   
	  $fieldset->addField('dgarea_countery', 'text', array(
          'label'     => Mage::helper('dgarea')->__('Area Counter (Y)'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'dgarea_countery',
      )); */
     
      if ( Mage::getSingleton('adminhtml/session')->getDgareaData() )
      {
		  
          $form->setValues(Mage::getSingleton('adminhtml/session')->getDgareaData());
          Mage::getSingleton('adminhtml/session')->setDgareaData(null);
      } elseif ( Mage::registry('dgarea_data') ) {
			
          $form->setValues(Mage::registry('dgarea_data')->getData());
      }
      return parent::_prepareForm();
  }
}