<?php

class Mage_Dgarea_Block_Adminhtml_Dgarea_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('dgareaGrid');
      $this->setDefaultSort('dgarea_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('dgarea/dgarea')->getCollection();
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('dgarea_id', array(
          'header'    => Mage::helper('dgarea')->__('ID'),
          'align'     =>'right',
          'width'     => '50px',
          'index'     => 'dgarea_id',
      ));
		
	$this->addColumn('square_area', array(
          'header'    => Mage::helper('dgarea')->__('Square Area'),
          'align'     =>'left',
          'index'     => 'square_area',
		  'filter'     => false,
      ));
	  
      /*$this->addColumn('dgarea_counterx', array(
          'header'    => Mage::helper('dgarea')->__('Area Counter (X)'),
          'align'     =>'left',
          'index'     => 'dgarea_counterx',
		  'filter'     => false,
      ));

	    $this->addColumn('dgarea_countery', array(
          'header'    => Mage::helper('dgarea')->__('Area Counter (Y)'),
          'align'     =>'left',
          'index'     => 'dgarea_countery',
		  'filter'     => false,
      ));
	  
	   $this->addColumn('dgarea_counterxy', array(
          'header'    => Mage::helper('dgarea')->__('Total Area Counter'),
          'align'     =>'left',
          'index'     => 'dgarea_counterxy',
		  'filter'     => false,
      ));  */
	  
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('dgarea')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('dgarea')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('dgarea')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('dgarea')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('dgarea_id');
        $this->getMassactionBlock()->setFormFieldName('dgarea');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('dgarea')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('dgarea')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('dgarea/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('dgarea')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('dgarea')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}