<?php
class Mage_Dgarea_Block_Adminhtml_Dgarea extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_dgarea';
    $this->_blockGroup = 'dgarea';
    $this->_headerText = Mage::helper('dgarea')->__('Square Area Manager');
    $this->_addButtonLabel = Mage::helper('dgarea')->__('Add Square Area');
    parent::__construct();
  }
}