<?php

$installer = $this;

$installer->startSetup();

	
	
$installer->run("CREATE TABLE IF NOT EXISTS `{$this->getTable('dgarea')}` (
  `dgarea_id` int(11) unsigned NOT NULL auto_increment,
  `square_area` int(11) NOT NULL default '0',
  PRIMARY KEY (`dgarea_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;");
/*	
$installer->run("CREATE TABLE IF NOT EXISTS `{$this->getTable('dgarea')}` (
  `dgarea_id` int(11) unsigned NOT NULL auto_increment,
  `dgarea_counterx` int(11) NOT NULL default '0',
  `dgarea_countery` int(11) NOT NULL default '0',
    `dgarea_counterxy` int(11) NOT NULL default '0',
  PRIMARY KEY (`dgarea_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;");
*/
$installer->endSetup(); 