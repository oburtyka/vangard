<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Sales
 * @copyright   Copyright (c) 2012 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Quote item abstract model
 *
 * Price attributes:
 *  - price - initial item price, declared during product association
 *  - original_price - product price before any calculations
 *  - calculation_price - prices for item totals calculation
 *  - custom_price - new price that can be declared by user and recalculated during calculation process
 *  - original_custom_price - original defined value of custom price without any convertion
 *
 * @category   Mage
 * @package    Mage_Sales
 * @author      Magento Core Team <core@magentocommerce.com>
 */
abstract class Mage_Sales_Model_Quote_Item_Abstract extends Mage_Core_Model_Abstract
    implements Mage_Catalog_Model_Product_Configuration_Item_Interface
{
    protected $_parentItem  = null;
    protected $_children    = array();
    protected $_messages    = array();

    /**
     * Retrieve Quote instance
     *
     * @return Mage_Sales_Model_Quote
     */
    abstract function getQuote();

    /**
     * Retrieve product model object associated with item
     *
     * @return Mage_Catalog_Model_Product
     */
    public function getProduct()
    {
        $product = $this->_getData('product');
        if (($product === null) && $this->getProductId()) {
            $product = Mage::getModel('catalog/product')
                ->setStoreId($this->getQuote()->getStoreId())
                ->load($this->getProductId());
            $this->setProduct($product);
        }

        /**
         * Reset product final price because it related to custom options
         */
        $product->setFinalPrice(null);
        if (is_array($this->_optionsByCode)) {
            $product->setCustomOptions($this->_optionsByCode);
        }
        return $product;
    }

    /**
     * Returns special download params (if needed) for custom option with type = 'file'
     * Needed to implement Mage_Catalog_Model_Product_Configuration_Item_Interface.
     * Return null, as quote item needs no additional configuration.
     *
     * @return null|Varien_Object
     */
    public function getFileDownloadParams()
    {
        return null;
    }

    /**
     * Specify parent item id before saving data
     *
     * @return  Mage_Sales_Model_Quote_Item_Abstract
     */
    protected function _beforeSave()
    {
        parent::_beforeSave();
        if ($this->getParentItem()) {
            $this->setParentItemId($this->getParentItem()->getId());
        }
        return $this;
    }


    /**
     * Set parent item
     *
     * @param  Mage_Sales_Model_Quote_Item $parentItem
     * @return Mage_Sales_Model_Quote_Item
     */
    public function setParentItem($parentItem)
    {
        if ($parentItem) {
            $this->_parentItem = $parentItem;
            $parentItem->addChild($this);
        }
        return $this;
    }

    /**
     * Get parent item
     *
     * @return Mage_Sales_Model_Quote_Item
     */
    public function getParentItem()
    {
        return $this->_parentItem;
    }

    /**
     * Get chil items
     *
     * @return array
     */
    public function getChildren()
    {
        return $this->_children;
    }

    /**
     * Add child item
     *
     * @param  Mage_Sales_Model_Quote_Item_Abstract $child
     * @return Mage_Sales_Model_Quote_Item_Abstract
     */
    public function addChild($child)
    {
        $this->setHasChildren(true);
        $this->_children[] = $child;
        return $this;
    }

    /**
     * Adds message(s) for quote item. Duplicated messages are not added.
     *
     * @param  mixed $messages
     * @return Mage_Sales_Model_Quote_Item_Abstract
     */
    public function setMessage($messages)
    {
        $messagesExists = $this->getMessage(false);
        if (!is_array($messages)) {
            $messages = array($messages);
        }
        foreach ($messages as $message) {
            if (!in_array($message, $messagesExists)) {
                $this->addMessage($message);
            }
        }
        return $this;
    }

    /**
     * Add message of quote item to array of messages
     *
     * @param   string $message
     * @return  Mage_Sales_Model_Quote_Item_Abstract
     */
    public function addMessage($message)
    {
        $this->_messages[] = $message;
        return $this;
    }

    /**
     * Get messages array of quote item
     *
     * @param   bool $string flag for converting messages to string
     * @return  array|string
     */
    public function getMessage($string = true)
    {
        if ($string) {
            return join("\n", $this->_messages);
        }
        return $this->_messages;
    }

    /**
     * Removes message by text
     *
     * @param string $text
     * @return Mage_Sales_Model_Quote_Item_Abstract
     */
    public function removeMessageByText($text)
    {
        foreach ($this->_messages as $key => $message) {
            if ($message == $text) {
                unset($this->_messages[$key]);
            }
        }
        return $this;
    }

    /**
     * Clears all messages
     *
     * @return Mage_Sales_Model_Quote_Item_Abstract
     */
    public function clearMessage()
    {
        $this->unsMessage(); // For older compatibility, when we kept message inside data array
        $this->_messages = array();
        return $this;
    }

    /**
     * Retrieve store model object
     *
     * @return Mage_Core_Model_Store
     */
    public function getStore()
    {
        return $this->getQuote()->getStore();
    }

    /**
     * Checking item data
     *
     * @return Mage_Sales_Model_Quote_Item_Abstract
     */
    public function checkData()
    {
        $this->setHasError(false);
        $this->clearMessage();

        $qty = $this->_getData('qty');

        try {
            $this->setQty($qty);
        } catch (Mage_Core_Exception $e){
            $this->setHasError(true);
            $this->setMessage($e->getMessage());
        } catch (Exception $e){
            $this->setHasError(true);
            $this->setMessage(Mage::helper('sales')->__('Item qty declaration error.'));
        }

        try {
            $this->getProduct()->getTypeInstance(true)->checkProductBuyState($this->getProduct());
        } catch (Mage_Core_Exception $e) {
            $this->setHasError(true);
            $this->setMessage($e->getMessage());
            $this->getQuote()->setHasError(true);
            $this->getQuote()->addMessage(
                Mage::helper('sales')->__('Some of the products below do not have all the required options. Please edit them and configure all the required options.')
            );
        } catch (Exception $e) {
            $this->setHasError(true);
            $this->setMessage(Mage::helper('sales')->__('Item options declaration error.'));
            $this->getQuote()->setHasError(true);
            $this->getQuote()->addMessage(Mage::helper('sales')->__('Items options declaration error.'));
        }

        return $this;
    }

    /**
     * Get original (not related with parent item) item quantity
     *
     * @return  int|float
     */
    public function getQty()
    {
        return $this->_getData('qty');
    }

    /**
     * Get total item quantity (include parent item relation)
     *
     * @return  int|float
     */
    public function getTotalQty()
    {
        if ($this->getParentItem()) {
            return $this->getQty()*$this->getParentItem()->getQty();
        }
        return $this->getQty();
    }

    /**
     * Calculate item row total price
     *
     * @return Mage_Sales_Model_Quote_Item
     */
    public function calcRowTotal()
    {
        $qty        = $this->getTotalQty();
        // Round unit price before multiplying to prevent losing 1 cent on subtotal
        $total      = $this->getStore()->roundPrice($this->getCalculationPriceOriginal()) * $qty;
        $baseTotal  = $this->getBaseCalculationPriceOriginal() * $qty;

        $this->setRowTotal($this->getStore()->roundPrice($total));
        $this->setBaseRowTotal($this->getStore()->roundPrice($baseTotal));
        return $this;
    }

    /**
     * Get item price used for quote calculation process.
     * This method get custom price (if it is defined) or original product final price
     *
     * @return float
     */
    public function getCalculationPrice()
    {
        $price = $this->_getData('calculation_price');
        if (is_null($price)) {
            if ($this->hasCustomPrice()) {
                $price = $this->getCustomPrice();
            } else {
                $price = $this->getConvertedPrice();
            }
            $this->setData('calculation_price', $price);
        }
        return $price;
    }

    /**
     * Get item price used for quote calculation process.
     * This method get original custom price applied before tax calculation
     *
     * @return float
     */
    public function getCalculationPriceOriginal()
    {
        $price = $this->_getData('calculation_price');
        if (is_null($price)) {
            if ($this->hasOriginalCustomPrice()) {
                $price = $this->getOriginalCustomPrice();
            } else {
                $price = $this->getConvertedPrice();
            }
			/*get addon price added by Ajay*/
			$item = Mage::getModel('sales/quote_item')->load($this->getData('item_id'));
			
			
			// print_r($item->getProduct());
			$totalColor = $item->getTotalcolor();
            // Code to fetch total color used for add on price from sales_flat_quote_address_item table
            if($totalColor=='')
            {
                    $Item = Mage::getModel('sales/quote_item')->load($this->getData('quote_item_id'));  
                    $totalColor = $Item->getData('totalcolor');
            }
			if($item->getPdfdata()){
				$frontQCPrice = 0;
				$backQCPrice = 0;
				$leftQCPrice = 0;
				$rightQCPrice = 0;
				$totalQcPrice = 0;
				
				$frontSQArea = 0;
				$backSQArea = 0;
				$leftSQArea = 0;
				$rightSQArea = 0;				

				$frontQAPrice = 0;
				$backQAPrice = 0;
				$leftQAPrice = 0;
				$rightQAPrice = 0;
				$totalSquareAreaPrice = 0;
				
				$usedImageCounter = 0;
				$imageFixPrice = 0;
				$imagePrice = 0;
				
				$xmlData = new Varien_Simplexml_Config($item->getPdfdata());
				$designData = new Varien_Object(current($xmlData)->asArray());
				// echo "<pre>";
				// print_r($designData);
				$printMethodId = $designData->getData('printMethodId');
				$squareArea = $designData->getData('squareArea');				
				$iscustomized = $designData->getData('iscustomized');				
				$noofcolor = $designData->getData('noofcolor');				
				$noofimages = $designData->getData('noofimages');			
				
				$printingMethodData = Mage::getModel('printingmethod/printingmethod')->load($printMethodId);
				$qty = $this->getData('qty');
				if(count($printingMethodData)>0){				
					$pricingLogic = $printingMethodData->getPricingLogic();
					/* $pricingLogic = 2 = Quantity Color Price;
						$pricingLogic = 3 = Quantity Area Price*/
						if($pricingLogic == 1){
							if($iscustomized['front'] == 'true'){
								$frontFixedPrice = Mage::getModel('printingmethod/printingmethod')
												->getPrintingMethodFixedPrice($printMethodId, $qty, 'front');
							}
							if($iscustomized['back'] == 'true'){
								$backFixedPrice = Mage::getModel('printingmethod/printingmethod')
												->getPrintingMethodFixedPrice($printMethodId, $qty, 'back');
							}
							if($iscustomized['left'] == 'true'){
								$leftFixedPrice = Mage::getModel('printingmethod/printingmethod')
												->getPrintingMethodFixedPrice($printMethodId, $qty, 'left');
							}
							if($iscustomized['right'] == 'true'){
								$rightFixedPrice = Mage::getModel('printingmethod/printingmethod')
												->getPrintingMethodFixedPrice($printMethodId, $qty, 'right');
							}
							$totalFixedPrice = $totalFixedPrice + ($frontFixedPrice + $backFixedPrice + $leftFixedPrice + $rightFixedPrice);
							
							$price = $price + $totalFixedPrice;
						}else if($pricingLogic == 2){
							$frontColors = intval($noofcolor['front']);
							$backColors = intval($noofcolor['back']);
							$leftColors = intval($noofcolor['left']);
							$rightColors = intval($noofcolor['right']);
							if($frontColors > 0){
								$frontQCPrice = Mage::getModel('printingmethod/printingmethod')
												->getPrintingMethodQCPrice($printMethodId, $frontColors, $qty, 'front');
							}
							if($backColors > 0){
								$backQCPrice = Mage::getModel('printingmethod/printingmethod')
													->getPrintingMethodQCPrice($printMethodId, $backColors, $qty, 'back');
							}
							if($leftColors > 0){
								$leftQCPrice = Mage::getModel('printingmethod/printingmethod')
													->getPrintingMethodQCPrice($printMethodId, $leftColors, $qty, 'left');
							}
							if($rightColors > 0){
								$rightQCPrice = Mage::getModel('printingmethod/printingmethod')
													->getPrintingMethodQCPrice($printMethodId, $rightColors, $qty, 'right');				
						}										
						$totalQcPrice = $totalQcPrice + ($frontQCPrice + $backQCPrice + $leftQCPrice + $rightQCPrice);
						$price = $price + $totalQcPrice;
					}else if($pricingLogic == 3){
						$frontSQArea = floatval($squareArea['front']);
						$backSQArea = floatval($squareArea['back']);
						$leftSQArea = floatval($squareArea['left']);
						$rightSQArea = floatval($squareArea['right']);					
						
						if($frontSQArea > 0){
							$frontQAPrice = Mage::getModel('printingmethod/printingmethod')
									->getPrintingMethodQAPrice($printMethodId, $frontSQArea, $qty, 'front');
						}
						if($backSQArea > 0){
							$backQAPrice = Mage::getModel('printingmethod/printingmethod')
									->getPrintingMethodQAPrice($printMethodId, $backSQArea, $qty, 'back');
						}
						if($leftSQArea > 0){
							$leftQAPrice = Mage::getModel('printingmethod/printingmethod')
									->getPrintingMethodQAPrice($printMethodId, $leftSQArea, $qty, 'left');
						}
						if($rightSQArea > 0){
							$rightQAPrice = Mage::getModel('printingmethod/printingmethod')
									->getPrintingMethodQAPrice($printMethodId, $rightSQArea, $qty, 'right');
						}					
						
						$totalSquareAreaPrice = $totalSquareAreaPrice + ($frontQAPrice + $backQAPrice + $leftQAPrice + $rightQAPrice);
						$totalSquareArea = $squareArea['front'] + $squareArea['back'] + $squareArea['left'] + $squareArea['right'];						
						$price = $price + $totalSquareAreaPrice;							
					}
					
					$nameValue = $item->getNamevalue();
					$numberValue = $item->getNumbervalue();
					// Code to fetch name number vlaue used for add on price from sales_flat_quote_address_item table when multishipping
					if($nameValue=='')
					{
							$Item = Mage::getModel('sales/quote_item')->load($this->getData('quote_item_id'));  
							$nameValue = $Item->getData('namevalue');
					}
					
					if($numberValue=='')
					{
							$Item = Mage::getModel('sales/quote_item')->load($this->getData('quote_item_id'));  
							$numberValue = $Item->getData('numbervalue');
					}
					
					if($nameValue!='')
					{
						$nameArray = explode(',',$nameValue);				
						$namePrice = $printingMethodData->getNamePrice() * count($nameArray);				
						$price = $price + ($namePrice/$this->getData('qty'));
					}
					
					if($numberValue!='')
					{
						$numberArray = explode(',',$numberValue);							
						$numberPrice = $printingMethodData->getNumberPrice() * count($numberArray);
						$price = $price + ($numberPrice/$this->getData('qty'));
					}					
					$printingMethodData->getImagePrice();
					/*Image Price*/
					$imageFixPrice =(float) $printingMethodData->getImagePrice();
					$usedImageCounter = $noofimages['front'] + $noofimages['back'] + $noofimages['left'] + $noofimages['right'];
					$imageTotalPrice = $imageTotalPrice + ($usedImageCounter * $imageFixPrice * $qty);
					$price = $price + $imageTotalPrice;	
				}	
			}			
			
			
            $this->setData('calculation_price', $price);
        }
        return $price;
    }

    /**
     * Get calculation price used for quote calculation in base currency.
     *
     * @return float
     */
    public function getBaseCalculationPrice()
    {
        if (!$this->hasBaseCalculationPrice()) {
            if ($this->hasCustomPrice()) {
                $price = (float) $this->getCustomPrice();
                if ($price) {
                    $rate = $this->getStore()->convertPrice($price) / $price;
                    $price = $price / $rate;
                }
            } else {
                $price = $this->getPrice();
            }
            $this->setBaseCalculationPrice($price);
        }
        return $this->_getData('base_calculation_price');
    }

    /**
     * Get original calculation price used for quote calculation in base currency.
     *
     * @return float
     */
    public function getBaseCalculationPriceOriginal()
    {
        if (!$this->hasBaseCalculationPrice()) {
            if ($this->hasOriginalCustomPrice()) {
                $price = (float) $this->getOriginalCustomPrice();
                if ($price) {
                    $rate = $this->getStore()->convertPrice($price) / $price;
                    $price = $price / $rate;
                }
            } else {
                $price = $this->getPrice();
            }
			/*get addon price added by Ajay*/
			$item = Mage::getModel('sales/quote_item')->load($this->getData('item_id'));
			$totalColor = $item->getTotalcolor();
             // Code to fetch total color used for add on price from sales_flat_quote_address_item table when multishipping
            if($totalColor=='')
            {
                    $Item = Mage::getModel('sales/quote_item')->load($this->getData('quote_item_id'));  
                    $totalColor = $Item->getData('totalcolor');
            }
            /*
			if($totalColor>0)
            {
                    $qcPrice = Mage::getModel('qcprice/qcprice')->getQcPrice($totalColor,$this->getData('qty'));
                    $price = $price + $qcPrice;
            }
			*/
			if($item->getPdfdata()){
				$xmlData = new Varien_Simplexml_Config($item->getPdfdata());
				$designData = new Varien_Object(current($xmlData)->asArray());				
				$printMethodId = $designData->getData('printMethodId');
				$squareArea = $designData->getData('squareArea');
				$iscustomized = $designData->getData('iscustomized');	
							
				$noofcolor = $designData->getData('noofcolor');				
				$noofimages = $designData->getData('noofimages');				
				$printingMethodData = Mage::getModel('printingmethod/printingmethod')->load($printMethodId);
				$qty = $this->getData('qty');
				if(count($printingMethodData)>0){				
					$pricingLogic = $printingMethodData->getPricingLogic();
					/* $pricingLogic = 2 = Quantity Color Price;
						$pricingLogic = 3 = Quantity Area Price*/
						if($pricingLogic == 1){
							if($iscustomized['front'] == 'true'){
								$frontFixedPrice = Mage::getModel('printingmethod/printingmethod')
												->getPrintingMethodFixedPrice($printMethodId, $qty, 'front');
							}
							if($iscustomized['back'] == 'true'){
								$backFixedPrice = Mage::getModel('printingmethod/printingmethod')
												->getPrintingMethodFixedPrice($printMethodId, $qty, 'back');
							}
							if($iscustomized['left'] == 'true'){
								$leftFixedPrice = Mage::getModel('printingmethod/printingmethod')
												->getPrintingMethodFixedPrice($printMethodId, $qty, 'left');
							}
							if($iscustomized['right'] == 'true'){
								$rightFixedPrice = Mage::getModel('printingmethod/printingmethod')
												->getPrintingMethodFixedPrice($printMethodId, $qty, 'right');
							}
							$totalFixedPrice = $totalFixedPrice + ($frontFixedPrice + $backFixedPrice + $leftFixedPrice + $rightFixedPrice);
							$price = $price + $totalFixedPrice;
						}else if($pricingLogic == 2){
							$frontColors = intval($noofcolor['front']);
							$backColors = intval($noofcolor['back']);
							$leftColors = intval($noofcolor['left']);
							$rightColors = intval($noofcolor['right']);
							if($frontColors > 0){
								$frontQCPrice = Mage::getModel('printingmethod/printingmethod')
												->getPrintingMethodQCPrice($printMethodId, $frontColors, $qty, 'front');
							}
							if($backColors > 0){
								$backQCPrice = Mage::getModel('printingmethod/printingmethod')
													->getPrintingMethodQCPrice($printMethodId, $backColors, $qty, 'back');
							}
							if($leftColors > 0){
								$leftQCPrice = Mage::getModel('printingmethod/printingmethod')
													->getPrintingMethodQCPrice($printMethodId, $leftColors, $qty, 'left');
							}
							if($rightColors > 0){
								$rightQCPrice = Mage::getModel('printingmethod/printingmethod')
													->getPrintingMethodQCPrice($printMethodId, $rightColors, $qty, 'right');				
						}										
						$totalQcPrice = $totalQcPrice + ($frontQCPrice + $backQCPrice + $leftQCPrice + $rightQCPrice);
						$price = $price + $totalQcPrice;
					}else if($pricingLogic == 3){
						$frontSQArea = floatval($squareArea['front']);
						$backSQArea = floatval($squareArea['back']);
						$leftSQArea = floatval($squareArea['left']);
						$rightSQArea = floatval($squareArea['right']);					
						
						if($frontSQArea > 0){
							$frontQAPrice = Mage::getModel('printingmethod/printingmethod')
									->getPrintingMethodQAPrice($printMethodId, $frontSQArea, $qty, 'front');
						}
						if($backSQArea > 0){
							$backQAPrice = Mage::getModel('printingmethod/printingmethod')
									->getPrintingMethodQAPrice($printMethodId, $backSQArea, $qty, 'front');
						}
						if($leftSQArea > 0){
							$leftQAPrice = Mage::getModel('printingmethod/printingmethod')
									->getPrintingMethodQAPrice($printMethodId, $leftSQArea, $qty, 'front');
						}
						if($rightSQArea > 0){
							$rightQAPrice = Mage::getModel('printingmethod/printingmethod')
									->getPrintingMethodQAPrice($printMethodId, $rightSQArea, $qty, 'front');
						}					
						
						$totalSquareAreaPrice = $totalSquareAreaPrice + ($frontQAPrice + $backQAPrice + $leftQAPrice + $rightQAPrice);
						$totalSquareArea = $squareArea['front'] + $squareArea['back'] + $squareArea['left'] + $squareArea['right'];						
						$price = $price + $totalSquareAreaPrice;							
					}
					
					$nameValue = $item->getNamevalue();
					$numberValue = $item->getNumbervalue();
					// Code to fetch name number vlaue used for add on price from sales_flat_quote_address_item table when multishipping
					if($nameValue=='')
					{
							$Item = Mage::getModel('sales/quote_item')->load($this->getData('quote_item_id'));  
							$nameValue = $Item->getData('namevalue');
					}
					
					if($numberValue=='')
					{
							$Item = Mage::getModel('sales/quote_item')->load($this->getData('quote_item_id'));  
							$numberValue = $Item->getData('numbervalue');
					}
					
					if($nameValue!='')
					{
						$nameArray = explode(',',$nameValue);				
						$namePrice = $printingMethodData->getNamePrice() * count($nameArray);				
						$price = $price + ($namePrice/$this->getData('qty'));
					}
					
					if($numberValue!='')
					{
						$numberArray = explode(',',$numberValue);							
						$numberPrice = $printingMethodData->getNumberPrice() * count($numberArray);
						$price = $price + ($numberPrice/$this->getData('qty'));
					}					
					$printingMethodData->getImagePrice();
					/*Image Price*/
					$imageFixPrice =(float) $printingMethodData->getImagePrice();
					$usedImageCounter = $noofimages['front'] + $noofimages['back'] + $noofimages['left'] + $noofimages['right'];
					$imageTotalPrice = $imageTotalPrice + ($usedImageCounter * $imageFixPrice * $qty);
					$price = $price + $imageTotalPrice;	
				}	
			}
			
			
            $this->setBaseCalculationPrice($price);
        }
        return $this->_getData('base_calculation_price');
    }

    /**
     * Get whether the item is nominal
     * TODO: fix for multishipping checkout
     *
     * @return bool
     */
    public function isNominal()
    {
        if (!$this->hasData('is_nominal')) {
            $this->setData('is_nominal', $this->getProduct() ? '1' == $this->getProduct()->getIsRecurring() : false);
        }
        return $this->_getData('is_nominal');
    }

    /**
     * Data getter for 'is_nominal'
     * Used for converting item to order item
     *
     * @return int
     */
    public function getIsNominal()
    {
        return (int)$this->isNominal();
    }

    /**
     * Get original price (retrieved from product) for item.
     * Original price value is in quote selected currency
     *
     * @return float
     */
    public function getOriginalPrice()
    {
        $price = $this->_getData('original_price');
        if (is_null($price)) {
            $price = $this->getStore()->convertPrice($this->getBaseOriginalPrice());
            $this->setData('original_price', $price);
        }
        return $price;
    }

    /**
     * Set original price to item (calculation price will be refreshed too)
     *
     * @param   float $price
     * @return  Mage_Sales_Model_Quote_Item_Abstract
     */
    public function setOriginalPrice($price)
    {
        return $this->setData('original_price', $price);
    }

    /**
     * Get Original item price (got from product) in base website currency
     *
     * @return float
     */
    public function getBaseOriginalPrice()
    {
        return $this->_getData('base_original_price');
    }

    /**
     * Specify custom item price (used in case whe we have apply not product price to item)
     *
     * @param   float $value
     * @return  Mage_Sales_Model_Quote_Item_Abstract
     */
    public function setCustomPrice($value)
    {
        $this->setCalculationPrice($value);
        $this->setBaseCalculationPrice(null);
        return $this->setData('custom_price', $value);
    }

    /**
     * Get item price. Item price currency is website base currency.
     *
     * @return decimal
     */
    public function getPrice()
    {
        return $this->_getData('price');
    }

    /**
     * Specify item price (base calculation price and converted price will be refreshed too)
     *
     * @param   float $value
     * @return  Mage_Sales_Model_Quote_Item_Abstract
     */
    public function setPrice($value)
    {
        $this->setBaseCalculationPrice(null);
        $this->setConvertedPrice(null);
        return $this->setData('price', $value);
    }

    /**
     * Get item price converted to quote currency
     * @return float
     */
    public function getConvertedPrice()
    {
        $price = $this->_getData('converted_price');
        if (is_null($price)) {
            $price = $this->getStore()->convertPrice($this->getPrice());
            $this->setData('converted_price', $price);
        }
        return $price;
    }

    /**
     * Set new value for converted price
     * @param float $value
     * @return Mage_Sales_Model_Quote_Item_Abstract
     */
    public function setConvertedPrice($value)
    {
        $this->setCalculationPrice(null);
        $this->setData('converted_price', $value);
        return $this;
    }

    /**
     * Clone quote item
     *
     * @return Mage_Sales_Model_Quote_Item
     */
    public function __clone()
    {
        $this->setId(null);
        $this->_parentItem  = null;
        $this->_children    = array();
        $this->_messages    = array();
        return $this;
    }

    /**
     * Checking if there children calculated or parent item
     * when we have parent quote item and its children
     *
     * @return bool
     */
    public function isChildrenCalculated()
    {
        if ($this->getParentItem()) {
            $calculate = $this->getParentItem()->getProduct()->getPriceType();
        } else {
            $calculate = $this->getProduct()->getPriceType();
        }

        if ((null !== $calculate) && (int)$calculate === Mage_Catalog_Model_Product_Type_Abstract::CALCULATE_CHILD) {
            return true;
        }
        return false;
    }

    /**
     * Checking can we ship product separatelly (each child separately)
     * or each parent product item can be shipped only like one item
     *
     * @return bool
     */
    public function isShipSeparately()
    {
        if ($this->getParentItem()) {
            $shipmentType = $this->getParentItem()->getProduct()->getShipmentType();
        } else {
            $shipmentType = $this->getProduct()->getShipmentType();
        }

        if ((null !== $shipmentType) &&
            (int)$shipmentType === Mage_Catalog_Model_Product_Type_Abstract::SHIPMENT_SEPARATELY) {
            return true;
        }
        return false;
    }


    /**
     * Calculate item tax amount
     *
     * @deprecated logic moved to tax totals calculation model
     * @return  Mage_Sales_Model_Quote_Item
     */
    public function calcTaxAmount()
    {
        $store = $this->getStore();

        if (!Mage::helper('tax')->priceIncludesTax($store)) {
            if (Mage::helper('tax')->applyTaxAfterDiscount($store)) {
                $rowTotal       = $this->getRowTotalWithDiscount();
                $rowBaseTotal   = $this->getBaseRowTotalWithDiscount();
            } else {
                $rowTotal       = $this->getRowTotal();
                $rowBaseTotal   = $this->getBaseRowTotal();
            }

            $taxPercent = $this->getTaxPercent()/100;

            $this->setTaxAmount($store->roundPrice($rowTotal * $taxPercent));
            $this->setBaseTaxAmount($store->roundPrice($rowBaseTotal * $taxPercent));

            $rowTotal       = $this->getRowTotal();
            $rowBaseTotal   = $this->getBaseRowTotal();
            $this->setTaxBeforeDiscount($store->roundPrice($rowTotal * $taxPercent));
            $this->setBaseTaxBeforeDiscount($store->roundPrice($rowBaseTotal * $taxPercent));
        } else {
            if (Mage::helper('tax')->applyTaxAfterDiscount($store)) {
                $totalBaseTax = $this->getBaseTaxAmount();
                $totalTax = $this->getTaxAmount();

                if ($totalTax && $totalBaseTax) {
                    $totalTax -= $this->getDiscountAmount()*($this->getTaxPercent()/100);
                    $totalBaseTax -= $this->getBaseDiscountAmount()*($this->getTaxPercent()/100);

                    $this->setBaseTaxAmount($store->roundPrice($totalBaseTax));
                    $this->setTaxAmount($store->roundPrice($totalTax));
                }
            }
        }

        if (Mage::helper('tax')->discountTax($store) && !Mage::helper('tax')->applyTaxAfterDiscount($store)) {
            if ($this->getDiscountPercent()) {
                $baseTaxAmount =  $this->getBaseTaxBeforeDiscount();
                $taxAmount = $this->getTaxBeforeDiscount();

                $baseDiscountDisposition = $baseTaxAmount/100*$this->getDiscountPercent();
                $discountDisposition = $taxAmount/100*$this->getDiscountPercent();

                $this->setDiscountAmount($this->getDiscountAmount()+$discountDisposition);
                $this->setBaseDiscountAmount($this->getBaseDiscountAmount()+$baseDiscountDisposition);
            }
        }

        return $this;
    }

    /**
     * Get item tax amount
     *
     * @deprecated
     * @return  decimal
     */
    public function getTaxAmount()
    {
        return $this->_getData('tax_amount');
    }


    /**
     * Get item base tax amount
     *
     * @deprecated
     * @return decimal
     */
    public function getBaseTaxAmount()
    {
        return $this->_getData('base_tax_amount');
    }

    /**
     * Get item price (item price always exclude price)
     *
     * @deprecated
     * @return decimal
     */
    protected function _calculatePrice($value, $saveTaxes = true)
    {
        $store = $this->getQuote()->getStore();

        if (Mage::helper('tax')->priceIncludesTax($store)) {
            $bAddress = $this->getQuote()->getBillingAddress();
            $sAddress = $this->getQuote()->getShippingAddress();

            $address = $this->getAddress();

            if ($address) {
                switch ($address->getAddressType()) {
                    case Mage_Sales_Model_Quote_Address::TYPE_BILLING:
                        $bAddress = $address;
                        break;
                    case Mage_Sales_Model_Quote_Address::TYPE_SHIPPING:
                        $sAddress = $address;
                        break;
                }
            }

            if ($this->getProduct()->getIsVirtual()) {
                $sAddress = $bAddress;
            }

            $priceExcludingTax = Mage::helper('tax')->getPrice(
                $this->getProduct()->setTaxPercent(null),
                $value,
                false,
                $sAddress,
                $bAddress,
                $this->getQuote()->getCustomerTaxClassId(),
                $store
            );

            $priceIncludingTax = Mage::helper('tax')->getPrice(
                $this->getProduct()->setTaxPercent(null),
                $value,
                true,
                $sAddress,
                $bAddress,
                $this->getQuote()->getCustomerTaxClassId(),
                $store
            );

            if ($saveTaxes) {
                $qty = $this->getQty();
                if ($this->getParentItem()) {
                    $qty = $qty*$this->getParentItem()->getQty();
                }

                if (Mage::helper('tax')->displayCartPriceInclTax($store)) {
                    $rowTotal = $value*$qty;
                    $rowTotalExcTax = Mage::helper('tax')->getPrice(
                        $this->getProduct()->setTaxPercent(null),
                        $rowTotal,
                        false,
                        $sAddress,
                        $bAddress,
                        $this->getQuote()->getCustomerTaxClassId(),
                        $store
                    );
                    $rowTotalIncTax = Mage::helper('tax')->getPrice(
                        $this->getProduct()->setTaxPercent(null),
                        $rowTotal,
                        true,
                        $sAddress,
                        $bAddress,
                        $this->getQuote()->getCustomerTaxClassId(),
                        $store
                    );
                    $totalBaseTax = $rowTotalIncTax-$rowTotalExcTax;
                    $this->setRowTotalExcTax($rowTotalExcTax);
                }
                else {
                    $taxAmount = $priceIncludingTax - $priceExcludingTax;
                    $this->setTaxPercent($this->getProduct()->getTaxPercent());
                    $totalBaseTax = $taxAmount*$qty;
                }

                $totalTax = $this->getStore()->convertPrice($totalBaseTax);
                $this->setTaxBeforeDiscount($totalTax);
                $this->setBaseTaxBeforeDiscount($totalBaseTax);

                $this->setTaxAmount($totalTax);
                $this->setBaseTaxAmount($totalBaseTax);
            }

            $value = $priceExcludingTax;
        }

        return $value;
    }
}
