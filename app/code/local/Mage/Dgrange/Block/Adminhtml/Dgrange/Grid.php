	<?php

class Mage_Dgrange_Block_Adminhtml_Dgrange_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
  {
      parent::__construct();
      $this->setId('dgrangeGrid');
      $this->setDefaultSort('dgrange_id');
      $this->setDefaultDir('ASC');
      $this->setSaveParametersInSession(true);
  }

  protected function _prepareCollection()
  {
      $collection = Mage::getModel('dgrange/dgrange')->getCollection();
	  $collection->getSelect()->columns(new Zend_Db_Expr("Concat(quantity_range_from,' - ',quantity_range_to) as quantity_range"));
	  
      $this->setCollection($collection);
      return parent::_prepareCollection();
  }

  protected function _prepareColumns()
  {
      $this->addColumn('dgrange_id', array(
          'header'    => Mage::helper('dgrange')->__('ID'),
          'align'     => 'right',
          'width'     => '50px',
          'index'     => 'dgrange_id',
      ));

      $this->addColumn('quantity_range', array(
          'header'    => Mage::helper('dgrange')->__('Quantity Range'),
          'align'     => 'left',
		  'index'     => 'quantity_range',
		  'filter'	  => false
      ));

	  /*
      $this->addColumn('content', array(
			'header'    => Mage::helper('dgrange')->__('Item Content'),
			'width'     => '150px',
			'index'     => 'content',
      ));
	  */

      /*$this->addColumn('status', array(
          'header'    => Mage::helper('dgrange')->__('Status'),
          'align'     => 'left',
          'width'     => '80px',
          'index'     => 'status',
          'type'      => 'options',
          'options'   => array(
              1 => 'Enabled',
              2 => 'Disabled',
          ),
      ));*/
	  
        $this->addColumn('action',
            array(
                'header'    =>  Mage::helper('dgrange')->__('Action'),
                'width'     => '100',
                'type'      => 'action',
                'getter'    => 'getId',
                'actions'   => array(
                    array(
                        'caption'   => Mage::helper('dgrange')->__('Edit'),
                        'url'       => array('base'=> '*/*/edit'),
                        'field'     => 'id'
                    )
                ),
                'filter'    => false,
                'sortable'  => false,
                'index'     => 'stores',
                'is_system' => true,
        ));
		
		$this->addExportType('*/*/exportCsv', Mage::helper('dgrange')->__('CSV'));
		$this->addExportType('*/*/exportXml', Mage::helper('dgrange')->__('XML'));
	  
      return parent::_prepareColumns();
  }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('dgrange_id');
        $this->getMassactionBlock()->setFormFieldName('dgrange');

        $this->getMassactionBlock()->addItem('delete', array(
             'label'    => Mage::helper('dgrange')->__('Delete'),
             'url'      => $this->getUrl('*/*/massDelete'),
             'confirm'  => Mage::helper('dgrange')->__('Are you sure?')
        ));

        $statuses = Mage::getSingleton('dgrange/status')->getOptionArray();

        array_unshift($statuses, array('label'=>'', 'value'=>''));
        $this->getMassactionBlock()->addItem('status', array(
             'label'=> Mage::helper('dgrange')->__('Change status'),
             'url'  => $this->getUrl('*/*/massStatus', array('_current'=>true)),
             'additional' => array(
                    'visibility' => array(
                         'name' => 'status',
                         'type' => 'select',
                         'class' => 'required-entry',
                         'label' => Mage::helper('dgrange')->__('Status'),
                         'values' => $statuses
                     )
             )
        ));
        return $this;
    }

  public function getRowUrl($row)
  {
      return $this->getUrl('*/*/edit', array('id' => $row->getId()));
  }

}