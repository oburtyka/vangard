<?php

class Mage_Dgrange_Block_Adminhtml_Dgrange_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{

  public function __construct()
  {
      parent::__construct();
      $this->setId('dgrange_tabs');
      $this->setDestElementId('edit_form');
      $this->setTitle(Mage::helper('dgrange')->__('Quantity Range Information'));
  }

  protected function _beforeToHtml()
  {
      $this->addTab('form_section', array(
          'label'     => Mage::helper('dgrange')->__('Quantity Range Information'),
          'title'     => Mage::helper('dgrange')->__('Quantity Range Information'),
          'content'   => $this->getLayout()->createBlock('dgrange/adminhtml_dgrange_edit_tab_form')->toHtml(),
      ));
     
      return parent::_beforeToHtml();
  }
}