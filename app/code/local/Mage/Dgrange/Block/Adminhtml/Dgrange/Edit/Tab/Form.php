<?php

class Mage_Dgrange_Block_Adminhtml_Dgrange_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
  protected function _prepareForm()
  {
      $form = new Varien_Data_Form();
      $this->setForm($form);
      $fieldset = $form->addFieldset('dgrange_form', array('legend'=>Mage::helper('dgrange')->__('Quantity Range information')));
     
      $fieldset->addField('quantity_range_from', 'text', array(
          'label'     => Mage::helper('dgrange')->__('Quantity From'),
          'class'     => 'required-entry',
          'required'  => true,
          'name'      => 'quantity_range_from',
      ));

      $fieldset->addField('quantity_range_to', 'text', array(
          'label'     => Mage::helper('dgrange')->__('Quantity To'),
          'required'  => false,
          'name'      => 'quantity_range_to',
	  ));
     
      if ( Mage::getSingleton('adminhtml/session')->getDgrangeData() )
      {
          $form->setValues(Mage::getSingleton('adminhtml/session')->getDgrangeData());
          Mage::getSingleton('adminhtml/session')->setDgrangeData(null);
      } elseif ( Mage::registry('dgrange_data') ) {
          $form->setValues(Mage::registry('dgrange_data')->getData());
      }
      return parent::_prepareForm();
  }
}