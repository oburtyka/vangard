<?php

class Mage_Dgrange_Block_Adminhtml_Dgrange_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
                 
        $this->_objectId = 'id';
        $this->_blockGroup = 'dgrange';
        $this->_controller = 'adminhtml_dgrange';
        
        $this->_updateButton('save', 'label', Mage::helper('dgrange')->__('Save Quantity Range'));
        $this->_updateButton('delete', 'label', Mage::helper('dgrange')->__('Delete Quantity Range'));
		
        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save And Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "
            function toggleEditor() {
                if (tinyMCE.getInstanceById('dgrange_content') == null) {
                    tinyMCE.execCommand('mceAddControl', false, 'dgrange_content');
                } else {
                    tinyMCE.execCommand('mceRemoveControl', false, 'dgrange_content');
                }
            }

            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    public function getHeaderText()
    {
        if( Mage::registry('dgrange_data') && Mage::registry('dgrange_data')->getId() ) {
            return Mage::helper('dgrange')->__("Edit Item '%s'", $this->htmlEscape(Mage::registry('dgrange_data')->getTitle()));
        } else {
            return Mage::helper('dgrange')->__('Add Quantity Range');
        }
    }
}