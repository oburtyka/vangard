<?php
class Mage_Dgrange_Block_Adminhtml_Dgrange extends Mage_Adminhtml_Block_Widget_Grid_Container
{
  public function __construct()
  {
    $this->_controller = 'adminhtml_dgrange';
    $this->_blockGroup = 'dgrange';
    $this->_headerText = Mage::helper('dgrange')->__('Quantity Range Manager');
    $this->_addButtonLabel = Mage::helper('dgrange')->__('Add Quantity Range');
    parent::__construct();
  }
}