<?php
class Mage_Dgrange_Block_Dgrange extends Mage_Core_Block_Template
{
	public function _prepareLayout()
    {
		return parent::_prepareLayout();
    }
    
     public function getDgrange()     
     { 
        if (!$this->hasData('dgrange')) {
            $this->setData('dgrange', Mage::registry('dgrange'));
        }
        return $this->getData('dgrange');
        
    }
}