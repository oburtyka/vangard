<?php

class Mage_Dgrange_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
    	
    	/*
    	 * Load an object by id 
    	 * Request looking like:
    	 * http://site.com/dgrange?id=15 
    	 *  or
    	 * http://site.com/dgrange/id/15 	
    	 */
    	/* 
		$dgrange_id = $this->getRequest()->getParam('id');

  		if($dgrange_id != null && $dgrange_id != '')	{
			$dgrange = Mage::getModel('dgrange/dgrange')->load($dgrange_id)->getData();
		} else {
			$dgrange = null;
		}	
		*/
		
		 /*
    	 * If no param we load a the last created item
    	 */ 
    	/*
    	if($dgrange == null) {
			$resource = Mage::getSingleton('core/resource');
			$read= $resource->getConnection('core_read');
			$dgrangeTable = $resource->getTableName('dgrange');
			
			$select = $read->select()
			   ->from($dgrangeTable,array('dgrange_id','title','content','status'))
			   ->where('status',1)
			   ->order('created_time DESC') ;
			   
			$dgrange = $read->fetchRow($select);
		}
		Mage::register('dgrange', $dgrange);
		*/

			
		$this->loadLayout();     
		$this->renderLayout();
    }
}