<?php

class Mage_Dgrange_Model_Mysql4_Dgrange_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{
    public function _construct()
    {
        parent::_construct();
        $this->_init('dgrange/dgrange');
    }
}