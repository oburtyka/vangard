<?php

class Mage_Dgrange_Model_Mysql4_Dgrange extends Mage_Core_Model_Mysql4_Abstract
{
    public function _construct()
    {    
        // Note that the dgrange_id refers to the key field in your database table.
        $this->_init('dgrange/dgrange', 'dgrange_id');
    }
}