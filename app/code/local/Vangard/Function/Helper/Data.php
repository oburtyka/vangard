<?php

class Vangard_Function_Helper_Data extends Mage_Core_Helper_Abstract{
	
	public function testing(){
		echo "Function Test.";
	}
	
	public function featuredProduct($data){
		$limit = $data['limit']; if (!$limit) $limit = 4;
		$inner = $data['inner']; if ($inner==null) $inner=0;
		$collection = Mage::getModel('catalog/product')->getCollection();
		$collection->addAttributeToSelect('*');	
		$collection->addFieldToFilter(array(
			array('attribute'=>'featured','eq'=>1),
		));
	 
		$i=0; $show = $limit; $desclen = 45; //var_dump($products->_data);
		foreach ($collection as $product) {
			if ($i < $show){			
				$title = $product->getName(); 
				$img = Mage::helper('catalog/image')->init($product, 'image')->resize(200,193);
				$desc = $product->getShortDescription(); if (strlen($desc)>$desclen) $desc = substr($desc,0,$desclen)."...";
				$url = $product->unsRequestPath()->getUrlInStore(array('_ignore_category' => true)); 
				$list .= '<li><a href="'.$url.'"><img src="'.$img.'" /><p class="title">'.$title.'</p><p class="desc">'.$desc.'</p><p class="more">Mere information om produktet ></p></a></li>';	
			}
			$i++;
		}
 
		if ($list) : ?>
		
		<div class="xbox featured_products <?php if ($inner) echo "inner"; ?>">
			<div class="inbox">	
				<h2>Udvalgte Produkter</h2>
				<ul class="product list">
					<?php echo $list; ?>
				</ul>
			</div>
		</div>		
		
		<?php
		endif;
	}
	
	public function attrID2Label($attr,$val,$rev=0){ // attr_color, 45 = Blue
		$attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product',$attr);
		$attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
		$opts = $attribute ->getSource()->getAllOptions();		 
		foreach ($opts as $a) { //var_dump($a);
			if (!trim($a['value'])) continue;
			$opt[$a['value']] = $a['label'];		
			$optR[$a['label']] = $a['value'];		
		} //echo $val." = ".$opt[$val]; //var_dump($opt);
		if ($rev) return $optR[$val];
		else return $opt[$val];
	}
	
	public function array_flatten($var) {
		ob_start();
		var_dump($var);
		$result = ob_get_clean();
		return $result;
	}	
	 
	public function doLog($text){		
		$filename = Mage::getBaseDir()."\_pu\updates.log";
		$date =  Mage::getModel('core/date')->date('Y-m-d H:i:s');
		$string = "[$date]  $text \n";
		//echo "$filename / $string";
		
		$context = stream_context_create();
		$fp = fopen($filename, 'r', 1, $context);		
		$tmpname = md5($string);
		file_put_contents($tmpname, $string);
		file_put_contents($tmpname, $fp, FILE_APPEND);
		fclose($fp);
		unlink($filename);
		rename($tmpname, $filename);
	}
	
	public function doLogger($text,$file){
		umask(0);
		$filename = Mage::getBaseDir()."\_pu\/".$file;
		$date =  Mage::getModel('core/date')->date('Y-m-d H:i:s');
		$string = "[$date] $text \n";
		//echo "$filename / $string";
		
		$context = stream_context_create();
		$fp = fopen($filename, 'r', 1, $context);		
		$tmpname = md5($string);
		file_put_contents($tmpname, $string);
		file_put_contents($tmpname, $fp, FILE_APPEND);
		fclose($fp);
		unlink($filename);
		rename($tmpname, $filename);
	}
	
	public function attOrder($att,$atts){	
		$attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $att);
		if ($attribute->usesSource())	{
			$options = $attribute->getSource()->getAllOptions('positions');
		}

		foreach($options as $val){   
			$raw[] = $val['label'];
			if (in_array($val['label'],$atts)) $sorted[] = $val['label'];
		}
		
		return $sorted;		

		/*
		$attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product',$att);
		$attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
		return $attribute ->getSource()->getAllOptions();		 
		*/		 
	}
	
	public function attAdd($att,$set){	
		$set = trim($set);
		$att = htmlentities(trim($att), ENT_QUOTES | ENT_IGNORE, "ISO-8859-15");
		if (!$att || !$set) return; 
		
		$attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $set);				
		if ($attribute->usesSource())	{			
			$options = $attribute->getSource()->getAllOptions('positions');			
		}

		foreach($options as $val){   
			$encoded = trim($val['label']);  
			if ($encoded) $attSet[] = $encoded;
		}
		//echo "$att / $set <br/><pre>".print_r($attSet,true)."</pre>"; //exit();	 
	 
		if (!in_array($att,$attSet)){ // if attribute value not in attribute options, ADD!!
			$data['option']['value'] = array(array( 0 => $att  ));
			$attribute->addData($data);
			try {
				$attribute->save();
				$session = Mage::getSingleton('adminhtml/session');
				Mage::app()->cleanCache(array(Mage_Core_Model_Translate::CACHE_TAG));
				$session->setAttributeData(false);			
				echo "Add Attribute $att in $set Success!!";
				return true;
			} catch (Exception $e) {
				echo $e->getMessage();	
				echo "Failed to Add Attribute $att in $set.";
			}	
		} else {
			echo "Attribute $att in $set already exists!";
			return true;
		}
	}
	
	public function simpleText($string){
		  $string = ereg_replace("�", "&aelig;", $string);
		  $string = ereg_replace("�", "&oslash;", $string);
		  $string = ereg_replace("�", "&aring;", $string);
		  $string = ereg_replace("�", "&AElig;", $string);
		  $string = ereg_replace("�", "&Oslash;", $string);
		  $string = ereg_replace("�", "&Aring;", $string);	
		  return $string;
	}
	
	public function showOrder($orderId,$dev=false){	
		/*
		Ordrenr = Ordernumber
		Ordredato = Order date
		Kundenr = customer no
		Kundenavn = companyname
		Adresse = address
		Postnr_by = zipcode and city
		Leveringsnavn = shipping name (companyname if another shipping companyname isn�t defined)
		Leveringsadresse = Shipping address (billing address if shipping address is not defined)
		Leverings_postnr = Shipping zipcode and city (billing zipcode and city if shipping zipcode and city is not defined)
		Bemaerkninger = address prefix and should say �Att. prefixfield�
		attention = VAT no
		 
		Now all the items ordered by the customer is being listed. Here is a description of the content for each item
		 
		varenr = item number
		produktnavn = Product titel
		antal = item quantity
		price = price in DKK	
		*/
	
		$order = Mage::getSingleton('sales/order')->load($orderId);
		$_totalData = $order->getData(); 
		$_sub = $_totalData['subtotal']; if ($dev) var_dump($_totalData);
			  
		$orderID = $orderId;	
		$orderID = $_totalData['increment_id'];
		$orderDate = $_totalData['created_at'];
		$cID = $_totalData['customer_id'];
		$customer = Mage::getModel('customer/customer')->load($cID)->getData(); if ($dev) var_dump($customer);	
		$cTaxVat = $customer['taxvat'];
		
		$cAddBillID = $_totalData['billing_address_id']; //$customer['default_billing'];
		$cAddBillRaw = Mage::getModel('sales/order_address')->load($cAddBillID)->getData(); if ($dev) var_dump($cAddBillRaw);
		$cAddBillCompany = $cAddBillRaw['company'];
		$cAddBillStreet = $cAddBillRaw['street'];
		$cAddBillZipCity = $cAddBillRaw['postcode']." ".$cAddBillRaw['city'];
		
		$cAddShipID = $_totalData['shipping_address_id']; //$customer['default_shipping'];
		$cAddShipRaw = Mage::getModel('sales/order_address')->load($cAddShipID)->getData(); if ($dev) var_dump($cAddShipRaw);
		$cAddShipCompany =  $cAddShipRaw['company'];
		$cAddShipStreet = $cAddShipRaw['street'];
		$cAddShipZipCity = $cAddShipRaw['postcode']." ".$cAddShipRaw['city'];		
		$cAddPrefix = $cAddShipRaw['prefix'];
		$cAddPhone = $cAddShipRaw['telephone'];
		
		$allitems = $order->getAllItems();
		foreach($allitems as $item){
			$itemID = $item->getSku();
			$itemName = $item->getName();
			$itemPrice = $item->getPrice(); $itemPrice = number_format($itemPrice);
			$itemQTY = $item->getQtyToShip();		
			$itemList .= 
			"<linie>
			<varenr>$itemID</varenr>
			<produktnavn>$itemName</produktnavn>
			<antal>$itemQTY</antal>
			<pris>$itemPrice</pris>
			</linie>\n";
			//echo "<p>$itemID | $itemName | $itemPrice x $itemQTY</p>";
		}

		$textTop = "
		<Ordrenr>$orderID</Ordrenr>
		<Ordredato>$orderDate</Ordredato>
		<Kundenr>$cAddPhone</Kundenr>
		<Kundenavn>$cAddBillCompany</Kundenavn>
		<Adresse>$cAddBillStreet</Adresse>
		<Postnr_by>$cAddBillZipCity</Postnr_by>
		<Leveringsnavn>$cAddShipCompany</Leveringsnavn>
		<Leveringsadresse>$cAddShipStreet</Leveringsadresse>
		<Leverings_postnr>$cAddShipZipCity</Leverings_postnr>
		<Bemaerkninger>$cAddPrefix</Bemaerkninger>
		<attention>$cTaxVat</attention>\n\n";
		
		$body = "$textTop<ordrelinjer>$itemList</ordrelinjer>";				
		$xml="<ordre>";
		$xml .= html_entity_decode($body, ENT_NOQUOTES, 'UTF-8');
		$xml .="</ordre>";
		$xml = preg_replace('/\t+/', '', $xml);				
		
		umask(0);
		$file = Mage::getBaseDir()."/ud/$orderID.xml"; 
		//Mage::helper('function/Data')->doLogger("Saving to: $file ","exportxml.log");
		//$fh = fopen($file, 'w') or die("Failed to create xml.");
		$fh = fopen($file, 'w');
		fwrite($fh, $xml); fclose($fh);
		
		if ($dev){
			echo $file;
			echo "<p>CustomerID: $cID / LastOrderID: $orderId</p>";					
			echo "<pre>$xml</pre>";			
		}
		
		//http://injustfiveminutes.wordpress.com/2012/11/30/change-starting-order-number-in-magento/
		//$xmlobj=new SimpleXMLElement($xml);
		//$xmlobj->asXML($file);
	
	}
	
	public function autoCurrency(){
		//$enabled= Mage::getStoreConfig('vangard/currency/enabled'); 
		//if (!$enabled) return; // If not enabled, use default.

		$groupId = Mage::getSingleton('customer/session')->getCustomerGroupId();	
		if ($groupId>0) {
			$group = Mage::getModel('customer/group')->load($groupId); 
			$groupName =  $group->getCode(); 			
			$storeCurrency = Mage::app()->getStore()->getCurrentCurrencyCode(); 
			$userCurrency = $storeCurrency;
			
			if ($groupName=="Euro") $userCurrency = "EUR"; 			
			else if ($groupName=="Dkk") $userCurrency = "DKK"; 			
			
			echo "<p style='display:none;'>$storeCurrency != $userCurrency </p>";

			if ($storeCurrency != $userCurrency) {
				$switchURL = Mage::helper('directory/url')->getSwitchCurrencyUrl()."currency/".$userCurrency;
				echo "<p>Switching to user currency $userCurrency...</p><script>window.location = '$switchURL';</script>";
				echo "<p style='display:none;'>$switchURL</p>";
				//echo "$storeCurrency != $userCurrency"; return;
				//Mage::app()->getResponse()->setRedirect($switchURL); 															
			} 
		}
		return;		
	}
	
	public function getCuSymbol(){
		$storeCurrency = Mage::app()->getStore()->getCurrentCurrencyCode(); 
		if ($storeCurrency=="DKK") $storeCurrency = "DKR";
		return $storeCurrency;
	}
	
	public function getPrice($id,$type=null){			
		//$this->autoCurrency();
		Mage::helper('function')->autoCurrency();

		$p = Mage::getModel('catalog/product')->load($id); 		
		if ($p->getData('type_id') == "grouped"){ //Check if the product is GROUP, if so load its first product instead
			$groupSub = $p->getTypeInstance(true)->getAssociatedProducts($p); //First product from group 
			if ($groupSub){				
				foreach ($groupSub as $gs){
					$temp["id"] = intval($gs->getId());
					$temp["msrp"] = intval($gs->getMsrp());
					$temp["srp"] = intval($gs->getPrice());
					$temp["name"] = $gs->getName();
					$temp["groupMsrp"] = intval($gs->getData("msrp_alt"));
					$temp["groupSrp"] = intval($gs->getGroupPrice());					
					$gPrices["groupMsrp"][$temp["id"]] = $temp["groupMsrp"];
					$gPrices["groupSrp"][$temp["id"]] = $temp["groupSrp"];
					//echo "<p>[{$temp['id']}]  {$temp['name']} / {$temp['msrp']} / {$temp['srp']} / {$temp['groupMsrp']} / {$temp['groupSrp']}</p>";
				}
				arsort($gPrices["groupSrp"], SORT_NUMERIC); 
				arsort($gPrices["groupMsrp"], SORT_NUMERIC); 
				reset($gPrices["groupMsrp"]);
				$highest["msrpProductId"] = key($gPrices["groupMsrp"]);				
				$highest["msrpPrice"] = $gPrices["groupMsrp"][$highest["msrpProductId"]];
				//$p = Mage::getModel('catalog/product')->load($groupSub[0]->getId()); 	
				//echo "<p>Highest MSRP: ".$highest["msrpProductId"]." = ".$highest["msrpPrice"]."</p>";
				//echo "<p style='display:none'>".$groupSub[0]->getId()."</p>";
				$p = Mage::getModel('catalog/product')->load($highest["msrpProductId"]); 	
			}
		}
		 
		//var_dump($gPrices["groupSrp"]);
		//var_dump($gPrices["groupMsrp"]);
		//return;
		
		$msrp = $p->getMsrp();
		$srp = $p->getPrice();
		
		//Currency
		$storeCurrency = Mage::app()->getStore()->getCurrentCurrencyCode(); 
		$userCurrency = $storeCurrency;
		$groupId = Mage::getSingleton('customer/session')->getCustomerGroupId();	
		if ($groupId>0){ // If loggedIn
			$group = Mage::getModel('customer/group')->load($groupId); 
			$groupName =  $group->getCode(); 
			$groupPrice = $p->getGroupPrice();	
			$srp = $groupPrice;
			if ($groupName=="Euro")  {
				$userCurrency = "EUR";  
				$msrp = $p->getData("msrp_alt");
			}
		}
		$cuSymbol = $userCurrency;
		if ($cuSymbol=="DKK") $cuSymbol = "DKR";
		
		$msrp = number_format($msrp, '2', ',', '.');
		$srp = number_format($srp, '2', ',', '.');
		$data['cuSymbol'] = $cuSymbol;
		$data['msrp'] = $msrp;
		$data['srp'] = $srp;
		
		
		//var_dump($p->getData());		
		//echo "<p>ID: $id / $msrp / $srp / $storeCurrency </p>";
		//echo "<p>From Function:<br/>Group: $groupId / $groupName / $groupPrice / $currentCurrCode / $userCurrCode </p>";
		return $data;

	}

}  


?>