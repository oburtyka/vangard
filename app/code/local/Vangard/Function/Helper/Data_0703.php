<?php

class Vangard_Function_Helper_Data extends Mage_Core_Helper_Abstract{

	public function testing(){
		echo "Function Test.";
	}
	 
	public function doLog($text){		
		$filename = Mage::getBaseDir()."\_pu\updates.log";
		$date =  Mage::getModel('core/date')->date('Y-m-d H:i:s');
		$string = "[$date]  $text \n";
		//echo "$filename / $string";
		
		$context = stream_context_create();
		$fp = fopen($filename, 'r', 1, $context);		
		$tmpname = md5($string);
		file_put_contents($tmpname, $string);
		file_put_contents($tmpname, $fp, FILE_APPEND);
		fclose($fp);
		unlink($filename);
		rename($tmpname, $filename);
	}
	
	public function doLogger($text,$file){				
		$filename = Mage::getBaseDir()."\_pu\/".$file;
		$date =  Mage::getModel('core/date')->date('Y-m-d H:i:s');
		$string = "[$date] $text \n";
		//echo "$filename / $string";
		
		$context = stream_context_create();
		$fp = fopen($filename, 'r', 1, $context);		
		$tmpname = md5($string);
		file_put_contents($tmpname, $string);
		file_put_contents($tmpname, $fp, FILE_APPEND);
		fclose($fp);
		unlink($filename);
		rename($tmpname, $filename);
	}
	
	public function attOrder($att,$atts){	
		$attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $att);
		if ($attribute->usesSource())	{
			$options = $attribute->getSource()->getAllOptions('positions');
		}

		foreach($options as $val){   
			$raw[] = $val['label'];
			if (in_array($val['label'],$atts)) $sorted[] = $val['label'];
		}
		
		return $sorted;		

		/*
		$attributeId = Mage::getResourceModel('eav/entity_attribute')->getIdByCode('catalog_product',$att);
		$attribute = Mage::getModel('catalog/resource_eav_attribute')->load($attributeId);
		return $attribute ->getSource()->getAllOptions();		 
		*/		 
	}
	
	public function attAdd($att,$set){	
		$att = trim($att); $set = trim($set);
		if (!$att || !$set) return; 
		$attribute = Mage::getSingleton('eav/config')->getAttribute('catalog_product', $set);
		if ($attribute->usesSource())	{
			$options = $attribute->getSource()->getAllOptions('positions');
		}

		foreach($options as $val){   
			if (trim($val['label'])) $attSet[] = $val['label'];
		}
		
		//var_dump($attSet);		
		
		if (!in_array($att,$attSet)){ // if attribute value not in attribute options, ADD!!
			$data['option']['value'] = array(array( 0 => $att  ));
			$attribute->addData($data);
			try {
				$attribute->save();
				$session = Mage::getSingleton('adminhtml/session');
				Mage::app()->cleanCache(array(Mage_Core_Model_Translate::CACHE_TAG));
				$session->setAttributeData(false);			
				echo "Add Attribute $att in $set Success!!";
				return true;
			} catch (Exception $e) {
				echo $e->getMessage();	
				echo "Failed to Add Attribute $att in $set.";
			}	
		} else {
			echo "Attribute $att in $set already exists!";
			return true;
		}
	}
 

}  


?>