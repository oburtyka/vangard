<?php

class Vangard_Function_Model_Updatefreq{
	public function toOptionArray(){
		return array(
		    array('value'=>'*/5 * * * *', 'label'=>Mage::helper('function')->__('DEV: Every 5 Minute')),
		    array('value'=>'*/30 * * * *', 'label'=>Mage::helper('function')->__('DEV: Every 30 Minute')),
		    array('value'=>'0 * * * *', 'label'=>Mage::helper('function')->__('Hourly')),
			array('value'=>'0 */2 * * *', 'label'=>Mage::helper('function')->__('Every 2 Hours')),
		    array('value'=>'0 0 * * *', 'label'=>Mage::helper('function')->__('Daily at 12MN')),
		);
	}

}

?>