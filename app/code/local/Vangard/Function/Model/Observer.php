<?php

class Vangard_Function_Model_Observer {
	public function init() {
		Mage::helper('function/Data')->doLogger("Init Test","log.log");
	}
	
	public function updater() {
		$run = Mage::getBaseUrl(); $run = str_replace("index.php/","",$run);
		$run = $run."_pu/xyz.php";		
		file_get_contents($run);
	}

	public function updaterCustomer() {
		$run = Mage::getBaseUrl(); $run = str_replace("index.php/","",$run);
		$run = $run."_pu/customer.php";		
		file_get_contents($run);
	}
	
	public function export_new_order($observer){
		$order = $observer->getEvent()->getOrder();
		$orderID = $order->getId();
		Mage::helper('function/Data')->showOrder($orderID);		
		return $this;
	}

	
 
}

?>